(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[7],{

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export getTab */
/* unused harmony export resetData */
/* unused harmony export removeAllData */
/* unused harmony export activateOnSite */
/* unused harmony export deactivateOnSite */
/* unused harmony export getPrivacyData */
/* unused harmony export getPrivacyOptions */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return savePopupToggle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return revertDomainControl; });
/* unused harmony export updateSettings */
/* unused harmony export updatePrivacyOverride */
/* unused harmony export getManifestFragmentPath */
/* unused harmony export getCopyablesPaths */
/*
 * Copyright (C) 2022 Surfboard Holding B.V. <https://www.startpage.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const getTab = () => __awaiter(void 0, void 0, void 0, function* () {
    const [tab] = yield browser.tabs.query({
        active: true,
        currentWindow: true
    });
    return tab;
});
const sendBadgerMessage = (message, extra) => __awaiter(void 0, void 0, void 0, function* () {
    const { id: tabId, url: tabUrl } = yield getTab();
    return yield browser.runtime.sendMessage(Object.assign({ type: message, tabId,
        tabUrl }, (extra !== null && extra !== void 0 ? extra : {})));
});
const resetData = () => __awaiter(void 0, void 0, void 0, function* () {
    const { id: tabId, url: tabUrl } = yield getTab();
    return yield browser.runtime.sendMessage({
        type: 'resetData',
        tabId,
        tabUrl
    });
});
const removeAllData = () => __awaiter(void 0, void 0, void 0, function* () {
    const { id: tabId, url: tabUrl } = yield getTab();
    return yield browser.runtime.sendMessage({
        type: 'removeAllData',
        tabId,
        tabUrl
    });
});
const activateOnSite = () => __awaiter(void 0, void 0, void 0, function* () {
    console.log('trying to activate');
    const { id: tabId, url: tabUrl } = yield getTab();
    console.log(`${tabId}, ${tabUrl}`);
    return yield browser.runtime.sendMessage({
        type: 'activateOnSite',
        tabId,
        tabUrl
    });
});
const deactivateOnSite = () => __awaiter(void 0, void 0, void 0, function* () {
    const { id: tabId, url: tabUrl } = yield getTab();
    return yield browser.runtime.sendMessage({
        type: 'deactivateOnSite',
        tabId,
        tabUrl
    });
});
const getPrivacyData = () => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield sendBadgerMessage('getPopupData');
    return data;
});
const getPrivacyOptions = () => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield sendBadgerMessage('getOptionsData');
    return data;
});
const savePopupToggle = (origin, action) => __awaiter(void 0, void 0, void 0, function* () {
    return yield sendBadgerMessage('savePopupToggle', {
        origin,
        action
    });
});
const revertDomainControl = (origin) => __awaiter(void 0, void 0, void 0, function* () {
    return yield sendBadgerMessage('revertDomainControl', { origin });
});
const updateSettings = (settings) => __awaiter(void 0, void 0, void 0, function* () {
    return yield browser.runtime.sendMessage({
        type: 'updateSettings',
        data: settings
    });
});
const updatePrivacyOverride = (settings) => __awaiter(void 0, void 0, void 0, function* () {
    yield updateSettings(settings);
    return yield browser.runtime.sendMessage({
        type: 'setPrivacyOverride'
    });
});
const getManifestFragmentPath = () => {
    return `manifest-fragment.json`;
};
const getCopyablesPaths = () => {
    const SRC_PATH = 'src';
    const copyables = [`${SRC_PATH}/data`, `${SRC_PATH}/js`, `${SRC_PATH}/lib`, 'sp-js'];
    return copyables;
};


/***/ }),

/***/ 257:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrackerSettings; });
/* unused harmony export TrackerRow */
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(21);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _emotion_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(81);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _emotion_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2);
/* harmony import */ var sp_privacy__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(231);
/* harmony import */ var _utils_localization__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6);




function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }







var _ref =  true ? {
  name: "lviczn-titleCss",
  styles: "color:#6573ff;font-family:'Inter',sans-serif;font-size:16px;font-weight:600;line-height:1;padding:12px 8px;;label:titleCss;"
} : undefined;

var _ref2 =  true ? {
  name: "1n2qqnt-optionsLabelCss",
  styles: "white-space:pre-wrap;width:calc(100% / 3);;label:optionsLabelCss;"
} : undefined;

var _ref3 =  true ? {
  name: "m0hm40-TrackerSettings",
  styles: ";label:TrackerSettings;"
} : undefined;

var TrackerSettings = (_ref12) => {
  var {
    trackers,
    title,
    height,
    labelBgColor,
    labelFontSize,
    lColWidthPercent,
    rColWidthPercent,
    alternateRowColors,
    cssOverrides,
    onActionChange,
    onRevert,
    onConfirmSiteBreak
  } = _ref12;
  var trackerSettingsCss = /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("border-bottom:1px solid #dee0f7;font-family:'Inter',sans-serif;", height && "height: ".concat(height, "px; overflow-y: auto;"), " &:last-of-type{border-bottom:none;};label:trackerSettingsCss;" + ( true ? "" : undefined));
  var titleCss = _ref;
  var labelRowCss = /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("background:", labelBgColor || 'transparent', ";align-items:center;display:flex;font-size:", labelFontSize || '14px', ";font-weight:500;margin-bottom:6px;padding:8px;position:sticky;z-index:1;top:0;border-radius:4px;background:#f2f3ff;;label:labelRowCss;" + ( true ? "" : undefined));
  var domainColCss = /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("min-width:0;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:", lColWidthPercent, "%;;label:domainColCss;" + ( true ? "" : undefined));
  var optionsColCss = /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("align-items:center;display:flex;justify-content:space-around;text-align:center;width:", rColWidthPercent, "%;;label:optionsColCss;" + ( true ? "" : undefined));
  var optionsLabelCss = _ref2;
  return Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: /*#__PURE__*/Object(_emotion_css__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])([trackerSettingsCss, cssOverrides || _ref3], ";label:TrackerSettings;" + ( true ? "" : undefined))
  }, title && Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: titleCss
  }, title), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: labelRowCss
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: domainColCss
  }, Object(_utils_localization__WEBPACK_IMPORTED_MODULE_6__[/* getMessage */ "b"])('popupManageTrackersDomainLabel')), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: optionsColCss
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: optionsLabelCss
  }, Object(_utils_localization__WEBPACK_IMPORTED_MODULE_6__[/* getMessage */ "b"])('popupManageTrackersBlockAllLabel')), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: optionsLabelCss
  }, Object(_utils_localization__WEBPACK_IMPORTED_MODULE_6__[/* getMessage */ "b"])('popupManageTrackersBlockCookiesLabel')), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: optionsLabelCss
  }, Object(_utils_localization__WEBPACK_IMPORTED_MODULE_6__[/* getMessage */ "b"])('popupManageTrackersAllowAllLabel')))), Object.entries(trackers) // eslint-disable-next-line @typescript-eslint/no-unused-vars
  .filter((_ref13) => {
    var [host, info] = _ref13;
    return info.action !== 'noaction';
  }).map((_ref14, i) => {
    var [host, info] = _ref14;
    return Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])(TrackerRow, {
      host: host,
      info: info,
      index: alternateRowColors ? i : undefined,
      lColWidthPercent: lColWidthPercent,
      rColWidthPercent: rColWidthPercent,
      onActionChange: onActionChange,
      onRevert: onRevert,
      onConfirmSiteBreak: onConfirmSiteBreak
    });
  }));
};

var _ref4 =  true ? {
  name: "bc0p8h-optionSelectCss",
  styles: "appearance:none;background:#fbfbfd;border:1px solid #ced1dd;border-radius:50%;height:16px;margin:0;padding:0;width:16px;&:focus{outline:none;};label:optionSelectCss;"
} : undefined;

var _ref5 =  true ? {
  name: "1f5fkwd-TrackerRow",
  styles: "display:inline-block;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;;label:TrackerRow;"
} : undefined;

var _ref6 =  true ? {
  name: "g6ysrs-TrackerRow",
  styles: "display:flex;position:relative;;label:TrackerRow;"
} : undefined;

var _ref7 =  true ? {
  name: "g6ysrs-TrackerRow",
  styles: "display:flex;position:relative;;label:TrackerRow;"
} : undefined;

var _ref8 =  true ? {
  name: "g6ysrs-TrackerRow",
  styles: "display:flex;position:relative;;label:TrackerRow;"
} : undefined;

var _ref9 =  true ? {
  name: "alc60x-TrackerRow",
  styles: "margin-right:auto;width:60%;;label:TrackerRow;"
} : undefined;

var _ref10 =  true ? {
  name: "1j35ba-TrackerRow",
  styles: "background:transparent;border:0;color:#ffffff;cursor:pointer;font-size:14px;font-weight:600;margin-right:24px;padding:0;text-decoration:underline;&:focus{outline:none;};label:TrackerRow;"
} : undefined;

var _ref11 =  true ? {
  name: "6nrdgt-TrackerRow",
  styles: "background:transparent;border:0;color:#ffffff;cursor:pointer;font-size:14px;font-weight:600;padding:0;text-decoration:underline;&:focus{outline:none;};label:TrackerRow;"
} : undefined;

var TrackerRow = (_ref15) => {
  var {
    host,
    info,
    index,
    lColWidthPercent,
    rColWidthPercent,
    onActionChange,
    onRevert,
    onConfirmSiteBreak
  } = _ref15;
  var [showWarning, setShowWarning] = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false);
  var optionSelectCss = _ref4;
  var trackerRowCss = /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("align-items:center;border-radius:4px;display:flex;font-size:14px;margin:1px 0;padding:10px 8px;input[type='radio']{", optionSelectCss, ";};label:trackerRowCss;" + ( true ? "" : undefined));
  var optionsColCss = /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("display:flex;justify-content:space-around;text-align:center;width:", rColWidthPercent, "%;;label:optionsColCss;" + ( true ? "" : undefined));
  var domainColCss = /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("align-items:center;display:flex;min-width:0;width:", lColWidthPercent, "%;;label:domainColCss;" + ( true ? "" : undefined));
  var optionSelectFillCss = /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("background:", showWarning || info.action === 'user_block' && info.cookieBlocklisted ? '#EB5757' : '#6573ff', ";border-radius:50%;height:16px;left:0;position:absolute;top:0;width:16px;;label:optionSelectFillCss;" + ( true ? "" : undefined));
  var revertBtnCss = /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("background:", info.action === 'user_block' && info.cookieBlocklisted ? '#EB5757' : '#6573ff', ";border:none;cursor:pointer;flex-shrink:0;height:16px;margin:0 8px 0 0;mask:url('revert-arrow.svg');padding:0;width:16px;&:focus{outline:none;};label:revertBtnCss;" + ( true ? "" : undefined));
  var isUserSet = ['user_block', 'user_allow', 'user_cookieblock'].includes(info.action);

  var resolveRowBgColor = active => {
    if (active) {
      if (info.action === 'user_block' && info.cookieBlocklisted) {
        return '#FFEDEE';
      }

      return '#F0F1FF';
    }

    if (index !== undefined && index % 2 === 0) {
      return '#fbfbfd';
    }

    return 'transparent';
  };

  var onChange = event => {
    var action = event.target.value;

    if (showWarning) {
      return;
    }

    if (info.cookieBlocklisted && action === 'block') {
      setShowWarning(true);
      return;
    }

    onActionChange(host, action);
  };

  var blockSelected = ['block', 'user_block'].includes(info.action) || showWarning;
  var cookieBlockSelected = ['cookieblock', 'user_cookieblock'].includes(info.action) && !showWarning;
  var allowSelected = ['allow', 'user_allow', 'dnt'].includes(info.action) && !showWarning;
  return Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])(react__WEBPACK_IMPORTED_MODULE_3___default.a.Fragment, null, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: /*#__PURE__*/Object(_emotion_css__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])([trackerRowCss, /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("background:", resolveRowBgColor(isUserSet), ";;label:TrackerRow;" + ( true ? "" : undefined))], ";label:TrackerRow;" + ( true ? "" : undefined))
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: domainColCss
  }, isUserSet && Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("button", {
    type: "submit",
    css: revertBtnCss,
    "aria-label": "revert",
    onClick: () => {
      Object(sp_privacy__WEBPACK_IMPORTED_MODULE_5__[/* revertDomainControl */ "a"])(host).then(() => {
        onRevert();
      });
    }
  }), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("span", {
    css: _ref5
  }, host)), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: optionsColCss
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("label", {
    htmlFor: "".concat(host),
    css: _ref6
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("input", {
    type: "radio",
    checked: blockSelected,
    name: host,
    value: "block",
    onChange: onChange,
    css: optionSelectCss
  }), blockSelected && Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("span", {
    css: optionSelectFillCss
  })), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("label", {
    htmlFor: "".concat(host),
    css: _ref7
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("input", {
    type: "radio",
    checked: cookieBlockSelected,
    name: host,
    value: "cookieblock",
    onChange: onChange,
    css: optionSelectCss
  }), cookieBlockSelected && Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("span", {
    css: optionSelectFillCss
  })), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("label", {
    htmlFor: "".concat(host),
    css: _ref8
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("input", {
    type: "radio",
    checked: allowSelected && !showWarning,
    name: host,
    value: "allow",
    onChange: onChange,
    css: optionSelectCss
  }), allowSelected && !showWarning && Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("span", {
    css: optionSelectFillCss
  })))), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: /*#__PURE__*/Object(_emotion_css__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])([trackerRowCss, /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* css */ "a"])("background:#eb5757;color:#ffffff;font-weight:600;", !showWarning && 'display: none', ";;label:TrackerRow;" + ( true ? "" : undefined))], ";label:TrackerRow;" + ( true ? "" : undefined))
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("div", {
    css: _ref9
  }, Object(_utils_localization__WEBPACK_IMPORTED_MODULE_6__[/* getMessage */ "b"])('popupManageTrackersWarning')), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("button", {
    type: "submit",
    onClick: () => {
      setShowWarning(false);
      Object(sp_privacy__WEBPACK_IMPORTED_MODULE_5__[/* savePopupToggle */ "b"])(host, 'block').then(() => {
        onConfirmSiteBreak();
      });
    },
    css: _ref10
  }, Object(_utils_localization__WEBPACK_IMPORTED_MODULE_6__[/* getMessage */ "b"])('popupManageTrackersWarningProceed')), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_4__[/* jsx */ "b"])("button", {
    type: "submit",
    onClick: () => {
      setShowWarning(false);
    },
    css: _ref11
  }, Object(_utils_localization__WEBPACK_IMPORTED_MODULE_6__[/* getMessage */ "b"])('popupManageTrackersWarningCancel'))));
};

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(21);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(29);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _emotion_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(39);
/* harmony import */ var sp_privacy__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(231);
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(335);
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_perfect_scrollbar_dist_css_styles_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(336);
/* harmony import */ var react_perfect_scrollbar_dist_css_styles_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_perfect_scrollbar_dist_css_styles_css__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _utils_localization__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(6);
/* harmony import */ var _selectors_index__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(54);
/* harmony import */ var _Common_TrackerSettings__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(257);



function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }












var _ref =  true ? {
  name: "ysslze-titleCss",
  styles: "border-bottom:1px solid #ebecf7;font-weight:600;font-size:22px;padding:16px;;label:titleCss;"
} : undefined;

var _ref2 =  true ? {
  name: "gc75tx-ManageTrackers",
  styles: "padding:0 8px 16px;;label:ManageTrackers;"
} : undefined;

var _ref3 =  true ? {
  name: "gc75tx-ManageTrackers",
  styles: "padding:0 8px 16px;;label:ManageTrackers;"
} : undefined;

var ManageTrackers = () => {
  var {
    trackers
  } = Object(react_redux__WEBPACK_IMPORTED_MODULE_4__[/* useSelector */ "c"])(_selectors_index__WEBPACK_IMPORTED_MODULE_9__[/* tabDataSelector */ "d"]);
  var majorTrackers = {};
  Object.entries(trackers).forEach((_ref4) => {
    var [host, info] = _ref4;

    if (info.type === 'major' && !['allow', 'dnt'].includes(info.action)) {
      majorTrackers[host] = info;
    }
  });
  var minorTrackers = {};
  Object.entries(trackers).forEach((_ref5) => {
    var [host, info] = _ref5;

    if (info.type === 'minor' && !['allow', 'dnt'].includes(info.action)) {
      minorTrackers[host] = info;
    }
  });
  var titleCss = _ref;
  return Object(_emotion_core__WEBPACK_IMPORTED_MODULE_3__[/* jsx */ "b"])("div", {
    css: /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_3__[/* css */ "a"])(react_perfect_scrollbar_dist_css_styles_css__WEBPACK_IMPORTED_MODULE_7___default.a, ";label:ManageTrackers;" + ( true ? "" : undefined))
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_3__[/* jsx */ "b"])(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6___default.a, {
    style: {
      height: '444px'
    }
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_3__[/* jsx */ "b"])("div", {
    css: titleCss
  }, Object(_utils_localization__WEBPACK_IMPORTED_MODULE_8__[/* getMessage */ "b"])('popupManageTrackersTitle')), Object.entries(majorTrackers).length > 0 && Object(_emotion_core__WEBPACK_IMPORTED_MODULE_3__[/* jsx */ "b"])(_Common_TrackerSettings__WEBPACK_IMPORTED_MODULE_10__[/* TrackerSettings */ "a"], {
    title: Object(_utils_localization__WEBPACK_IMPORTED_MODULE_8__[/* getMessage */ "b"])('popupManageTrackersMajorTitle'),
    labelBgColor: "#fbfbfd",
    labelFontSize: "11px",
    trackers: majorTrackers,
    lColWidthPercent: 46.34,
    rColWidthPercent: 56.35,
    cssOverrides: _ref2,
    onActionChange: (host, action) => {
      Object(sp_privacy__WEBPACK_IMPORTED_MODULE_5__[/* savePopupToggle */ "b"])(host, action).then(() => {
        browser.runtime.sendMessage({
          message: 'getPrivacyData'
        });
      });
    },
    onRevert: () => {
      browser.tabs.reload().then(() => {
        window.close();
      });
    },
    onConfirmSiteBreak: () => {
      browser.runtime.sendMessage({
        message: 'getPrivacyData'
      });
    }
  }), Object.entries(minorTrackers).length > 0 && Object(_emotion_core__WEBPACK_IMPORTED_MODULE_3__[/* jsx */ "b"])(_Common_TrackerSettings__WEBPACK_IMPORTED_MODULE_10__[/* TrackerSettings */ "a"], {
    title: Object(_utils_localization__WEBPACK_IMPORTED_MODULE_8__[/* getMessage */ "b"])('popupManageTrackersMinorTitle'),
    labelBgColor: "#fbfbfd",
    labelFontSize: "11px",
    trackers: minorTrackers,
    lColWidthPercent: 46.34,
    rColWidthPercent: 56.35,
    cssOverrides: _ref3,
    alternateRowColors: true,
    onActionChange: (host, action) => {
      Object(sp_privacy__WEBPACK_IMPORTED_MODULE_5__[/* savePopupToggle */ "b"])(host, action).then(() => {
        browser.runtime.sendMessage({
          message: 'getPrivacyData'
        });
      });
    },
    onRevert: () => {
      browser.tabs.reload().then(() => {
        window.close();
      });
    },
    onConfirmSiteBreak: () => {
      browser.runtime.sendMessage({
        message: 'getPrivacyData'
      });
    }
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (ManageTrackers);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc3AtcHJpdmFjeS9saWIvZXNtL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9wcml2YWN5LXNlYXJjaC9jb21wb25lbnRzL0NvbW1vbi9UcmFja2VyU2V0dGluZ3MudHN4Iiwid2VicGFjazovLy8uL3NyYy9wcml2YWN5LXNlYXJjaC9jb21wb25lbnRzL1BvcHVwL01hbmFnZVRyYWNrZXJzLnRzeCJdLCJuYW1lcyI6WyJUcmFja2VyU2V0dGluZ3MiLCJ0cmFja2VycyIsInRpdGxlIiwiaGVpZ2h0IiwibGFiZWxCZ0NvbG9yIiwibGFiZWxGb250U2l6ZSIsImxDb2xXaWR0aFBlcmNlbnQiLCJyQ29sV2lkdGhQZXJjZW50IiwiYWx0ZXJuYXRlUm93Q29sb3JzIiwiY3NzT3ZlcnJpZGVzIiwib25BY3Rpb25DaGFuZ2UiLCJvblJldmVydCIsIm9uQ29uZmlybVNpdGVCcmVhayIsInRyYWNrZXJTZXR0aW5nc0NzcyIsImNzcyIsInRpdGxlQ3NzIiwibGFiZWxSb3dDc3MiLCJkb21haW5Db2xDc3MiLCJvcHRpb25zQ29sQ3NzIiwib3B0aW9uc0xhYmVsQ3NzIiwiZ2V0TWVzc2FnZSIsIk9iamVjdCIsImVudHJpZXMiLCJmaWx0ZXIiLCJob3N0IiwiaW5mbyIsImFjdGlvbiIsIm1hcCIsImkiLCJ1bmRlZmluZWQiLCJUcmFja2VyUm93IiwiaW5kZXgiLCJzaG93V2FybmluZyIsInNldFNob3dXYXJuaW5nIiwidXNlU3RhdGUiLCJvcHRpb25TZWxlY3RDc3MiLCJ0cmFja2VyUm93Q3NzIiwib3B0aW9uU2VsZWN0RmlsbENzcyIsImNvb2tpZUJsb2NrbGlzdGVkIiwicmV2ZXJ0QnRuQ3NzIiwiaXNVc2VyU2V0IiwiaW5jbHVkZXMiLCJyZXNvbHZlUm93QmdDb2xvciIsImFjdGl2ZSIsIm9uQ2hhbmdlIiwiZXZlbnQiLCJ0YXJnZXQiLCJ2YWx1ZSIsImJsb2NrU2VsZWN0ZWQiLCJjb29raWVCbG9ja1NlbGVjdGVkIiwiYWxsb3dTZWxlY3RlZCIsInJldmVydERvbWFpbkNvbnRyb2wiLCJ0aGVuIiwic2F2ZVBvcHVwVG9nZ2xlIiwiTWFuYWdlVHJhY2tlcnMiLCJ1c2VTZWxlY3RvciIsInRhYkRhdGFTZWxlY3RvciIsIm1ham9yVHJhY2tlcnMiLCJmb3JFYWNoIiwidHlwZSIsIm1pbm9yVHJhY2tlcnMiLCJzY3JvbGxiYXJTdHlsZSIsImxlbmd0aCIsImJyb3dzZXIiLCJydW50aW1lIiwic2VuZE1lc3NhZ2UiLCJtZXNzYWdlIiwidGFicyIsInJlbG9hZCIsIndpbmRvdyIsImNsb3NlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QjtBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsU0FBSSxJQUFJLFNBQUk7QUFDN0IsMkJBQTJCLCtEQUErRCxnQkFBZ0IsRUFBRSxFQUFFO0FBQzlHO0FBQ0EsbUNBQW1DLE1BQU0sNkJBQTZCLEVBQUUsWUFBWSxXQUFXLEVBQUU7QUFDakcsa0NBQWtDLE1BQU0saUNBQWlDLEVBQUUsWUFBWSxXQUFXLEVBQUU7QUFDcEcsK0JBQStCLHFGQUFxRjtBQUNwSDtBQUNBLEtBQUs7QUFDTDtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsQ0FBQztBQUNEO0FBQ0EsV0FBVyx5QkFBeUI7QUFDcEMsNERBQTREO0FBQzVELGdCQUFnQixrREFBa0Q7QUFDbEUsQ0FBQztBQUNNO0FBQ1AsV0FBVyx5QkFBeUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsQ0FBQztBQUNNO0FBQ1AsV0FBVyx5QkFBeUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsQ0FBQztBQUNNO0FBQ1A7QUFDQSxXQUFXLHlCQUF5QjtBQUNwQyxtQkFBbUIsTUFBTSxJQUFJLE9BQU87QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsQ0FBQztBQUNNO0FBQ1AsV0FBVyx5QkFBeUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsQ0FBQztBQUNNO0FBQ1A7QUFDQTtBQUNBLENBQUM7QUFDTTtBQUNQO0FBQ0E7QUFDQSxDQUFDO0FBQ007QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsQ0FBQztBQUNNO0FBQ1AsMkRBQTJELFNBQVM7QUFDcEUsQ0FBQztBQUNNO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLENBQUM7QUFDTTtBQUNQO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxDQUFDO0FBQ007QUFDUDtBQUNBO0FBQ087QUFDUDtBQUNBLDBCQUEwQixTQUFTLFdBQVcsU0FBUyxTQUFTLFNBQVM7QUFDekU7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0dBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQk8sSUFBTUEsZUFBeUMsR0FBRyxZQWFuRDtBQUFBLE1BYm9EO0FBQ3REQyxZQURzRDtBQUV0REMsU0FGc0Q7QUFHdERDLFVBSHNEO0FBSXREQyxnQkFKc0Q7QUFLdERDLGlCQUxzRDtBQU10REMsb0JBTnNEO0FBT3REQyxvQkFQc0Q7QUFRdERDLHNCQVJzRDtBQVN0REMsZ0JBVHNEO0FBVXREQyxrQkFWc0Q7QUFXdERDLFlBWHNEO0FBWXREQztBQVpzRCxHQWFwRDtBQUNGLE1BQU1DLGtCQUFrQixnQkFBR0MsaUVBQUgsb0VBR2xCWCxNQUFNLHNCQUFlQSxNQUFmLDBCQUhZLDhGQUF4QjtBQVVBLE1BQU1ZLFFBQVEsT0FBZDtBQVNBLE1BQU1DLFdBQVcsZ0JBQUdGLGlFQUFILGdCQUNDVixZQUFZLElBQUksYUFEakIsaURBSUFDLGFBQWEsSUFBSSxNQUpqQix1S0FBakI7QUFlQSxNQUFNWSxZQUFZLGdCQUFHSCxpRUFBSCxpRkFLTFIsZ0JBTEssc0RBQWxCO0FBUUEsTUFBTVksYUFBYSxnQkFBR0osaUVBQUgsMEZBS05QLGdCQUxNLHVEQUFuQjtBQVFBLE1BQU1ZLGVBQWUsUUFBckI7QUFLQSxTQUNJO0FBQUssT0FBRyxvRkFBRSxDQUFDTixrQkFBRCxFQUFxQkosWUFBWSxTQUFqQyxDQUFGO0FBQVIsS0FDS1AsS0FBSyxJQUFJO0FBQUssT0FBRyxFQUFFYTtBQUFWLEtBQXFCYixLQUFyQixDQURkLEVBRUk7QUFBSyxPQUFHLEVBQUVjO0FBQVYsS0FDSTtBQUFLLE9BQUcsRUFBRUM7QUFBVixLQUF5QkcsOEVBQVUsQ0FBQyxnQ0FBRCxDQUFuQyxDQURKLEVBRUk7QUFBSyxPQUFHLEVBQUVGO0FBQVYsS0FDSTtBQUFLLE9BQUcsRUFBRUM7QUFBVixLQUE0QkMsOEVBQVUsQ0FBQyxrQ0FBRCxDQUF0QyxDQURKLEVBRUk7QUFBSyxPQUFHLEVBQUVEO0FBQVYsS0FBNEJDLDhFQUFVLENBQUMsc0NBQUQsQ0FBdEMsQ0FGSixFQUdJO0FBQUssT0FBRyxFQUFFRDtBQUFWLEtBQTRCQyw4RUFBVSxDQUFDLGtDQUFELENBQXRDLENBSEosQ0FGSixDQUZKLEVBVUtDLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlckIsUUFBZixFQUNHO0FBREgsR0FFSXNCLE1BRkosQ0FFVztBQUFBLFFBQUMsQ0FBQ0MsSUFBRCxFQUFPQyxJQUFQLENBQUQ7QUFBQSxXQUFrQkEsSUFBSSxDQUFDQyxNQUFMLEtBQWdCLFVBQWxDO0FBQUEsR0FGWCxFQUdJQyxHQUhKLENBR1EsU0FBZUMsQ0FBZixLQUFxQjtBQUFBLFFBQXBCLENBQUNKLElBQUQsRUFBT0MsSUFBUCxDQUFvQjtBQUN0QixXQUNJLGtFQUFDLFVBQUQ7QUFDSSxVQUFJLEVBQUVELElBRFY7QUFFSSxVQUFJLEVBQUVDLElBRlY7QUFHSSxXQUFLLEVBQUVqQixrQkFBa0IsR0FBR29CLENBQUgsR0FBT0MsU0FIcEM7QUFJSSxzQkFBZ0IsRUFBRXZCLGdCQUp0QjtBQUtJLHNCQUFnQixFQUFFQyxnQkFMdEI7QUFNSSxvQkFBYyxFQUFFRyxjQU5wQjtBQU9JLGNBQVEsRUFBRUMsUUFQZDtBQVFJLHdCQUFrQixFQUFFQztBQVJ4QixNQURKO0FBWUgsR0FoQkosQ0FWTCxDQURKO0FBOEJILENBbkdNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnSEEsSUFBTWtCLFVBQStCLEdBQUcsWUFTekM7QUFBQSxNQVQwQztBQUM1Q04sUUFENEM7QUFFNUNDLFFBRjRDO0FBRzVDTSxTQUg0QztBQUk1Q3pCLG9CQUo0QztBQUs1Q0Msb0JBTDRDO0FBTTVDRyxrQkFONEM7QUFPNUNDLFlBUDRDO0FBUTVDQztBQVI0QyxHQVMxQztBQUNGLE1BQU0sQ0FBQ29CLFdBQUQsRUFBY0MsY0FBZCxJQUFnQ0Msc0RBQVEsQ0FBQyxLQUFELENBQTlDO0FBRUEsTUFBTUMsZUFBZSxRQUFyQjtBQWVBLE1BQU1DLGFBQWEsZ0JBQUd0QixpRUFBSCx3SEFVVHFCLGVBVlMsdURBQW5CO0FBY0EsTUFBTWpCLGFBQWEsZ0JBQUdKLGlFQUFILHVFQUlOUCxnQkFKTSx1REFBbkI7QUFPQSxNQUFNVSxZQUFZLGdCQUFHSCxpRUFBSCx1REFJTFIsZ0JBSkssc0RBQWxCO0FBT0EsTUFBTStCLG1CQUFtQixnQkFBR3ZCLGlFQUFILGdCQUNQa0IsV0FBVyxJQUFLUCxJQUFJLENBQUNDLE1BQUwsS0FBZ0IsWUFBaEIsSUFBZ0NELElBQUksQ0FBQ2EsaUJBQXJELEdBQTBFLFNBQTFFLEdBQXNGLFNBRC9FLG9JQUF6QjtBQVVBLE1BQU1DLFlBQVksZ0JBQUd6QixpRUFBSCxnQkFDQVcsSUFBSSxDQUFDQyxNQUFMLEtBQWdCLFlBQWhCLElBQWdDRCxJQUFJLENBQUNhLGlCQUFyQyxHQUF5RCxTQUF6RCxHQUFxRSxTQURyRSxtTUFBbEI7QUFnQkEsTUFBTUUsU0FBUyxHQUFHLENBQUMsWUFBRCxFQUFlLFlBQWYsRUFBNkIsa0JBQTdCLEVBQWlEQyxRQUFqRCxDQUEwRGhCLElBQUksQ0FBQ0MsTUFBL0QsQ0FBbEI7O0FBRUEsTUFBTWdCLGlCQUFpQixHQUFJQyxNQUFELElBQXFCO0FBQzNDLFFBQUlBLE1BQUosRUFBWTtBQUNSLFVBQUlsQixJQUFJLENBQUNDLE1BQUwsS0FBZ0IsWUFBaEIsSUFBZ0NELElBQUksQ0FBQ2EsaUJBQXpDLEVBQTREO0FBQ3hELGVBQU8sU0FBUDtBQUNIOztBQUNELGFBQU8sU0FBUDtBQUNIOztBQUVELFFBQUlQLEtBQUssS0FBS0YsU0FBVixJQUF1QkUsS0FBSyxHQUFHLENBQVIsS0FBYyxDQUF6QyxFQUE0QztBQUN4QyxhQUFPLFNBQVA7QUFDSDs7QUFDRCxXQUFPLGFBQVA7QUFDSCxHQVpEOztBQWNBLE1BQU1hLFFBQVEsR0FBSUMsS0FBRCxJQUEwQztBQUN2RCxRQUFNbkIsTUFBTSxHQUFHbUIsS0FBSyxDQUFDQyxNQUFOLENBQWFDLEtBQTVCOztBQUVBLFFBQUlmLFdBQUosRUFBaUI7QUFDYjtBQUNIOztBQUVELFFBQUlQLElBQUksQ0FBQ2EsaUJBQUwsSUFBMEJaLE1BQU0sS0FBSyxPQUF6QyxFQUFrRDtBQUM5Q08sb0JBQWMsQ0FBQyxJQUFELENBQWQ7QUFDQTtBQUNIOztBQUVEdkIsa0JBQWMsQ0FBQ2MsSUFBRCxFQUFPRSxNQUFQLENBQWQ7QUFDSCxHQWJEOztBQWVBLE1BQU1zQixhQUFhLEdBQUcsQ0FBQyxPQUFELEVBQVUsWUFBVixFQUF3QlAsUUFBeEIsQ0FBaUNoQixJQUFJLENBQUNDLE1BQXRDLEtBQWlETSxXQUF2RTtBQUNBLE1BQU1pQixtQkFBbUIsR0FBRyxDQUFDLGFBQUQsRUFBZ0Isa0JBQWhCLEVBQW9DUixRQUFwQyxDQUE2Q2hCLElBQUksQ0FBQ0MsTUFBbEQsS0FBNkQsQ0FBQ00sV0FBMUY7QUFDQSxNQUFNa0IsYUFBYSxHQUFHLENBQUMsT0FBRCxFQUFVLFlBQVYsRUFBd0IsS0FBeEIsRUFBK0JULFFBQS9CLENBQXdDaEIsSUFBSSxDQUFDQyxNQUE3QyxLQUF3RCxDQUFDTSxXQUEvRTtBQUVBLFNBQ0ksK0hBQ0k7QUFDSSxPQUFHLG9GQUFFLENBQ0RJLGFBREMsZUFFRHRCLGlFQUZDLGdCQUdpQjRCLGlCQUFpQixDQUFDRixTQUFELENBSGxDLG9EQUFGO0FBRFAsS0FRSTtBQUFLLE9BQUcsRUFBRXZCO0FBQVYsS0FDS3VCLFNBQVMsSUFDTjtBQUNJLFFBQUksRUFBQyxRQURUO0FBRUksT0FBRyxFQUFFRCxZQUZUO0FBR0ksa0JBQVcsUUFIZjtBQUlJLFdBQU8sRUFBRSxNQUFNO0FBQ1hZLG9GQUFtQixDQUFDM0IsSUFBRCxDQUFuQixDQUEwQjRCLElBQTFCLENBQStCLE1BQU07QUFDakN6QyxnQkFBUTtBQUNYLE9BRkQ7QUFHSDtBQVJMLElBRlIsRUFhSTtBQUNJLE9BQUc7QUFEUCxLQVFLYSxJQVJMLENBYkosQ0FSSixFQWdDSTtBQUFLLE9BQUcsRUFBRU47QUFBVixLQUNJO0FBQ0ksV0FBTyxZQUFLTSxJQUFMLENBRFg7QUFFSSxPQUFHO0FBRlAsS0FPSTtBQUNJLFFBQUksRUFBQyxPQURUO0FBRUksV0FBTyxFQUFFd0IsYUFGYjtBQUdJLFFBQUksRUFBRXhCLElBSFY7QUFJSSxTQUFLLEVBQUMsT0FKVjtBQUtJLFlBQVEsRUFBRW9CLFFBTGQ7QUFNSSxPQUFHLEVBQUVUO0FBTlQsSUFQSixFQWVLYSxhQUFhLElBQUk7QUFBTSxPQUFHLEVBQUVYO0FBQVgsSUFmdEIsQ0FESixFQWtCSTtBQUNJLFdBQU8sWUFBS2IsSUFBTCxDQURYO0FBRUksT0FBRztBQUZQLEtBT0k7QUFDSSxRQUFJLEVBQUMsT0FEVDtBQUVJLFdBQU8sRUFBRXlCLG1CQUZiO0FBR0ksUUFBSSxFQUFFekIsSUFIVjtBQUlJLFNBQUssRUFBQyxhQUpWO0FBS0ksWUFBUSxFQUFFb0IsUUFMZDtBQU1JLE9BQUcsRUFBRVQ7QUFOVCxJQVBKLEVBZUtjLG1CQUFtQixJQUFJO0FBQU0sT0FBRyxFQUFFWjtBQUFYLElBZjVCLENBbEJKLEVBbUNJO0FBQ0ksV0FBTyxZQUFLYixJQUFMLENBRFg7QUFFSSxPQUFHO0FBRlAsS0FPSTtBQUNJLFFBQUksRUFBQyxPQURUO0FBRUksV0FBTyxFQUFFMEIsYUFBYSxJQUFJLENBQUNsQixXQUYvQjtBQUdJLFFBQUksRUFBRVIsSUFIVjtBQUlJLFNBQUssRUFBQyxPQUpWO0FBS0ksWUFBUSxFQUFFb0IsUUFMZDtBQU1JLE9BQUcsRUFBRVQ7QUFOVCxJQVBKLEVBZUtlLGFBQWEsSUFBSSxDQUFDbEIsV0FBbEIsSUFBaUM7QUFBTSxPQUFHLEVBQUVLO0FBQVgsSUFmdEMsQ0FuQ0osQ0FoQ0osQ0FESixFQXVGSTtBQUNJLE9BQUcsb0ZBQUUsQ0FDREQsYUFEQyxlQUVEdEIsaUVBRkMsc0RBTUssQ0FBQ2tCLFdBQUQsSUFBZ0IsZUFOckIsb0RBQUY7QUFEUCxLQVdJO0FBQ0ksT0FBRztBQURQLEtBTUtaLDhFQUFVLENBQUMsNEJBQUQsQ0FOZixDQVhKLEVBbUJJO0FBQ0ksUUFBSSxFQUFDLFFBRFQ7QUFFSSxXQUFPLEVBQUUsTUFBTTtBQUNYYSxvQkFBYyxDQUFDLEtBQUQsQ0FBZDtBQUNBb0IsZ0ZBQWUsQ0FBQzdCLElBQUQsRUFBTyxPQUFQLENBQWYsQ0FBK0I0QixJQUEvQixDQUFvQyxNQUFNO0FBQ3RDeEMsMEJBQWtCO0FBQ3JCLE9BRkQ7QUFHSCxLQVBMO0FBUUksT0FBRztBQVJQLEtBd0JLUSw4RUFBVSxDQUFDLG1DQUFELENBeEJmLENBbkJKLEVBNkNJO0FBQ0ksUUFBSSxFQUFDLFFBRFQ7QUFFSSxXQUFPLEVBQUUsTUFBTTtBQUNYYSxvQkFBYyxDQUFDLEtBQUQsQ0FBZDtBQUNILEtBSkw7QUFLSSxPQUFHO0FBTFAsS0FvQktiLDhFQUFVLENBQUMsa0NBQUQsQ0FwQmYsQ0E3Q0osQ0F2RkosQ0FESjtBQThKSCxDQWxSTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0SVA7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNa0MsY0FBa0IsR0FBRyxNQUFNO0FBQzdCLE1BQU07QUFBQ3JEO0FBQUQsTUFBYXNELHVFQUFXLENBQUNDLHdFQUFELENBQTlCO0FBRUEsTUFBTUMsYUFBdUIsR0FBRyxFQUFoQztBQUNBcEMsUUFBTSxDQUFDQyxPQUFQLENBQWVyQixRQUFmLEVBQXlCeUQsT0FBekIsQ0FBaUMsV0FBa0I7QUFBQSxRQUFqQixDQUFDbEMsSUFBRCxFQUFPQyxJQUFQLENBQWlCOztBQUMvQyxRQUFJQSxJQUFJLENBQUNrQyxJQUFMLEtBQWMsT0FBZCxJQUF5QixDQUFDLENBQUMsT0FBRCxFQUFVLEtBQVYsRUFBaUJsQixRQUFqQixDQUEwQmhCLElBQUksQ0FBQ0MsTUFBL0IsQ0FBOUIsRUFBc0U7QUFDbEUrQixtQkFBYSxDQUFDakMsSUFBRCxDQUFiLEdBQXNCQyxJQUF0QjtBQUNIO0FBQ0osR0FKRDtBQU1BLE1BQU1tQyxhQUF1QixHQUFHLEVBQWhDO0FBQ0F2QyxRQUFNLENBQUNDLE9BQVAsQ0FBZXJCLFFBQWYsRUFBeUJ5RCxPQUF6QixDQUFpQyxXQUFrQjtBQUFBLFFBQWpCLENBQUNsQyxJQUFELEVBQU9DLElBQVAsQ0FBaUI7O0FBQy9DLFFBQUlBLElBQUksQ0FBQ2tDLElBQUwsS0FBYyxPQUFkLElBQXlCLENBQUMsQ0FBQyxPQUFELEVBQVUsS0FBVixFQUFpQmxCLFFBQWpCLENBQTBCaEIsSUFBSSxDQUFDQyxNQUEvQixDQUE5QixFQUFzRTtBQUNsRWtDLG1CQUFhLENBQUNwQyxJQUFELENBQWIsR0FBc0JDLElBQXRCO0FBQ0g7QUFDSixHQUpEO0FBTUEsTUFBTVYsUUFBUSxPQUFkO0FBT0EsU0FDSTtBQUNJLE9BQUcsZUFBRUQsaUVBQUYsQ0FDRytDLGtGQURIO0FBRFAsS0FLSSxrRUFBQyw4REFBRDtBQUFrQixTQUFLLEVBQUU7QUFBQzFELFlBQU0sRUFBRTtBQUFUO0FBQXpCLEtBQ0k7QUFBSyxPQUFHLEVBQUVZO0FBQVYsS0FBcUJLLDhFQUFVLENBQUMsMEJBQUQsQ0FBL0IsQ0FESixFQUVLQyxNQUFNLENBQUNDLE9BQVAsQ0FBZW1DLGFBQWYsRUFBOEJLLE1BQTlCLEdBQXVDLENBQXZDLElBQ0csa0VBQUMsZ0ZBQUQ7QUFDSSxTQUFLLEVBQUUxQyw4RUFBVSxDQUFDLCtCQUFELENBRHJCO0FBRUksZ0JBQVksRUFBQyxTQUZqQjtBQUdJLGlCQUFhLEVBQUMsTUFIbEI7QUFJSSxZQUFRLEVBQUVxQyxhQUpkO0FBS0ksb0JBQWdCLEVBQUUsS0FMdEI7QUFNSSxvQkFBZ0IsRUFBRSxLQU50QjtBQU9JLGdCQUFZLE9BUGhCO0FBVUksa0JBQWMsRUFBRSxDQUFDakMsSUFBRCxFQUFPRSxNQUFQLEtBQWtCO0FBQzlCMkIsZ0ZBQWUsQ0FBQzdCLElBQUQsRUFBT0UsTUFBUCxDQUFmLENBQThCMEIsSUFBOUIsQ0FBbUMsTUFBTTtBQUNyQ1csZUFBTyxDQUFDQyxPQUFSLENBQWdCQyxXQUFoQixDQUE0QjtBQUFDQyxpQkFBTyxFQUFFO0FBQVYsU0FBNUI7QUFDSCxPQUZEO0FBR0gsS0FkTDtBQWVJLFlBQVEsRUFBRSxNQUFNO0FBQ1pILGFBQU8sQ0FBQ0ksSUFBUixDQUFhQyxNQUFiLEdBQXNCaEIsSUFBdEIsQ0FBMkIsTUFBTTtBQUM3QmlCLGNBQU0sQ0FBQ0MsS0FBUDtBQUNILE9BRkQ7QUFHSCxLQW5CTDtBQW9CSSxzQkFBa0IsRUFBRSxNQUFNO0FBQ3RCUCxhQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLFdBQWhCLENBQTRCO0FBQUNDLGVBQU8sRUFBRTtBQUFWLE9BQTVCO0FBQ0g7QUF0QkwsSUFIUixFQTRCSzdDLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlc0MsYUFBZixFQUE4QkUsTUFBOUIsR0FBdUMsQ0FBdkMsSUFDRyxrRUFBQyxnRkFBRDtBQUNJLFNBQUssRUFBRTFDLDhFQUFVLENBQUMsK0JBQUQsQ0FEckI7QUFFSSxnQkFBWSxFQUFDLFNBRmpCO0FBR0ksaUJBQWEsRUFBQyxNQUhsQjtBQUlJLFlBQVEsRUFBRXdDLGFBSmQ7QUFLSSxvQkFBZ0IsRUFBRSxLQUx0QjtBQU1JLG9CQUFnQixFQUFFLEtBTnRCO0FBT0ksZ0JBQVksT0FQaEI7QUFVSSxzQkFBa0IsTUFWdEI7QUFXSSxrQkFBYyxFQUFFLENBQUNwQyxJQUFELEVBQU9FLE1BQVAsS0FBa0I7QUFDOUIyQixnRkFBZSxDQUFDN0IsSUFBRCxFQUFPRSxNQUFQLENBQWYsQ0FBOEIwQixJQUE5QixDQUFtQyxNQUFNO0FBQ3JDVyxlQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLFdBQWhCLENBQTRCO0FBQUNDLGlCQUFPLEVBQUU7QUFBVixTQUE1QjtBQUNILE9BRkQ7QUFHSCxLQWZMO0FBZ0JJLFlBQVEsRUFBRSxNQUFNO0FBQ1pILGFBQU8sQ0FBQ0ksSUFBUixDQUFhQyxNQUFiLEdBQXNCaEIsSUFBdEIsQ0FBMkIsTUFBTTtBQUM3QmlCLGNBQU0sQ0FBQ0MsS0FBUDtBQUNILE9BRkQ7QUFHSCxLQXBCTDtBQXFCSSxzQkFBa0IsRUFBRSxNQUFNO0FBQ3RCUCxhQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLFdBQWhCLENBQTRCO0FBQUNDLGVBQU8sRUFBRTtBQUFWLE9BQTVCO0FBQ0g7QUF2QkwsSUE3QlIsQ0FMSixDQURKO0FBZ0VILENBeEZEOztBQTBGZVosNkVBQWYsRSIsImZpbGUiOiI3LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIENvcHlyaWdodCAoQykgMjAyMiBTdXJmYm9hcmQgSG9sZGluZyBCLlYuIDxodHRwczovL3d3dy5zdGFydHBhZ2UuY29tPlxuICpcbiAqIFRoaXMgcHJvZ3JhbSBpcyBmcmVlIHNvZnR3YXJlOiB5b3UgY2FuIHJlZGlzdHJpYnV0ZSBpdCBhbmQvb3IgbW9kaWZ5XG4gKiBpdCB1bmRlciB0aGUgdGVybXMgb2YgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIGFzIHB1Ymxpc2hlZCBieVxuICogdGhlIEZyZWUgU29mdHdhcmUgRm91bmRhdGlvbiwgZWl0aGVyIHZlcnNpb24gMyBvZiB0aGUgTGljZW5zZSwgb3JcbiAqIChhdCB5b3VyIG9wdGlvbikgYW55IGxhdGVyIHZlcnNpb24uXG4gKlxuICogVGhpcyBwcm9ncmFtIGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXG4gKiBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxuICogTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxuICogR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cbiAqXG4gKiBZb3Ugc2hvdWxkIGhhdmUgcmVjZWl2ZWQgYSBjb3B5IG9mIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZVxuICogYWxvbmcgd2l0aCB0aGlzIHByb2dyYW0uICBJZiBub3QsIHNlZSA8aHR0cHM6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy8+LlxuICovXG52YXIgX19hd2FpdGVyID0gKHRoaXMgJiYgdGhpcy5fX2F3YWl0ZXIpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBfYXJndW1lbnRzLCBQLCBnZW5lcmF0b3IpIHtcbiAgICBmdW5jdGlvbiBhZG9wdCh2YWx1ZSkgeyByZXR1cm4gdmFsdWUgaW5zdGFuY2VvZiBQID8gdmFsdWUgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHZhbHVlKTsgfSk7IH1cbiAgICByZXR1cm4gbmV3IChQIHx8IChQID0gUHJvbWlzZSkpKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgZnVuY3Rpb24gZnVsZmlsbGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yLm5leHQodmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gc3RlcChyZXN1bHQpIHsgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBhZG9wdChyZXN1bHQudmFsdWUpLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7IH1cbiAgICAgICAgc3RlcCgoZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pKS5uZXh0KCkpO1xuICAgIH0pO1xufTtcbmV4cG9ydCBjb25zdCBnZXRUYWIgPSAoKSA9PiBfX2F3YWl0ZXIodm9pZCAwLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24qICgpIHtcbiAgICBjb25zdCBbdGFiXSA9IHlpZWxkIGJyb3dzZXIudGFicy5xdWVyeSh7XG4gICAgICAgIGFjdGl2ZTogdHJ1ZSxcbiAgICAgICAgY3VycmVudFdpbmRvdzogdHJ1ZVxuICAgIH0pO1xuICAgIHJldHVybiB0YWI7XG59KTtcbmNvbnN0IHNlbmRCYWRnZXJNZXNzYWdlID0gKG1lc3NhZ2UsIGV4dHJhKSA9PiBfX2F3YWl0ZXIodm9pZCAwLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24qICgpIHtcbiAgICBjb25zdCB7IGlkOiB0YWJJZCwgdXJsOiB0YWJVcmwgfSA9IHlpZWxkIGdldFRhYigpO1xuICAgIHJldHVybiB5aWVsZCBicm93c2VyLnJ1bnRpbWUuc2VuZE1lc3NhZ2UoT2JqZWN0LmFzc2lnbih7IHR5cGU6IG1lc3NhZ2UsIHRhYklkLFxuICAgICAgICB0YWJVcmwgfSwgKGV4dHJhICE9PSBudWxsICYmIGV4dHJhICE9PSB2b2lkIDAgPyBleHRyYSA6IHt9KSkpO1xufSk7XG5leHBvcnQgY29uc3QgcmVzZXREYXRhID0gKCkgPT4gX19hd2FpdGVyKHZvaWQgMCwgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uKiAoKSB7XG4gICAgY29uc3QgeyBpZDogdGFiSWQsIHVybDogdGFiVXJsIH0gPSB5aWVsZCBnZXRUYWIoKTtcbiAgICByZXR1cm4geWllbGQgYnJvd3Nlci5ydW50aW1lLnNlbmRNZXNzYWdlKHtcbiAgICAgICAgdHlwZTogJ3Jlc2V0RGF0YScsXG4gICAgICAgIHRhYklkLFxuICAgICAgICB0YWJVcmxcbiAgICB9KTtcbn0pO1xuZXhwb3J0IGNvbnN0IHJlbW92ZUFsbERhdGEgPSAoKSA9PiBfX2F3YWl0ZXIodm9pZCAwLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24qICgpIHtcbiAgICBjb25zdCB7IGlkOiB0YWJJZCwgdXJsOiB0YWJVcmwgfSA9IHlpZWxkIGdldFRhYigpO1xuICAgIHJldHVybiB5aWVsZCBicm93c2VyLnJ1bnRpbWUuc2VuZE1lc3NhZ2Uoe1xuICAgICAgICB0eXBlOiAncmVtb3ZlQWxsRGF0YScsXG4gICAgICAgIHRhYklkLFxuICAgICAgICB0YWJVcmxcbiAgICB9KTtcbn0pO1xuZXhwb3J0IGNvbnN0IGFjdGl2YXRlT25TaXRlID0gKCkgPT4gX19hd2FpdGVyKHZvaWQgMCwgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uKiAoKSB7XG4gICAgY29uc29sZS5sb2coJ3RyeWluZyB0byBhY3RpdmF0ZScpO1xuICAgIGNvbnN0IHsgaWQ6IHRhYklkLCB1cmw6IHRhYlVybCB9ID0geWllbGQgZ2V0VGFiKCk7XG4gICAgY29uc29sZS5sb2coYCR7dGFiSWR9LCAke3RhYlVybH1gKTtcbiAgICByZXR1cm4geWllbGQgYnJvd3Nlci5ydW50aW1lLnNlbmRNZXNzYWdlKHtcbiAgICAgICAgdHlwZTogJ2FjdGl2YXRlT25TaXRlJyxcbiAgICAgICAgdGFiSWQsXG4gICAgICAgIHRhYlVybFxuICAgIH0pO1xufSk7XG5leHBvcnQgY29uc3QgZGVhY3RpdmF0ZU9uU2l0ZSA9ICgpID0+IF9fYXdhaXRlcih2b2lkIDAsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiogKCkge1xuICAgIGNvbnN0IHsgaWQ6IHRhYklkLCB1cmw6IHRhYlVybCB9ID0geWllbGQgZ2V0VGFiKCk7XG4gICAgcmV0dXJuIHlpZWxkIGJyb3dzZXIucnVudGltZS5zZW5kTWVzc2FnZSh7XG4gICAgICAgIHR5cGU6ICdkZWFjdGl2YXRlT25TaXRlJyxcbiAgICAgICAgdGFiSWQsXG4gICAgICAgIHRhYlVybFxuICAgIH0pO1xufSk7XG5leHBvcnQgY29uc3QgZ2V0UHJpdmFjeURhdGEgPSAoKSA9PiBfX2F3YWl0ZXIodm9pZCAwLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24qICgpIHtcbiAgICBjb25zdCBkYXRhID0geWllbGQgc2VuZEJhZGdlck1lc3NhZ2UoJ2dldFBvcHVwRGF0YScpO1xuICAgIHJldHVybiBkYXRhO1xufSk7XG5leHBvcnQgY29uc3QgZ2V0UHJpdmFjeU9wdGlvbnMgPSAoKSA9PiBfX2F3YWl0ZXIodm9pZCAwLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24qICgpIHtcbiAgICBjb25zdCBkYXRhID0geWllbGQgc2VuZEJhZGdlck1lc3NhZ2UoJ2dldE9wdGlvbnNEYXRhJyk7XG4gICAgcmV0dXJuIGRhdGE7XG59KTtcbmV4cG9ydCBjb25zdCBzYXZlUG9wdXBUb2dnbGUgPSAob3JpZ2luLCBhY3Rpb24pID0+IF9fYXdhaXRlcih2b2lkIDAsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiogKCkge1xuICAgIHJldHVybiB5aWVsZCBzZW5kQmFkZ2VyTWVzc2FnZSgnc2F2ZVBvcHVwVG9nZ2xlJywge1xuICAgICAgICBvcmlnaW4sXG4gICAgICAgIGFjdGlvblxuICAgIH0pO1xufSk7XG5leHBvcnQgY29uc3QgcmV2ZXJ0RG9tYWluQ29udHJvbCA9IChvcmlnaW4pID0+IF9fYXdhaXRlcih2b2lkIDAsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiogKCkge1xuICAgIHJldHVybiB5aWVsZCBzZW5kQmFkZ2VyTWVzc2FnZSgncmV2ZXJ0RG9tYWluQ29udHJvbCcsIHsgb3JpZ2luIH0pO1xufSk7XG5leHBvcnQgY29uc3QgdXBkYXRlU2V0dGluZ3MgPSAoc2V0dGluZ3MpID0+IF9fYXdhaXRlcih2b2lkIDAsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiogKCkge1xuICAgIHJldHVybiB5aWVsZCBicm93c2VyLnJ1bnRpbWUuc2VuZE1lc3NhZ2Uoe1xuICAgICAgICB0eXBlOiAndXBkYXRlU2V0dGluZ3MnLFxuICAgICAgICBkYXRhOiBzZXR0aW5nc1xuICAgIH0pO1xufSk7XG5leHBvcnQgY29uc3QgdXBkYXRlUHJpdmFjeU92ZXJyaWRlID0gKHNldHRpbmdzKSA9PiBfX2F3YWl0ZXIodm9pZCAwLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24qICgpIHtcbiAgICB5aWVsZCB1cGRhdGVTZXR0aW5ncyhzZXR0aW5ncyk7XG4gICAgcmV0dXJuIHlpZWxkIGJyb3dzZXIucnVudGltZS5zZW5kTWVzc2FnZSh7XG4gICAgICAgIHR5cGU6ICdzZXRQcml2YWN5T3ZlcnJpZGUnXG4gICAgfSk7XG59KTtcbmV4cG9ydCBjb25zdCBnZXRNYW5pZmVzdEZyYWdtZW50UGF0aCA9ICgpID0+IHtcbiAgICByZXR1cm4gYG1hbmlmZXN0LWZyYWdtZW50Lmpzb25gO1xufTtcbmV4cG9ydCBjb25zdCBnZXRDb3B5YWJsZXNQYXRocyA9ICgpID0+IHtcbiAgICBjb25zdCBTUkNfUEFUSCA9ICdzcmMnO1xuICAgIGNvbnN0IGNvcHlhYmxlcyA9IFtgJHtTUkNfUEFUSH0vZGF0YWAsIGAke1NSQ19QQVRIfS9qc2AsIGAke1NSQ19QQVRIfS9saWJgLCAnc3AtanMnXTtcbiAgICByZXR1cm4gY29weWFibGVzO1xufTtcbiIsImltcG9ydCBSZWFjdCwge0ZDLCBDaGFuZ2VFdmVudCwgdXNlU3RhdGV9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7Y3NzLCBTZXJpYWxpemVkU3R5bGVzfSBmcm9tICdAZW1vdGlvbi9jb3JlJztcbmltcG9ydCB7QWN0aW9ucywgcmV2ZXJ0RG9tYWluQ29udHJvbCwgc2F2ZVBvcHVwVG9nZ2xlfSBmcm9tICdzcC1wcml2YWN5JztcblxuaW1wb3J0IHtnZXRNZXNzYWdlfSBmcm9tICcuLi8uLi91dGlscy9sb2NhbGl6YXRpb24nO1xuaW1wb3J0IHtUcmFja2VySW5mbywgVHJhY2tlcnN9IGZyb20gJy4uLy4uL3ByaXZhY3kgdG9vbHMvdHJhY2tlcnMnO1xuXG50eXBlIFRyYWNrZXJTZXR0aW5nc1Byb3BzID0ge1xuICAgIHRpdGxlPzogc3RyaW5nO1xuICAgIGhlaWdodD86IG51bWJlcjtcbiAgICB0cmFja2VyczogVHJhY2tlcnM7XG4gICAgbGFiZWxCZ0NvbG9yPzogc3RyaW5nO1xuICAgIGxhYmVsRm9udFNpemU/OiBzdHJpbmc7XG4gICAgbENvbFdpZHRoUGVyY2VudDogbnVtYmVyO1xuICAgIHJDb2xXaWR0aFBlcmNlbnQ6IG51bWJlcjtcbiAgICBhbHRlcm5hdGVSb3dDb2xvcnM/OiBib29sZWFuO1xuICAgIGNzc092ZXJyaWRlcz86IFNlcmlhbGl6ZWRTdHlsZXM7XG4gICAgb25BY3Rpb25DaGFuZ2U6IChob3N0OiBzdHJpbmcsIGFjdGlvbjogQWN0aW9ucykgPT4gdm9pZDtcbiAgICBvblJldmVydDogKCkgPT4gdm9pZDtcbiAgICBvbkNvbmZpcm1TaXRlQnJlYWs6ICgpID0+IHZvaWQ7XG59O1xuXG5leHBvcnQgY29uc3QgVHJhY2tlclNldHRpbmdzOiBGQzxUcmFja2VyU2V0dGluZ3NQcm9wcz4gPSAoe1xuICAgIHRyYWNrZXJzLFxuICAgIHRpdGxlLFxuICAgIGhlaWdodCxcbiAgICBsYWJlbEJnQ29sb3IsXG4gICAgbGFiZWxGb250U2l6ZSxcbiAgICBsQ29sV2lkdGhQZXJjZW50LFxuICAgIHJDb2xXaWR0aFBlcmNlbnQsXG4gICAgYWx0ZXJuYXRlUm93Q29sb3JzLFxuICAgIGNzc092ZXJyaWRlcyxcbiAgICBvbkFjdGlvbkNoYW5nZSxcbiAgICBvblJldmVydCxcbiAgICBvbkNvbmZpcm1TaXRlQnJlYWtcbn0pID0+IHtcbiAgICBjb25zdCB0cmFja2VyU2V0dGluZ3NDc3MgPSBjc3NgXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGVlMGY3O1xuICAgICAgICBmb250LWZhbWlseTogJ0ludGVyJywgc2Fucy1zZXJpZjtcbiAgICAgICAgJHtoZWlnaHQgJiYgYGhlaWdodDogJHtoZWlnaHR9cHg7IG92ZXJmbG93LXk6IGF1dG87YH1cblxuICAgICAgICAmOmxhc3Qtb2YtdHlwZSB7XG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xuICAgICAgICB9XG4gICAgYDtcblxuICAgIGNvbnN0IHRpdGxlQ3NzID0gY3NzYFxuICAgICAgICBjb2xvcjogIzY1NzNmZjtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdJbnRlcicsIHNhbnMtc2VyaWY7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgICAgIHBhZGRpbmc6IDEycHggOHB4O1xuICAgIGA7XG5cbiAgICBjb25zdCBsYWJlbFJvd0NzcyA9IGNzc2BcbiAgICAgICAgYmFja2dyb3VuZDogJHtsYWJlbEJnQ29sb3IgfHwgJ3RyYW5zcGFyZW50J307XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZvbnQtc2l6ZTogJHtsYWJlbEZvbnRTaXplIHx8ICcxNHB4J307XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDZweDtcbiAgICAgICAgcGFkZGluZzogOHB4O1xuICAgICAgICBwb3NpdGlvbjogc3RpY2t5O1xuICAgICAgICB6LWluZGV4OiAxO1xuICAgICAgICB0b3A6IDA7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgICAgYmFja2dyb3VuZDogI2YyZjNmZjtcbiAgICBgO1xuXG4gICAgY29uc3QgZG9tYWluQ29sQ3NzID0gY3NzYFxuICAgICAgICBtaW4td2lkdGg6IDA7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICB3aWR0aDogJHtsQ29sV2lkdGhQZXJjZW50fSU7XG4gICAgYDtcblxuICAgIGNvbnN0IG9wdGlvbnNDb2xDc3MgPSBjc3NgXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIHdpZHRoOiAke3JDb2xXaWR0aFBlcmNlbnR9JTtcbiAgICBgO1xuXG4gICAgY29uc3Qgb3B0aW9uc0xhYmVsQ3NzID0gY3NzYFxuICAgICAgICB3aGl0ZS1zcGFjZTogcHJlLXdyYXA7XG4gICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLyAzKTtcbiAgICBgO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPGRpdiBjc3M9e1t0cmFja2VyU2V0dGluZ3NDc3MsIGNzc092ZXJyaWRlcyB8fCBjc3NgYF19PlxuICAgICAgICAgICAge3RpdGxlICYmIDxkaXYgY3NzPXt0aXRsZUNzc30+e3RpdGxlfTwvZGl2Pn1cbiAgICAgICAgICAgIDxkaXYgY3NzPXtsYWJlbFJvd0Nzc30+XG4gICAgICAgICAgICAgICAgPGRpdiBjc3M9e2RvbWFpbkNvbENzc30+e2dldE1lc3NhZ2UoJ3BvcHVwTWFuYWdlVHJhY2tlcnNEb21haW5MYWJlbCcpfTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY3NzPXtvcHRpb25zQ29sQ3NzfT5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjc3M9e29wdGlvbnNMYWJlbENzc30+e2dldE1lc3NhZ2UoJ3BvcHVwTWFuYWdlVHJhY2tlcnNCbG9ja0FsbExhYmVsJyl9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY3NzPXtvcHRpb25zTGFiZWxDc3N9PntnZXRNZXNzYWdlKCdwb3B1cE1hbmFnZVRyYWNrZXJzQmxvY2tDb29raWVzTGFiZWwnKX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjc3M9e29wdGlvbnNMYWJlbENzc30+e2dldE1lc3NhZ2UoJ3BvcHVwTWFuYWdlVHJhY2tlcnNBbGxvd0FsbExhYmVsJyl9PC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIHtPYmplY3QuZW50cmllcyh0cmFja2VycylcbiAgICAgICAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgQHR5cGVzY3JpcHQtZXNsaW50L25vLXVudXNlZC12YXJzXG4gICAgICAgICAgICAgICAgLmZpbHRlcigoW2hvc3QsIGluZm9dKSA9PiBpbmZvLmFjdGlvbiAhPT0gJ25vYWN0aW9uJylcbiAgICAgICAgICAgICAgICAubWFwKChbaG9zdCwgaW5mb10sIGkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxUcmFja2VyUm93XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaG9zdD17aG9zdH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmZvPXtpbmZvfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluZGV4PXthbHRlcm5hdGVSb3dDb2xvcnMgPyBpIDogdW5kZWZpbmVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxDb2xXaWR0aFBlcmNlbnQ9e2xDb2xXaWR0aFBlcmNlbnR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgckNvbFdpZHRoUGVyY2VudD17ckNvbFdpZHRoUGVyY2VudH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkFjdGlvbkNoYW5nZT17b25BY3Rpb25DaGFuZ2V9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25SZXZlcnQ9e29uUmV2ZXJ0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ29uZmlybVNpdGVCcmVhaz17b25Db25maXJtU2l0ZUJyZWFrfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICB9KX1cbiAgICAgICAgPC9kaXY+XG4gICAgKTtcbn07XG5cbnR5cGUgVHJhY2tlclJvd1Byb3BzID0ge1xuICAgIGhvc3Q6IHN0cmluZztcbiAgICBpbmZvOiBUcmFja2VySW5mbztcbiAgICBsQ29sV2lkdGhQZXJjZW50OiBudW1iZXI7XG4gICAgckNvbFdpZHRoUGVyY2VudDogbnVtYmVyO1xuICAgIG9uQWN0aW9uQ2hhbmdlOiAoaG9zdDogc3RyaW5nLCBhY3Rpb246IEFjdGlvbnMpID0+IHZvaWQ7XG4gICAgb25SZXZlcnQ6ICgpID0+IHZvaWQ7XG4gICAgb25Db25maXJtU2l0ZUJyZWFrOiAoKSA9PiB2b2lkO1xuICAgIGluZGV4PzogbnVtYmVyO1xufTtcblxuZXhwb3J0IGNvbnN0IFRyYWNrZXJSb3c6IEZDPFRyYWNrZXJSb3dQcm9wcz4gPSAoe1xuICAgIGhvc3QsXG4gICAgaW5mbyxcbiAgICBpbmRleCxcbiAgICBsQ29sV2lkdGhQZXJjZW50LFxuICAgIHJDb2xXaWR0aFBlcmNlbnQsXG4gICAgb25BY3Rpb25DaGFuZ2UsXG4gICAgb25SZXZlcnQsXG4gICAgb25Db25maXJtU2l0ZUJyZWFrXG59KSA9PiB7XG4gICAgY29uc3QgW3Nob3dXYXJuaW5nLCBzZXRTaG93V2FybmluZ10gPSB1c2VTdGF0ZShmYWxzZSk7XG5cbiAgICBjb25zdCBvcHRpb25TZWxlY3RDc3MgPSBjc3NgXG4gICAgICAgIGFwcGVhcmFuY2U6IG5vbmU7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmYmZiZmQ7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjZWQxZGQ7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgaGVpZ2h0OiAxNnB4O1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIHdpZHRoOiAxNnB4O1xuXG4gICAgICAgICY6Zm9jdXMge1xuICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgfVxuICAgIGA7XG5cbiAgICBjb25zdCB0cmFja2VyUm93Q3NzID0gY3NzYFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgbWFyZ2luOiAxcHggMDtcbiAgICAgICAgcGFkZGluZzogMTBweCA4cHg7XG5cbiAgICAgICAgLyogT3ZlcnJpZGUgZmlyZWZveCByYWRpbyBidXR0b24gc3R5bGVzICovXG4gICAgICAgIGlucHV0W3R5cGU9J3JhZGlvJ10ge1xuICAgICAgICAgICAgJHtvcHRpb25TZWxlY3RDc3N9O1xuICAgICAgICB9XG4gICAgYDtcblxuICAgIGNvbnN0IG9wdGlvbnNDb2xDc3MgPSBjc3NgXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIHdpZHRoOiAke3JDb2xXaWR0aFBlcmNlbnR9JTtcbiAgICBgO1xuXG4gICAgY29uc3QgZG9tYWluQ29sQ3NzID0gY3NzYFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBtaW4td2lkdGg6IDA7XG4gICAgICAgIHdpZHRoOiAke2xDb2xXaWR0aFBlcmNlbnR9JTtcbiAgICBgO1xuXG4gICAgY29uc3Qgb3B0aW9uU2VsZWN0RmlsbENzcyA9IGNzc2BcbiAgICAgICAgYmFja2dyb3VuZDogJHtzaG93V2FybmluZyB8fCAoaW5mby5hY3Rpb24gPT09ICd1c2VyX2Jsb2NrJyAmJiBpbmZvLmNvb2tpZUJsb2NrbGlzdGVkKSA/ICcjRUI1NzU3JyA6ICcjNjU3M2ZmJ307XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgaGVpZ2h0OiAxNnB4O1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogMDtcbiAgICAgICAgd2lkdGg6IDE2cHg7XG4gICAgYDtcblxuICAgIGNvbnN0IHJldmVydEJ0bkNzcyA9IGNzc2BcbiAgICAgICAgYmFja2dyb3VuZDogJHtpbmZvLmFjdGlvbiA9PT0gJ3VzZXJfYmxvY2snICYmIGluZm8uY29va2llQmxvY2tsaXN0ZWQgPyAnI0VCNTc1NycgOiAnIzY1NzNmZid9O1xuICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgZmxleC1zaHJpbms6IDA7XG4gICAgICAgIGhlaWdodDogMTZweDtcbiAgICAgICAgbWFyZ2luOiAwIDhweCAwIDA7XG4gICAgICAgIG1hc2s6IHVybCgncmV2ZXJ0LWFycm93LnN2ZycpO1xuICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICB3aWR0aDogMTZweDtcblxuICAgICAgICAmOmZvY3VzIHtcbiAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XG4gICAgICAgIH1cbiAgICBgO1xuXG4gICAgY29uc3QgaXNVc2VyU2V0ID0gWyd1c2VyX2Jsb2NrJywgJ3VzZXJfYWxsb3cnLCAndXNlcl9jb29raWVibG9jayddLmluY2x1ZGVzKGluZm8uYWN0aW9uKTtcblxuICAgIGNvbnN0IHJlc29sdmVSb3dCZ0NvbG9yID0gKGFjdGl2ZTogYm9vbGVhbikgPT4ge1xuICAgICAgICBpZiAoYWN0aXZlKSB7XG4gICAgICAgICAgICBpZiAoaW5mby5hY3Rpb24gPT09ICd1c2VyX2Jsb2NrJyAmJiBpbmZvLmNvb2tpZUJsb2NrbGlzdGVkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICcjRkZFREVFJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAnI0YwRjFGRic7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoaW5kZXggIT09IHVuZGVmaW5lZCAmJiBpbmRleCAlIDIgPT09IDApIHtcbiAgICAgICAgICAgIHJldHVybiAnI2ZiZmJmZCc7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuICd0cmFuc3BhcmVudCc7XG4gICAgfTtcblxuICAgIGNvbnN0IG9uQ2hhbmdlID0gKGV2ZW50OiBDaGFuZ2VFdmVudDxIVE1MSW5wdXRFbGVtZW50PikgPT4ge1xuICAgICAgICBjb25zdCBhY3Rpb24gPSBldmVudC50YXJnZXQudmFsdWUgYXMgQWN0aW9ucztcblxuICAgICAgICBpZiAoc2hvd1dhcm5pbmcpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChpbmZvLmNvb2tpZUJsb2NrbGlzdGVkICYmIGFjdGlvbiA9PT0gJ2Jsb2NrJykge1xuICAgICAgICAgICAgc2V0U2hvd1dhcm5pbmcodHJ1ZSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBvbkFjdGlvbkNoYW5nZShob3N0LCBhY3Rpb24pO1xuICAgIH07XG5cbiAgICBjb25zdCBibG9ja1NlbGVjdGVkID0gWydibG9jaycsICd1c2VyX2Jsb2NrJ10uaW5jbHVkZXMoaW5mby5hY3Rpb24pIHx8IHNob3dXYXJuaW5nO1xuICAgIGNvbnN0IGNvb2tpZUJsb2NrU2VsZWN0ZWQgPSBbJ2Nvb2tpZWJsb2NrJywgJ3VzZXJfY29va2llYmxvY2snXS5pbmNsdWRlcyhpbmZvLmFjdGlvbikgJiYgIXNob3dXYXJuaW5nO1xuICAgIGNvbnN0IGFsbG93U2VsZWN0ZWQgPSBbJ2FsbG93JywgJ3VzZXJfYWxsb3cnLCAnZG50J10uaW5jbHVkZXMoaW5mby5hY3Rpb24pICYmICFzaG93V2FybmluZztcblxuICAgIHJldHVybiAoXG4gICAgICAgIDw+XG4gICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgY3NzPXtbXG4gICAgICAgICAgICAgICAgICAgIHRyYWNrZXJSb3dDc3MsXG4gICAgICAgICAgICAgICAgICAgIGNzc2BcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICR7cmVzb2x2ZVJvd0JnQ29sb3IoaXNVc2VyU2V0KX07XG4gICAgICAgICAgICAgICAgICAgIGBcbiAgICAgICAgICAgICAgICBdfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxkaXYgY3NzPXtkb21haW5Db2xDc3N9PlxuICAgICAgICAgICAgICAgICAgICB7aXNVc2VyU2V0ICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjc3M9e3JldmVydEJ0bkNzc31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwicmV2ZXJ0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldmVydERvbWFpbkNvbnRyb2woaG9zdCkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvblJldmVydCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICAgICAgICAgIGNzcz17Y3NzYFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgICAgICAgICAgICAgICAgICBgfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7aG9zdH1cbiAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY3NzPXtvcHRpb25zQ29sQ3NzfT5cbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsXG4gICAgICAgICAgICAgICAgICAgICAgICBodG1sRm9yPXtgJHtob3N0fWB9XG4gICAgICAgICAgICAgICAgICAgICAgICBjc3M9e2Nzc2BcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGB9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJyYWRpb1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17YmxvY2tTZWxlY3RlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lPXtob3N0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPVwiYmxvY2tcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjc3M9e29wdGlvblNlbGVjdENzc31cbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICB7YmxvY2tTZWxlY3RlZCAmJiA8c3BhbiBjc3M9e29wdGlvblNlbGVjdEZpbGxDc3N9IC8+fVxuICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICAgICAgICAgICAgICA8bGFiZWxcbiAgICAgICAgICAgICAgICAgICAgICAgIGh0bWxGb3I9e2Ake2hvc3R9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNzcz17Y3NzYFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgICAgICAgICAgYH1cbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInJhZGlvXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXtjb29raWVCbG9ja1NlbGVjdGVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9e2hvc3R9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCJjb29raWVibG9ja1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNzcz17b3B0aW9uU2VsZWN0Q3NzfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtjb29raWVCbG9ja1NlbGVjdGVkICYmIDxzcGFuIGNzcz17b3B0aW9uU2VsZWN0RmlsbENzc30gLz59XG4gICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbFxuICAgICAgICAgICAgICAgICAgICAgICAgaHRtbEZvcj17YCR7aG9zdH1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgY3NzPXtjc3NgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICAgICAgICAgICAgICBgfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwicmFkaW9cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e2FsbG93U2VsZWN0ZWQgJiYgIXNob3dXYXJuaW5nfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9e2hvc3R9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9XCJhbGxvd1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNzcz17b3B0aW9uU2VsZWN0Q3NzfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgIHthbGxvd1NlbGVjdGVkICYmICFzaG93V2FybmluZyAmJiA8c3BhbiBjc3M9e29wdGlvblNlbGVjdEZpbGxDc3N9IC8+fVxuICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgY3NzPXtbXG4gICAgICAgICAgICAgICAgICAgIHRyYWNrZXJSb3dDc3MsXG4gICAgICAgICAgICAgICAgICAgIGNzc2BcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNlYjU3NTc7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICAgICAgICAgICAgICAkeyFzaG93V2FybmluZyAmJiAnZGlzcGxheTogbm9uZSd9O1xuICAgICAgICAgICAgICAgICAgICBgXG4gICAgICAgICAgICAgICAgXX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgIGNzcz17Y3NzYFxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDYwJTtcbiAgICAgICAgICAgICAgICAgICAgYH1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIHtnZXRNZXNzYWdlKCdwb3B1cE1hbmFnZVRyYWNrZXJzV2FybmluZycpfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNldFNob3dXYXJuaW5nKGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNhdmVQb3B1cFRvZ2dsZShob3N0LCAnYmxvY2snKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNvbmZpcm1TaXRlQnJlYWsoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICBjc3M9e2Nzc2BcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgICAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAyNHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAmOmZvY3VzIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBgfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAge2dldE1lc3NhZ2UoJ3BvcHVwTWFuYWdlVHJhY2tlcnNXYXJuaW5nUHJvY2VlZCcpfVxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNldFNob3dXYXJuaW5nKGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgY3NzPXtjc3NgXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgJjpmb2N1cyB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgYH1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIHtnZXRNZXNzYWdlKCdwb3B1cE1hbmFnZVRyYWNrZXJzV2FybmluZ0NhbmNlbCcpfVxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvPlxuICAgICk7XG59O1xuIiwiaW1wb3J0IFJlYWN0LCB7RkN9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7Y3NzfSBmcm9tICdAZW1vdGlvbi9jb3JlJztcbmltcG9ydCB7dXNlU2VsZWN0b3J9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7c2F2ZVBvcHVwVG9nZ2xlfSBmcm9tICdzcC1wcml2YWN5JztcbmltcG9ydCBQZXJmZWN0U2Nyb2xsYmFyIGZyb20gJ3JlYWN0LXBlcmZlY3Qtc2Nyb2xsYmFyJztcbmltcG9ydCBzY3JvbGxiYXJTdHlsZSBmcm9tICdyZWFjdC1wZXJmZWN0LXNjcm9sbGJhci9kaXN0L2Nzcy9zdHlsZXMuY3NzJztcblxuaW1wb3J0IHtnZXRNZXNzYWdlfSBmcm9tICcuLi8uLi91dGlscy9sb2NhbGl6YXRpb24nO1xuaW1wb3J0IHt0YWJEYXRhU2VsZWN0b3J9IGZyb20gJy4uLy4uL3NlbGVjdG9ycy9pbmRleCc7XG5pbXBvcnQge1RyYWNrZXJzfSBmcm9tICcuLi8uLi9wcml2YWN5IHRvb2xzL3RyYWNrZXJzJztcbmltcG9ydCB7VHJhY2tlclNldHRpbmdzfSBmcm9tICcuLi9Db21tb24vVHJhY2tlclNldHRpbmdzJztcblxuY29uc3QgTWFuYWdlVHJhY2tlcnM6IEZDID0gKCkgPT4ge1xuICAgIGNvbnN0IHt0cmFja2Vyc30gPSB1c2VTZWxlY3Rvcih0YWJEYXRhU2VsZWN0b3IpO1xuXG4gICAgY29uc3QgbWFqb3JUcmFja2VyczogVHJhY2tlcnMgPSB7fTtcbiAgICBPYmplY3QuZW50cmllcyh0cmFja2VycykuZm9yRWFjaCgoW2hvc3QsIGluZm9dKSA9PiB7XG4gICAgICAgIGlmIChpbmZvLnR5cGUgPT09ICdtYWpvcicgJiYgIVsnYWxsb3cnLCAnZG50J10uaW5jbHVkZXMoaW5mby5hY3Rpb24pKSB7XG4gICAgICAgICAgICBtYWpvclRyYWNrZXJzW2hvc3RdID0gaW5mbztcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgY29uc3QgbWlub3JUcmFja2VyczogVHJhY2tlcnMgPSB7fTtcbiAgICBPYmplY3QuZW50cmllcyh0cmFja2VycykuZm9yRWFjaCgoW2hvc3QsIGluZm9dKSA9PiB7XG4gICAgICAgIGlmIChpbmZvLnR5cGUgPT09ICdtaW5vcicgJiYgIVsnYWxsb3cnLCAnZG50J10uaW5jbHVkZXMoaW5mby5hY3Rpb24pKSB7XG4gICAgICAgICAgICBtaW5vclRyYWNrZXJzW2hvc3RdID0gaW5mbztcbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgY29uc3QgdGl0bGVDc3MgPSBjc3NgXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWJlY2Y3O1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBmb250LXNpemU6IDIycHg7XG4gICAgICAgIHBhZGRpbmc6IDE2cHg7XG4gICAgYDtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxkaXZcbiAgICAgICAgICAgIGNzcz17Y3NzYFxuICAgICAgICAgICAgICAgICR7c2Nyb2xsYmFyU3R5bGV9XG4gICAgICAgICAgICBgfVxuICAgICAgICA+XG4gICAgICAgICAgICA8UGVyZmVjdFNjcm9sbGJhciBzdHlsZT17e2hlaWdodDogJzQ0NHB4J319PlxuICAgICAgICAgICAgICAgIDxkaXYgY3NzPXt0aXRsZUNzc30+e2dldE1lc3NhZ2UoJ3BvcHVwTWFuYWdlVHJhY2tlcnNUaXRsZScpfTwvZGl2PlxuICAgICAgICAgICAgICAgIHtPYmplY3QuZW50cmllcyhtYWpvclRyYWNrZXJzKS5sZW5ndGggPiAwICYmIChcbiAgICAgICAgICAgICAgICAgICAgPFRyYWNrZXJTZXR0aW5nc1xuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e2dldE1lc3NhZ2UoJ3BvcHVwTWFuYWdlVHJhY2tlcnNNYWpvclRpdGxlJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbEJnQ29sb3I9XCIjZmJmYmZkXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsRm9udFNpemU9XCIxMXB4XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYWNrZXJzPXttYWpvclRyYWNrZXJzfVxuICAgICAgICAgICAgICAgICAgICAgICAgbENvbFdpZHRoUGVyY2VudD17NDYuMzR9XG4gICAgICAgICAgICAgICAgICAgICAgICByQ29sV2lkdGhQZXJjZW50PXs1Ni4zNX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNzc092ZXJyaWRlcz17Y3NzYFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDAgOHB4IDE2cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBgfVxuICAgICAgICAgICAgICAgICAgICAgICAgb25BY3Rpb25DaGFuZ2U9eyhob3N0LCBhY3Rpb24pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzYXZlUG9wdXBUb2dnbGUoaG9zdCwgYWN0aW9uKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJvd3Nlci5ydW50aW1lLnNlbmRNZXNzYWdlKHttZXNzYWdlOiAnZ2V0UHJpdmFjeURhdGEnfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgb25SZXZlcnQ9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicm93c2VyLnRhYnMucmVsb2FkKCkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5jbG9zZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ29uZmlybVNpdGVCcmVhaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyb3dzZXIucnVudGltZS5zZW5kTWVzc2FnZSh7bWVzc2FnZTogJ2dldFByaXZhY3lEYXRhJ30pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIHtPYmplY3QuZW50cmllcyhtaW5vclRyYWNrZXJzKS5sZW5ndGggPiAwICYmIChcbiAgICAgICAgICAgICAgICAgICAgPFRyYWNrZXJTZXR0aW5nc1xuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e2dldE1lc3NhZ2UoJ3BvcHVwTWFuYWdlVHJhY2tlcnNNaW5vclRpdGxlJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbEJnQ29sb3I9XCIjZmJmYmZkXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsRm9udFNpemU9XCIxMXB4XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYWNrZXJzPXttaW5vclRyYWNrZXJzfVxuICAgICAgICAgICAgICAgICAgICAgICAgbENvbFdpZHRoUGVyY2VudD17NDYuMzR9XG4gICAgICAgICAgICAgICAgICAgICAgICByQ29sV2lkdGhQZXJjZW50PXs1Ni4zNX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNzc092ZXJyaWRlcz17Y3NzYFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDAgOHB4IDE2cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBgfVxuICAgICAgICAgICAgICAgICAgICAgICAgYWx0ZXJuYXRlUm93Q29sb3JzXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkFjdGlvbkNoYW5nZT17KGhvc3QsIGFjdGlvbikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNhdmVQb3B1cFRvZ2dsZShob3N0LCBhY3Rpb24pLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicm93c2VyLnJ1bnRpbWUuc2VuZE1lc3NhZ2Uoe21lc3NhZ2U6ICdnZXRQcml2YWN5RGF0YSd9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICBvblJldmVydD17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyb3dzZXIudGFicy5yZWxvYWQoKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LmNsb3NlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgb25Db25maXJtU2l0ZUJyZWFrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJvd3Nlci5ydW50aW1lLnNlbmRNZXNzYWdlKHttZXNzYWdlOiAnZ2V0UHJpdmFjeURhdGEnfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L1BlcmZlY3RTY3JvbGxiYXI+XG4gICAgICAgIDwvZGl2PlxuICAgICk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBNYW5hZ2VUcmFja2VycztcbiJdLCJzb3VyY2VSb290IjoiIn0=