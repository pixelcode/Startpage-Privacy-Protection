(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getDisplayCount; });
var getDisplayCount = (_ref) => {
  var {
    count,
    kStart = 1000,
    mStart = 1000000
  } = _ref;

  // Format millions
  if (count >= mStart) {
    return "".concat(Math.floor(count / 1000000), "M");
  } // Format thousands


  if (count >= kStart) {
    return "".concat(Math.floor(count / 1000), "k");
  }

  return count;
};

/***/ }),

/***/ 358:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function GreenCheck (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"12","cy":"12","r":"12","fill":"#29DDCC","key":0}),React.createElement("g",{"clipPath":"url(#clip0)","key":1},React.createElement("path",{"d":"M17.25 9.0057L15.0363 7.5L10.8871 12.9183L8.34677 11.1844L6.75 13.2719L11.4919 16.5V16.4886L11.504 16.5L17.25 9.0057Z","fill":"#202945"})),React.createElement("defs",{"key":2},React.createElement("clipPath",{"id":"clip0"},React.createElement("rect",{"width":"10.5","height":"9","fill":"white","transform":"translate(6.75 7.5)"})))]);
}

GreenCheck.defaultProps = {"width":"24","height":"24","viewBox":"0 0 24 24","fill":"none"};

module.exports = GreenCheck;

GreenCheck.default = GreenCheck;


/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.regexp.to-string.js
var es_regexp_to_string = __webpack_require__(178);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(0);

// EXTERNAL MODULE: ./node_modules/@emotion/core/dist/core.browser.esm.js + 5 modules
var core_browser_esm = __webpack_require__(2);

// EXTERNAL MODULE: ./node_modules/react-perfect-scrollbar/lib/index.js
var lib = __webpack_require__(335);
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// EXTERNAL MODULE: ./node_modules/react-perfect-scrollbar/dist/css/styles.css
var styles = __webpack_require__(336);
var styles_default = /*#__PURE__*/__webpack_require__.n(styles);

// EXTERNAL MODULE: ./node_modules/react-redux/es/index.js + 24 modules
var es = __webpack_require__(39);

// EXTERNAL MODULE: ./src/privacy-search/utils/localization.ts
var localization = __webpack_require__(6);

// EXTERNAL MODULE: ./src/privacy-search/selectors/index.ts
var selectors = __webpack_require__(54);

// EXTERNAL MODULE: ./src/privacy-search/utils/display.ts
var display = __webpack_require__(334);

// EXTERNAL MODULE: ./src/privacy-search/icons/green-check.svg
var green_check = __webpack_require__(358);
var green_check_default = /*#__PURE__*/__webpack_require__.n(green_check);

// CONCATENATED MODULE: ./src/privacy-search/components/Popup/PrivacyReport/TotalDisplay.tsx
function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }



 // @ts-expect-error



var totalDisplayCss =  true ? {
  name: "89in1o-totalDisplayCss",
  styles: "align-items:center;background:#ffffff;border:1px solid #ebecf7;border-radius:4px;display:flex;flex-direction:column;height:70px;justify-content:space-between;line-height:1;margin-bottom:12px;padding:24px 23px;position:relative;text-align:center;width:112px;;label:totalDisplayCss;"
} : undefined;
var countCss =  true ? {
  name: "41pl3-countCss",
  styles: "font-size:46px;font-weight:600;;label:countCss;"
} : undefined;
var subtitleCss =  true ? {
  name: "vi1uy2-subtitleCss",
  styles: "font-size:14px;font-weight:600;;label:subtitleCss;"
} : undefined;
var checkmarkCss =  true ? {
  name: "1pgct0c-checkmarkCss",
  styles: "align-items:center;height:24px;width:24px;position:absolute;bottom:-12px;;label:checkmarkCss;"
} : undefined;
var TotalDisplay = (_ref) => {
  var {
    subtitle,
    count
  } = _ref;
  return Object(core_browser_esm["b" /* jsx */])("div", {
    css: totalDisplayCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: countCss
  }, Object(display["a" /* getDisplayCount */])({
    count,
    kStart: 10000
  })), Object(core_browser_esm["b" /* jsx */])("div", {
    css: subtitleCss
  }, subtitle), Object(core_browser_esm["b" /* jsx */])(green_check_default.a, {
    css: checkmarkCss
  }));
};
// CONCATENATED MODULE: ./src/privacy-search/components/Popup/PrivacyReport/CountsSummary.tsx
function CountsSummary_EMOTION_STRINGIFIED_CSS_ERROR_() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

/* eslint-disable @typescript-eslint/no-unused-vars */







var countsSummaryCss =  true ? {
  name: "1wwq802-countsSummaryCss",
  styles: "padding:16px 16px 0;;label:countsSummaryCss;"
} : undefined;
var innerContainerCss =  true ? {
  name: "1dujgs2-innerContainerCss",
  styles: "border:1px solid #ebecf7;border-radius:4px;background:#fbfbfd;padding:16px 7px;;label:innerContainerCss;"
} : undefined;
var titleCss =  true ? {
  name: "1owxzah-titleCss",
  styles: "color:#202945;font-weight:600;font-size:14px;line-height:16px;margin-bottom:12px;;label:titleCss;"
} : undefined;
var totalCountsCss =  true ? {
  name: "1e5zfsx-totalCountsCss",
  styles: "background:#fbfbfd;border-radius:4px;display:flex;height:132px;justify-content:space-between;;label:totalCountsCss;"
} : undefined;
var CountsSummary = () => {
  var metrics = Object(es["c" /* useSelector */])(selectors["a" /* metricsSelector */]);
  var blockedTrackerCount = metrics.blockedMajorTrackerCounts + metrics.blockedMinorTrackerCounts;
  return Object(core_browser_esm["b" /* jsx */])("div", {
    css: countsSummaryCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: innerContainerCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: titleCss
  }, Object(localization["b" /* getMessage */])('popupReportBlockedTitle')), Object(core_browser_esm["b" /* jsx */])("div", {
    css: totalCountsCss
  }, Object(core_browser_esm["b" /* jsx */])(TotalDisplay, {
    subtitle: Object(localization["b" /* getMessage */])('popupReportBlockedTrackersLabel'),
    count: blockedTrackerCount
  }), Object(core_browser_esm["b" /* jsx */])(TotalDisplay, {
    subtitle: Object(localization["b" /* getMessage */])('popupReportBlockedCookiesLabel'),
    count: metrics.blockedCookiesCounts
  }))));
};
// EXTERNAL MODULE: ./src/privacy-search/reducers/Metrics.ts
var Metrics = __webpack_require__(173);

// EXTERNAL MODULE: ./src/privacy-search/privacy tools/metrics.ts
var privacy_tools_metrics = __webpack_require__(43);

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.iterator.js
var es_array_iterator = __webpack_require__(21);

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.sort.js
var es_array_sort = __webpack_require__(359);

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.to-fixed.js
var es_number_to_fixed = __webpack_require__(337);

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom-collections.iterator.js
var web_dom_collections_iterator = __webpack_require__(29);

// EXTERNAL MODULE: ./node_modules/react-chartjs-2/dist/index.js
var dist = __webpack_require__(363);

// EXTERNAL MODULE: ./node_modules/chart.js/dist/chart.esm.js + 1 modules
var chart_esm = __webpack_require__(338);

// CONCATENATED MODULE: ./src/privacy-search/components/Popup/PrivacyReport/ScoreSummary.tsx





function ScoreSummary_EMOTION_STRINGIFIED_CSS_ERROR_() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

/* eslint-disable @typescript-eslint/no-unused-vars */

 // https://www.chartjs.org/docs/latest/charts/doughnut.html - chartjs docs
// https://github.com/reactchartjs/react-chartjs-2#docs - docs for react wrapper for chartjs


 // https://stackoverflow.com/a/70098543/5340646



 // https://stackoverflow.com/a/70098543/5340646


chart_esm["d" /* Chart */].register(chart_esm["a" /* ArcElement */]);
var scoreSummaryCss =  true ? {
  name: "k8zgzn-scoreSummaryCss",
  styles: "padding:16px 16px 0;;label:scoreSummaryCss;"
} : undefined;
var ScoreSummary_innerContainerCss =  true ? {
  name: "1dujgs2-innerContainerCss",
  styles: "border:1px solid #ebecf7;border-radius:4px;background:#fbfbfd;padding:16px 7px;;label:innerContainerCss;"
} : undefined;
var ScoreSummary_titleCss =  true ? {
  name: "1owxzah-titleCss",
  styles: "color:#202945;font-weight:600;font-size:14px;line-height:16px;margin-bottom:12px;;label:titleCss;"
} : undefined;
var descriptionCss =  true ? {
  name: "feuagj-descriptionCss",
  styles: "color:#7f869f;font-size:14px;line-height:18px;margin-bottom:16px;;label:descriptionCss;"
} : undefined;
var chartContainerCss =  true ? {
  name: "c0jn3c-chartContainerCss",
  styles: "display:flex;;label:chartContainerCss;"
} : undefined;
var chartLegendCss =  true ? {
  name: "29vciz-chartLegendCss",
  styles: "display:flex;flex-direction:column;font-size:14px;justify-content:space-evenly;margin-left:12px;;label:chartLegendCss;"
} : undefined;
var scoreColors = {
  5: '#29DDCC',
  4: '#A4D411',
  3: '#FBC917',
  2: '#FF9839',
  1: '#EB5757'
};
var makeChartData = (scoreDistribution, totalScores) => {
  return Object.entries(scoreDistribution).filter((_ref2) => {
    var [score, val] = _ref2;
    return val > 0;
  }).map((_ref3) => {
    var [score, val] = _ref3;
    var value = totalScores !== 0 ? val / totalScores * 100 : 0;
    return {
      score,
      val: Math.round(value)
    };
  }).sort((a, b) => {
    return parseInt(b.score, 10) - parseInt(a.score, 10);
  });
};
var makeChartColors = (chartData, colors) => {
  return chartData.map((_ref4) => {
    var {
      score,
      val
    } = _ref4;

    if (val > 0) {
      return colors[score];
    }

    return '';
  }).filter(brush => brush);
};

var ScoreSummary_ref =  true ? {
  name: "1isxh66-ScoreSummary",
  styles: "align-items:center;display:flex;line-height:1;;label:ScoreSummary;"
} : undefined;

var ScoreSummary = () => {
  var metrics = Object(es["c" /* useSelector */])(selectors["a" /* metricsSelector */]);
  var {
    scoreDistribution
  } = metrics;
  var totalScores = Object.values(scoreDistribution).reduce((prev, next) => prev + next, 0);
  var chartData = makeChartData(scoreDistribution, totalScores);
  var chartColors = makeChartColors(chartData, scoreColors);
  var options = {
    legend: {
      display: false
    },
    elements: {
      arc: {
        borderWidth: 0
      }
    },
    tooltips: {
      enabled: false
    }
  };
  var data = {
    maintainAspectRatio: false,
    responsive: false,
    datasets: [{
      data: chartData.map((_ref5) => {
        var {
          val
        } = _ref5;
        return val;
      }),
      backgroundColor: chartColors,
      hoverBackgroundColor: chartColors
    }]
  };
  var descScores = Object.entries(scoreDistribution).map(s => s).sort((a, b) => {
    return parseInt(b[0], 10) - parseInt(a[0], 10);
  });
  return Object(core_browser_esm["b" /* jsx */])("div", {
    css: scoreSummaryCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: ScoreSummary_innerContainerCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: ScoreSummary_titleCss
  }, Object(localization["b" /* getMessage */])('popupReportScoreTitle')), Object(core_browser_esm["b" /* jsx */])("div", {
    css: descriptionCss
  }, Object(localization["b" /* getMessage */])('popupReportScoreDescription')), Object(core_browser_esm["b" /* jsx */])("div", {
    css: chartContainerCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    style: {
      height: '190px',
      width: '190px'
    }
  }, Object(core_browser_esm["b" /* jsx */])(dist["a" /* Doughnut */], {
    data: data,
    options: options
  })), Object(core_browser_esm["b" /* jsx */])("div", {
    css: chartLegendCss
  }, descScores.map((_ref6) => {
    var [score, count] = _ref6;
    var percent = totalScores !== 0 ? (count / totalScores * 100).toFixed() : 0;
    return Object(core_browser_esm["b" /* jsx */])("div", {
      css: ScoreSummary_ref
    }, Object(core_browser_esm["b" /* jsx */])("span", {
      css: /*#__PURE__*/Object(core_browser_esm["a" /* css */])("background:", scoreColors[score], ";border-radius:50%;height:14px;margin-right:8px;width:14px;;label:ScoreSummary;" + ( true ? "" : undefined))
    }), score, "\xA0:\xA0", Object(core_browser_esm["b" /* jsx */])("b", null, "".concat(percent, "%")));
  })))));
};
// CONCATENATED MODULE: ./src/privacy-search/components/Popup/PrivacyReport/TypesSummary.tsx


function TypesSummary_EMOTION_STRINGIFIED_CSS_ERROR_() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

/* eslint-disable @typescript-eslint/no-unused-vars */






var typesSummaryCss =  true ? {
  name: "1vr604d-typesSummaryCss",
  styles: "padding:16px 16px 0;;label:typesSummaryCss;"
} : undefined;
var TypesSummary_innerContainerCss =  true ? {
  name: "1dujgs2-innerContainerCss",
  styles: "border:1px solid #ebecf7;border-radius:4px;background:#fbfbfd;padding:16px 7px;;label:innerContainerCss;"
} : undefined;
var TypesSummary_titleCss =  true ? {
  name: "1owxzah-titleCss",
  styles: "color:#202945;font-weight:600;font-size:14px;line-height:16px;margin-bottom:12px;;label:titleCss;"
} : undefined;
var TypesSummary_descriptionCss =  true ? {
  name: "1q3lj7d-descriptionCss",
  styles: "color:#7f869f;font-size:14px;line-height:18px;margin-bottom:19px;;label:descriptionCss;"
} : undefined;
var trackerBarCss =  true ? {
  name: "bga08y-trackerBarCss",
  styles: "border-radius:8px;height:40px;margin-bottom:20px;overflow:hidden;position:relative;width:100%;;label:trackerBarCss;"
} : undefined;

var TypesSummary_ref =  true ? {
  name: "1v685qj-TypesSummary",
  styles: "display:flex;font-size:13.5px;justify-content:space-between;;label:TypesSummary;"
} : undefined;

var _ref2 =  true ? {
  name: "pt96dn-TypesSummary",
  styles: "display:flex;align-items:center;;label:TypesSummary;"
} : undefined;

var _ref3 =  true ? {
  name: "wlyweg-TypesSummary",
  styles: "background:#6573ff;border-radius:50%;display:block;height:14px;margin-right:4px;width:14px;;label:TypesSummary;"
} : undefined;

var _ref4 =  true ? {
  name: "pt96dn-TypesSummary",
  styles: "display:flex;align-items:center;;label:TypesSummary;"
} : undefined;

var _ref5 =  true ? {
  name: "i3z6ix-TypesSummary",
  styles: "background:#374677;border-radius:50%;display:block;height:14px;margin-right:4px;width:14px;;label:TypesSummary;"
} : undefined;

var TypesSummary = () => {
  var metrics = Object(es["c" /* useSelector */])(selectors["a" /* metricsSelector */]);
  var {
    blockedMajorTrackerCounts,
    blockedMinorTrackerCounts
  } = metrics;
  var blockedTrackerCount = blockedMajorTrackerCounts + blockedMinorTrackerCounts;
  var majorPercent = blockedTrackerCount !== 0 ? (blockedMajorTrackerCounts / blockedTrackerCount * 100).toFixed() : 0;
  var minorPercent = blockedTrackerCount !== 0 ? (blockedMinorTrackerCounts / blockedTrackerCount * 100).toFixed() : 0;
  var majorBarCss = /*#__PURE__*/Object(core_browser_esm["a" /* css */])("background:#6573ff;border-right:1px solid #fbfbfd;height:100%;left:0;margin-right:auto;position:absolute;top:0;width:", majorPercent, "%;z-index:1;;label:majorBarCss;" + ( true ? "" : undefined));
  var minorBarCss = /*#__PURE__*/Object(core_browser_esm["a" /* css */])("background:#374677;height:100%;margin-left:auto;position:absolute;right:0;top:0;width:", minorPercent, "%;;label:minorBarCss;" + ( true ? "" : undefined));
  return Object(core_browser_esm["b" /* jsx */])("div", {
    css: typesSummaryCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: TypesSummary_innerContainerCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: TypesSummary_titleCss
  }, Object(localization["b" /* getMessage */])('popupReportBreakdownTitle')), Object(core_browser_esm["b" /* jsx */])("div", {
    css: TypesSummary_descriptionCss
  }, Object(localization["b" /* getMessage */])('popupReportBreakdownDescription')), Object(core_browser_esm["b" /* jsx */])("div", {
    css: trackerBarCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: majorBarCss
  }), Object(core_browser_esm["b" /* jsx */])("div", {
    css: minorBarCss
  })), Object(core_browser_esm["b" /* jsx */])("div", {
    css: TypesSummary_ref
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: _ref2
  }, Object(core_browser_esm["b" /* jsx */])("span", {
    css: _ref3
  }), Object(localization["b" /* getMessage */])('popupReportBreakdownMajorLabel'), "\xA0:\xA0", Object(core_browser_esm["b" /* jsx */])("b", null, majorPercent, "%")), Object(core_browser_esm["b" /* jsx */])("div", {
    css: _ref4
  }, Object(core_browser_esm["b" /* jsx */])("span", {
    css: _ref5
  }), Object(localization["b" /* getMessage */])('popupReportBreakdownMinorLabel'), "\xA0:\xA0", Object(core_browser_esm["b" /* jsx */])("b", null, minorPercent, "%")))));
};
// CONCATENATED MODULE: ./src/privacy-search/components/Popup/PrivacyReport/index.tsx


function PrivacyReport_EMOTION_STRINGIFIED_CSS_ERROR_() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

/* eslint-disable @typescript-eslint/no-unused-vars */













var PrivacyReport_titleCss =  true ? {
  name: "ysslze-titleCss",
  styles: "border-bottom:1px solid #ebecf7;font-weight:600;font-size:22px;padding:16px;;label:titleCss;"
} : undefined;
var resetBtnCss =  true ? {
  name: "129x3hp-resetBtnCss",
  styles: "background:#ffffff;border:1px solid #6573ff;border-radius:4px;color:#6573ff;cursor:pointer;font-size:14px;font-weight:500;height:38px;margin-top:auto;transition:0.4s;width:100%;&:focus{outline:none;}&:disabled{color:lightgray;cursor:initial;border:1px solid lightgray;};label:resetBtnCss;"
} : undefined;
var disclaimerCss =  true ? {
  name: "14zdrm9-disclaimerCss",
  styles: "color:#7f869f;font-size:14px;line-height:18px;padding:16px 42px 24px;text-align:center;;label:disclaimerCss;"
} : undefined;

var PrivacyReport_ref =  true ? {
  name: "16fzzrj-PrivacyReport",
  styles: "padding:16px 16px 0;;label:PrivacyReport;"
} : undefined;

var PrivacyReport = () => {
  var metrics = Object(es["c" /* useSelector */])(selectors["a" /* metricsSelector */]); // TODO: Beef up this check (order of keys matters atm).

  var isInitialMetrics = Object.entries(metrics).toString() === Object.entries(Metrics["b" /* initialState */]).toString();
  return Object(core_browser_esm["b" /* jsx */])("div", {
    className: "privacy-report",
    css: /*#__PURE__*/Object(core_browser_esm["a" /* css */])(styles_default.a, ";label:PrivacyReport;" + ( true ? "" : undefined))
  }, Object(core_browser_esm["b" /* jsx */])(lib_default.a, {
    style: {
      height: '420px'
    }
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: PrivacyReport_titleCss
  }, Object(localization["b" /* getMessage */])('popupReportTitle')), Object(core_browser_esm["b" /* jsx */])(ScoreSummary, null), Object(core_browser_esm["b" /* jsx */])(CountsSummary, null), Object(core_browser_esm["b" /* jsx */])(TypesSummary, null), Object(core_browser_esm["b" /* jsx */])("div", {
    css: PrivacyReport_ref
  }, Object(core_browser_esm["b" /* jsx */])("button", {
    css: resetBtnCss,
    type: "submit",
    onClick: () => {
      Object(privacy_tools_metrics["d" /* resetMetrics */])();
    },
    disabled: isInitialMetrics
  }, Object(localization["b" /* getMessage */])('popupReportResetBtn'))), Object(core_browser_esm["b" /* jsx */])("div", {
    css: disclaimerCss
  }, Object(localization["b" /* getMessage */])('popupReportDisclaimer'))));
};

/* harmony default export */ var Popup_PrivacyReport = __webpack_exports__["default"] = (PrivacyReport);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvdXRpbHMvZGlzcGxheS50cyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvZ3JlZW4tY2hlY2suc3ZnIiwid2VicGFjazovLy8uL3NyYy9wcml2YWN5LXNlYXJjaC9jb21wb25lbnRzL1BvcHVwL1ByaXZhY3lSZXBvcnQvVG90YWxEaXNwbGF5LnRzeCIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvY29tcG9uZW50cy9Qb3B1cC9Qcml2YWN5UmVwb3J0L0NvdW50c1N1bW1hcnkudHN4Iiwid2VicGFjazovLy8uL3NyYy9wcml2YWN5LXNlYXJjaC9jb21wb25lbnRzL1BvcHVwL1ByaXZhY3lSZXBvcnQvU2NvcmVTdW1tYXJ5LnRzeCIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvY29tcG9uZW50cy9Qb3B1cC9Qcml2YWN5UmVwb3J0L1R5cGVzU3VtbWFyeS50c3giLCJ3ZWJwYWNrOi8vLy4vc3JjL3ByaXZhY3ktc2VhcmNoL2NvbXBvbmVudHMvUG9wdXAvUHJpdmFjeVJlcG9ydC9pbmRleC50c3giXSwibmFtZXMiOlsiZ2V0RGlzcGxheUNvdW50IiwiY291bnQiLCJrU3RhcnQiLCJtU3RhcnQiLCJNYXRoIiwiZmxvb3IiLCJ0b3RhbERpc3BsYXlDc3MiLCJjb3VudENzcyIsInN1YnRpdGxlQ3NzIiwiY2hlY2ttYXJrQ3NzIiwiVG90YWxEaXNwbGF5Iiwic3VidGl0bGUiLCJjb3VudHNTdW1tYXJ5Q3NzIiwiaW5uZXJDb250YWluZXJDc3MiLCJ0aXRsZUNzcyIsInRvdGFsQ291bnRzQ3NzIiwiQ291bnRzU3VtbWFyeSIsIm1ldHJpY3MiLCJ1c2VTZWxlY3RvciIsIm1ldHJpY3NTZWxlY3RvciIsImJsb2NrZWRUcmFja2VyQ291bnQiLCJibG9ja2VkTWFqb3JUcmFja2VyQ291bnRzIiwiYmxvY2tlZE1pbm9yVHJhY2tlckNvdW50cyIsImdldE1lc3NhZ2UiLCJibG9ja2VkQ29va2llc0NvdW50cyIsIkNoYXJ0IiwicmVnaXN0ZXIiLCJBcmNFbGVtZW50Iiwic2NvcmVTdW1tYXJ5Q3NzIiwiZGVzY3JpcHRpb25Dc3MiLCJjaGFydENvbnRhaW5lckNzcyIsImNoYXJ0TGVnZW5kQ3NzIiwic2NvcmVDb2xvcnMiLCJtYWtlQ2hhcnREYXRhIiwic2NvcmVEaXN0cmlidXRpb24iLCJ0b3RhbFNjb3JlcyIsIk9iamVjdCIsImVudHJpZXMiLCJmaWx0ZXIiLCJzY29yZSIsInZhbCIsIm1hcCIsInZhbHVlIiwicm91bmQiLCJzb3J0IiwiYSIsImIiLCJwYXJzZUludCIsIm1ha2VDaGFydENvbG9ycyIsImNoYXJ0RGF0YSIsImNvbG9ycyIsImJydXNoIiwiU2NvcmVTdW1tYXJ5IiwidmFsdWVzIiwicmVkdWNlIiwicHJldiIsIm5leHQiLCJjaGFydENvbG9ycyIsIm9wdGlvbnMiLCJsZWdlbmQiLCJkaXNwbGF5IiwiZWxlbWVudHMiLCJhcmMiLCJib3JkZXJXaWR0aCIsInRvb2x0aXBzIiwiZW5hYmxlZCIsImRhdGEiLCJtYWludGFpbkFzcGVjdFJhdGlvIiwicmVzcG9uc2l2ZSIsImRhdGFzZXRzIiwiYmFja2dyb3VuZENvbG9yIiwiaG92ZXJCYWNrZ3JvdW5kQ29sb3IiLCJkZXNjU2NvcmVzIiwicyIsImhlaWdodCIsIndpZHRoIiwicGVyY2VudCIsInRvRml4ZWQiLCJjc3MiLCJ0eXBlc1N1bW1hcnlDc3MiLCJ0cmFja2VyQmFyQ3NzIiwiVHlwZXNTdW1tYXJ5IiwibWFqb3JQZXJjZW50IiwibWlub3JQZXJjZW50IiwibWFqb3JCYXJDc3MiLCJtaW5vckJhckNzcyIsInJlc2V0QnRuQ3NzIiwiZGlzY2xhaW1lckNzcyIsIlByaXZhY3lSZXBvcnQiLCJpc0luaXRpYWxNZXRyaWNzIiwidG9TdHJpbmciLCJpbml0aWFsTWV0cmljcyIsInNjcm9sbGJhclN0eWxlIiwicmVzZXRNZXRyaWNzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFNQTtBQUFPLElBQU1BLGVBQWUsR0FBRyxVQUFvRTtBQUFBLE1BQW5FO0FBQUNDLFNBQUQ7QUFBUUMsVUFBTSxHQUFHLElBQWpCO0FBQXVCQyxVQUFNLEdBQUc7QUFBaEMsR0FBbUU7O0FBQy9GO0FBQ0EsTUFBSUYsS0FBSyxJQUFJRSxNQUFiLEVBQXFCO0FBQ2pCLHFCQUFVQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0osS0FBSyxHQUFHLE9BQW5CLENBQVY7QUFDSCxHQUo4RixDQU0vRjs7O0FBQ0EsTUFBSUEsS0FBSyxJQUFJQyxNQUFiLEVBQXFCO0FBQ2pCLHFCQUFVRSxJQUFJLENBQUNDLEtBQUwsQ0FBV0osS0FBSyxHQUFHLElBQW5CLENBQVY7QUFDSDs7QUFFRCxTQUFPQSxLQUFQO0FBQ0gsQ0FaTSxDOzs7Ozs7O0FDTlAsWUFBWSxtQkFBTyxDQUFDLENBQU87O0FBRTNCO0FBQ0EsMEVBQTBFLHNEQUFzRCwyQkFBMkIsaUNBQWlDLDZCQUE2Qiw2SUFBNkksK0JBQStCLFFBQVEsaUNBQWlDLGFBQWEsNkJBQTZCLDZFQUE2RTtBQUNyaUI7O0FBRUEsMkJBQTJCOztBQUUzQjs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVkE7QUFDQTtDQUlBOztBQUNBOztBQUVBLElBQU1LLGVBQWUsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFyQjtBQWlCQSxJQUFNQyxRQUFRLEdBQUc7QUFBSDtBQUFBO0FBQUEsYUFBZDtBQUtBLElBQU1DLFdBQVcsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFqQjtBQUtBLElBQU1DLFlBQVksR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFsQjtBQWFPLElBQU1DLFlBQW1DLEdBQUcsVUFBdUI7QUFBQSxNQUF0QjtBQUFDQyxZQUFEO0FBQVdWO0FBQVgsR0FBc0I7QUFDdEUsU0FDSTtBQUFLLE9BQUcsRUFBRUs7QUFBVixLQUNJO0FBQUssT0FBRyxFQUFFQztBQUFWLEtBQXFCUCwwQ0FBZSxDQUFDO0FBQUNDLFNBQUQ7QUFBUUMsVUFBTSxFQUFFO0FBQWhCLEdBQUQsQ0FBcEMsQ0FESixFQUVJO0FBQUssT0FBRyxFQUFFTTtBQUFWLEtBQXdCRyxRQUF4QixDQUZKLEVBR0ksd0NBQUMscUJBQUQ7QUFBWSxPQUFHLEVBQUVGO0FBQWpCLElBSEosQ0FESjtBQU9ILENBUk0sQzs7OztBQ2hEUDtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7QUFFQSxJQUFNRyxnQkFBZ0IsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUF0QjtBQUlBLElBQU1DLGlCQUFpQixHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQXZCO0FBT0EsSUFBTUMsUUFBUSxHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQWQ7QUFRQSxJQUFNQyxjQUFjLEdBQUc7QUFBSDtBQUFBO0FBQUEsYUFBcEI7QUFRTyxJQUFNQyxhQUFpQixHQUFHLE1BQU07QUFDbkMsTUFBTUMsT0FBTyxHQUFHQyxpQ0FBVyxDQUFDQyxvQ0FBRCxDQUEzQjtBQUNBLE1BQU1DLG1CQUFtQixHQUFHSCxPQUFPLENBQUNJLHlCQUFSLEdBQW9DSixPQUFPLENBQUNLLHlCQUF4RTtBQUVBLFNBQ0k7QUFBSyxPQUFHLEVBQUVWO0FBQVYsS0FDSTtBQUFLLE9BQUcsRUFBRUM7QUFBVixLQUNJO0FBQUssT0FBRyxFQUFFQztBQUFWLEtBQXFCUywwQ0FBVSxDQUFDLHlCQUFELENBQS9CLENBREosRUFFSTtBQUFLLE9BQUcsRUFBRVI7QUFBVixLQUNJLHdDQUFDLFlBQUQ7QUFDSSxZQUFRLEVBQUVRLDBDQUFVLENBQUMsaUNBQUQsQ0FEeEI7QUFFSSxTQUFLLEVBQUVIO0FBRlgsSUFESixFQUtJLHdDQUFDLFlBQUQ7QUFDSSxZQUFRLEVBQUVHLDBDQUFVLENBQUMsZ0NBQUQsQ0FEeEI7QUFFSSxTQUFLLEVBQUVOLE9BQU8sQ0FBQ087QUFGbkIsSUFMSixDQUZKLENBREosQ0FESjtBQWlCSCxDQXJCTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwQ1A7QUFDQTtDQUVBO0FBQ0E7O0FBQ0E7Q0FFQTs7QUFDQTtBQUVBO0NBR0E7OztBQUNBQywwQkFBSyxDQUFDQyxRQUFOLENBQWVDLCtCQUFmO0FBRUEsSUFBTUMsZUFBZSxHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQXJCO0FBSUEsSUFBTWYsOEJBQWlCLEdBQUc7QUFBSDtBQUFBO0FBQUEsYUFBdkI7QUFPQSxJQUFNQyxxQkFBUSxHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQWQ7QUFRQSxJQUFNZSxjQUFjLEdBQUc7QUFBSDtBQUFBO0FBQUEsYUFBcEI7QUFPQSxJQUFNQyxpQkFBaUIsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUF2QjtBQUlBLElBQU1DLGNBQWMsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFwQjtBQTBDQSxJQUFNQyxXQUFxQixHQUFHO0FBQzFCLEtBQUcsU0FEdUI7QUFFMUIsS0FBRyxTQUZ1QjtBQUcxQixLQUFHLFNBSHVCO0FBSTFCLEtBQUcsU0FKdUI7QUFLMUIsS0FBRztBQUx1QixDQUE5QjtBQVFPLElBQU1DLGFBQWEsR0FBRyxDQUFDQyxpQkFBRCxFQUErQ0MsV0FBL0MsS0FBa0Y7QUFDM0csU0FBT0MsTUFBTSxDQUFDQyxPQUFQLENBQWVILGlCQUFmLEVBQ0ZJLE1BREUsQ0FDSztBQUFBLFFBQUMsQ0FBQ0MsS0FBRCxFQUFRQyxHQUFSLENBQUQ7QUFBQSxXQUFrQkEsR0FBRyxHQUFHLENBQXhCO0FBQUEsR0FETCxFQUVGQyxHQUZFLENBRUUsV0FBa0I7QUFBQSxRQUFqQixDQUFDRixLQUFELEVBQVFDLEdBQVIsQ0FBaUI7QUFDbkIsUUFBTUUsS0FBSyxHQUFHUCxXQUFXLEtBQUssQ0FBaEIsR0FBcUJLLEdBQUcsR0FBR0wsV0FBUCxHQUFzQixHQUExQyxHQUFnRCxDQUE5RDtBQUNBLFdBQU87QUFBQ0ksV0FBRDtBQUFRQyxTQUFHLEVBQUVwQyxJQUFJLENBQUN1QyxLQUFMLENBQVdELEtBQVg7QUFBYixLQUFQO0FBQ0gsR0FMRSxFQU1GRSxJQU5FLENBTUcsQ0FBQ0MsQ0FBRCxFQUFJQyxDQUFKLEtBQVU7QUFDWixXQUFPQyxRQUFRLENBQUNELENBQUMsQ0FBQ1AsS0FBSCxFQUFVLEVBQVYsQ0FBUixHQUF3QlEsUUFBUSxDQUFDRixDQUFDLENBQUNOLEtBQUgsRUFBVSxFQUFWLENBQXZDO0FBQ0gsR0FSRSxDQUFQO0FBU0gsQ0FWTTtBQVlBLElBQU1TLGVBQWUsR0FBRyxDQUFDQyxTQUFELEVBQXVCQyxNQUF2QixLQUFvRDtBQUMvRSxTQUFPRCxTQUFTLENBQ1hSLEdBREUsQ0FDRSxXQUFrQjtBQUFBLFFBQWpCO0FBQUNGLFdBQUQ7QUFBUUM7QUFBUixLQUFpQjs7QUFDbkIsUUFBSUEsR0FBRyxHQUFHLENBQVYsRUFBYTtBQUNULGFBQU9VLE1BQU0sQ0FBQ1gsS0FBRCxDQUFiO0FBQ0g7O0FBQ0QsV0FBTyxFQUFQO0FBQ0gsR0FORSxFQU9GRCxNQVBFLENBT01hLEtBQUQsSUFBV0EsS0FQaEIsQ0FBUDtBQVFILENBVE07Ozs7Ozs7QUFXQSxJQUFNQyxZQUFnQixHQUFHLE1BQU07QUFDbEMsTUFBTW5DLE9BQU8sR0FBR0MsaUNBQVcsQ0FBQ0Msb0NBQUQsQ0FBM0I7QUFDQSxNQUFNO0FBQUNlO0FBQUQsTUFBc0JqQixPQUE1QjtBQUVBLE1BQU1rQixXQUFXLEdBQUdDLE1BQU0sQ0FBQ2lCLE1BQVAsQ0FBY25CLGlCQUFkLEVBQWlDb0IsTUFBakMsQ0FBd0MsQ0FBQ0MsSUFBRCxFQUFPQyxJQUFQLEtBQWdCRCxJQUFJLEdBQUdDLElBQS9ELEVBQXFFLENBQXJFLENBQXBCO0FBRUEsTUFBTVAsU0FBb0IsR0FBR2hCLGFBQWEsQ0FBQ0MsaUJBQUQsRUFBb0JDLFdBQXBCLENBQTFDO0FBQ0EsTUFBTXNCLFdBQW1CLEdBQUdULGVBQWUsQ0FBQ0MsU0FBRCxFQUFZakIsV0FBWixDQUEzQztBQUVBLE1BQU0wQixPQUF1QixHQUFHO0FBQzVCQyxVQUFNLEVBQUU7QUFDSkMsYUFBTyxFQUFFO0FBREwsS0FEb0I7QUFJNUJDLFlBQVEsRUFBRTtBQUNOQyxTQUFHLEVBQUU7QUFDREMsbUJBQVcsRUFBRTtBQURaO0FBREMsS0FKa0I7QUFTNUJDLFlBQVEsRUFBRTtBQUNOQyxhQUFPLEVBQUU7QUFESDtBQVRrQixHQUFoQztBQWNBLE1BQU1DLElBQWlCLEdBQUc7QUFDdEJDLHVCQUFtQixFQUFFLEtBREM7QUFFdEJDLGNBQVUsRUFBRSxLQUZVO0FBR3RCQyxZQUFRLEVBQUUsQ0FDTjtBQUNJSCxVQUFJLEVBQUVqQixTQUFTLENBQUNSLEdBQVYsQ0FBYztBQUFBLFlBQUM7QUFBQ0Q7QUFBRCxTQUFEO0FBQUEsZUFBV0EsR0FBWDtBQUFBLE9BQWQsQ0FEVjtBQUVJOEIscUJBQWUsRUFBRWIsV0FGckI7QUFHSWMsMEJBQW9CLEVBQUVkO0FBSDFCLEtBRE07QUFIWSxHQUExQjtBQVlBLE1BQU1lLFVBQVUsR0FBR3BDLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlSCxpQkFBZixFQUNkTyxHQURjLENBQ1RnQyxDQUFELElBQU9BLENBREcsRUFFZDdCLElBRmMsQ0FFVCxDQUFDQyxDQUFELEVBQUlDLENBQUosS0FBVTtBQUNaLFdBQU9DLFFBQVEsQ0FBQ0QsQ0FBQyxDQUFDLENBQUQsQ0FBRixFQUFPLEVBQVAsQ0FBUixHQUFxQkMsUUFBUSxDQUFDRixDQUFDLENBQUMsQ0FBRCxDQUFGLEVBQU8sRUFBUCxDQUFwQztBQUNILEdBSmMsQ0FBbkI7QUFNQSxTQUNJO0FBQUssT0FBRyxFQUFFakI7QUFBVixLQUNJO0FBQUssT0FBRyxFQUFFZiw4QkFBaUJBO0FBQTNCLEtBQ0k7QUFBSyxPQUFHLEVBQUVDLHFCQUFRQTtBQUFsQixLQUFxQlMsMENBQVUsQ0FBQyx1QkFBRCxDQUEvQixDQURKLEVBRUk7QUFBSyxPQUFHLEVBQUVNO0FBQVYsS0FBMkJOLDBDQUFVLENBQUMsNkJBQUQsQ0FBckMsQ0FGSixFQUdJO0FBQUssT0FBRyxFQUFFTztBQUFWLEtBQ0k7QUFBSyxTQUFLLEVBQUU7QUFBQzRDLFlBQU0sRUFBRSxPQUFUO0FBQWtCQyxXQUFLLEVBQUU7QUFBekI7QUFBWixLQUNJLHdDQUFDLHdCQUFEO0FBQVUsUUFBSSxFQUFFVCxJQUFoQjtBQUFzQixXQUFPLEVBQUVSO0FBQS9CLElBREosQ0FESixFQUlJO0FBQUssT0FBRyxFQUFFM0I7QUFBVixLQUNLeUMsVUFBVSxDQUFDL0IsR0FBWCxDQUFlLFdBQW9CO0FBQUEsUUFBbkIsQ0FBQ0YsS0FBRCxFQUFRdEMsS0FBUixDQUFtQjtBQUNoQyxRQUFNMkUsT0FBTyxHQUFHekMsV0FBVyxLQUFLLENBQWhCLEdBQW9CLENBQUVsQyxLQUFLLEdBQUdrQyxXQUFULEdBQXdCLEdBQXpCLEVBQThCMEMsT0FBOUIsRUFBcEIsR0FBOEQsQ0FBOUU7QUFFQSxXQUNJO0FBQ0ksU0FBRztBQURQLE9BT0k7QUFDSSxTQUFHLGVBQUVDLHVDQUFGLGdCQUNlOUMsV0FBVyxDQUFDTyxLQUFELENBRDFCO0FBRFAsTUFQSixFQWdCS0EsS0FoQkwsZUFnQndCLDZEQUFPcUMsT0FBUCxPQWhCeEIsQ0FESjtBQW9CSCxHQXZCQSxDQURMLENBSkosQ0FISixDQURKLENBREo7QUF1Q0gsQ0FoRk0sQzs7Ozs7O0FDdkhQO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTs7QUFFQSxJQUFNRyxlQUFlLEdBQUc7QUFBSDtBQUFBO0FBQUEsYUFBckI7QUFJQSxJQUFNbEUsOEJBQWlCLEdBQUc7QUFBSDtBQUFBO0FBQUEsYUFBdkI7QUFPQSxJQUFNQyxxQkFBUSxHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQWQ7QUFRQSxJQUFNZSwyQkFBYyxHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQXBCO0FBT0EsSUFBTW1ELGFBQWEsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFuQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBU08sSUFBTUMsWUFBZ0IsR0FBRyxNQUFNO0FBQ2xDLE1BQU1oRSxPQUFPLEdBQUdDLGlDQUFXLENBQUNDLG9DQUFELENBQTNCO0FBQ0EsTUFBTTtBQUFDRSw2QkFBRDtBQUE0QkM7QUFBNUIsTUFBeURMLE9BQS9EO0FBQ0EsTUFBTUcsbUJBQW1CLEdBQUdDLHlCQUF5QixHQUFHQyx5QkFBeEQ7QUFFQSxNQUFNNEQsWUFBWSxHQUNkOUQsbUJBQW1CLEtBQUssQ0FBeEIsR0FBNEIsQ0FBRUMseUJBQXlCLEdBQUdELG1CQUE3QixHQUFvRCxHQUFyRCxFQUEwRHlELE9BQTFELEVBQTVCLEdBQWtHLENBRHRHO0FBRUEsTUFBTU0sWUFBWSxHQUNkL0QsbUJBQW1CLEtBQUssQ0FBeEIsR0FBNEIsQ0FBRUUseUJBQXlCLEdBQUdGLG1CQUE3QixHQUFvRCxHQUFyRCxFQUEwRHlELE9BQTFELEVBQTVCLEdBQWtHLENBRHRHO0FBR0EsTUFBTU8sV0FBVyxnQkFBR04sdUNBQUgsMEhBUUpJLFlBUkksK0RBQWpCO0FBWUEsTUFBTUcsV0FBVyxnQkFBR1AsdUNBQUgsMkZBT0pLLFlBUEkscURBQWpCO0FBVUEsU0FDSTtBQUFLLE9BQUcsRUFBRUo7QUFBVixLQUNJO0FBQUssT0FBRyxFQUFFbEUsOEJBQWlCQTtBQUEzQixLQUNJO0FBQUssT0FBRyxFQUFFQyxxQkFBUUE7QUFBbEIsS0FBcUJTLDBDQUFVLENBQUMsMkJBQUQsQ0FBL0IsQ0FESixFQUVJO0FBQUssT0FBRyxFQUFFTSwyQkFBY0E7QUFBeEIsS0FBMkJOLDBDQUFVLENBQUMsaUNBQUQsQ0FBckMsQ0FGSixFQUdJO0FBQUssT0FBRyxFQUFFeUQ7QUFBVixLQUNJO0FBQUssT0FBRyxFQUFFSTtBQUFWLElBREosRUFFSTtBQUFLLE9BQUcsRUFBRUM7QUFBVixJQUZKLENBSEosRUFPSTtBQUNJLE9BQUc7QUFEUCxLQU9JO0FBQ0ksT0FBRztBQURQLEtBTUk7QUFDSSxPQUFHO0FBRFAsSUFOSixFQWdCSzlELDBDQUFVLENBQUMsZ0NBQUQsQ0FoQmYsZUFnQitELG1EQUFJMkQsWUFBSixNQWhCL0QsQ0FQSixFQXlCSTtBQUNJLE9BQUc7QUFEUCxLQU1JO0FBQ0ksT0FBRztBQURQLElBTkosRUFnQkszRCwwQ0FBVSxDQUFDLGdDQUFELENBaEJmLGVBZ0IrRCxtREFBSTRELFlBQUosTUFoQi9ELENBekJKLENBUEosQ0FESixDQURKO0FBd0RILENBeEZNLEM7Ozs7OztBQzNDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxJQUFNckUsc0JBQVEsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFkO0FBT0EsSUFBTXdFLFdBQVcsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFqQjtBQXdCQSxJQUFNQyxhQUFhLEdBQUc7QUFBSDtBQUFBO0FBQUEsYUFBbkI7Ozs7Ozs7QUFRQSxJQUFNQyxhQUFpQixHQUFHLE1BQU07QUFDNUIsTUFBTXZFLE9BQU8sR0FBR0MsaUNBQVcsQ0FBQ0Msb0NBQUQsQ0FBM0IsQ0FENEIsQ0FHNUI7O0FBQ0EsTUFBTXNFLGdCQUFnQixHQUFHckQsTUFBTSxDQUFDQyxPQUFQLENBQWVwQixPQUFmLEVBQXdCeUUsUUFBeEIsT0FBdUN0RCxNQUFNLENBQUNDLE9BQVAsQ0FBZXNELCtCQUFmLEVBQStCRCxRQUEvQixFQUFoRTtBQUVBLFNBQ0k7QUFDSSxhQUFTLEVBQUMsZ0JBRGQ7QUFFSSxPQUFHLGVBQUVaLHVDQUFGLENBQ0djLGdCQURIO0FBRlAsS0FNSSx3Q0FBQyxhQUFEO0FBQWtCLFNBQUssRUFBRTtBQUFDbEIsWUFBTSxFQUFFO0FBQVQ7QUFBekIsS0FDSTtBQUFLLE9BQUcsRUFBRTVELHNCQUFRQTtBQUFsQixLQUFxQlMsMENBQVUsQ0FBQyxrQkFBRCxDQUEvQixDQURKLEVBRUksd0NBQUMsWUFBRCxPQUZKLEVBR0ksd0NBQUMsYUFBRCxPQUhKLEVBSUksd0NBQUMsWUFBRCxPQUpKLEVBS0k7QUFDSSxPQUFHO0FBRFAsS0FLSTtBQUNJLE9BQUcsRUFBRStELFdBRFQ7QUFFSSxRQUFJLEVBQUMsUUFGVDtBQUdJLFdBQU8sRUFBRSxNQUFNO0FBQ1hPLDJEQUFZO0FBQ2YsS0FMTDtBQU1JLFlBQVEsRUFBRUo7QUFOZCxLQVFLbEUsMENBQVUsQ0FBQyxxQkFBRCxDQVJmLENBTEosQ0FMSixFQXFCSTtBQUFLLE9BQUcsRUFBRWdFO0FBQVYsS0FBMEJoRSwwQ0FBVSxDQUFDLHVCQUFELENBQXBDLENBckJKLENBTkosQ0FESjtBQWdDSCxDQXRDRDs7QUF3Q2VpRSxzR0FBZixFIiwiZmlsZSI6IjguanMiLCJzb3VyY2VzQ29udGVudCI6WyJ0eXBlIGdldERpc3BsYXlDb3VudFByb3BzID0ge1xuICAgIGNvdW50OiBudW1iZXI7XG4gICAga1N0YXJ0PzogbnVtYmVyO1xuICAgIG1TdGFydD86IG51bWJlcjtcbn07XG5cbmV4cG9ydCBjb25zdCBnZXREaXNwbGF5Q291bnQgPSAoe2NvdW50LCBrU3RhcnQgPSAxMDAwLCBtU3RhcnQgPSAxMDAwMDAwfTogZ2V0RGlzcGxheUNvdW50UHJvcHMpID0+IHtcbiAgICAvLyBGb3JtYXQgbWlsbGlvbnNcbiAgICBpZiAoY291bnQgPj0gbVN0YXJ0KSB7XG4gICAgICAgIHJldHVybiBgJHtNYXRoLmZsb29yKGNvdW50IC8gMTAwMDAwMCl9TWA7XG4gICAgfVxuXG4gICAgLy8gRm9ybWF0IHRob3VzYW5kc1xuICAgIGlmIChjb3VudCA+PSBrU3RhcnQpIHtcbiAgICAgICAgcmV0dXJuIGAke01hdGguZmxvb3IoY291bnQgLyAxMDAwKX1rYDtcbiAgICB9XG5cbiAgICByZXR1cm4gY291bnQ7XG59O1xuIiwidmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxuZnVuY3Rpb24gR3JlZW5DaGVjayAocHJvcHMpIHtcbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcInN2Z1wiLHByb3BzLFtSZWFjdC5jcmVhdGVFbGVtZW50KFwiY2lyY2xlXCIse1wiY3hcIjpcIjEyXCIsXCJjeVwiOlwiMTJcIixcInJcIjpcIjEyXCIsXCJmaWxsXCI6XCIjMjlERENDXCIsXCJrZXlcIjowfSksUmVhY3QuY3JlYXRlRWxlbWVudChcImdcIix7XCJjbGlwUGF0aFwiOlwidXJsKCNjbGlwMClcIixcImtleVwiOjF9LFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZFwiOlwiTTE3LjI1IDkuMDA1N0wxNS4wMzYzIDcuNUwxMC44ODcxIDEyLjkxODNMOC4zNDY3NyAxMS4xODQ0TDYuNzUgMTMuMjcxOUwxMS40OTE5IDE2LjVWMTYuNDg4NkwxMS41MDQgMTYuNUwxNy4yNSA5LjAwNTdaXCIsXCJmaWxsXCI6XCIjMjAyOTQ1XCJ9KSksUmVhY3QuY3JlYXRlRWxlbWVudChcImRlZnNcIix7XCJrZXlcIjoyfSxSZWFjdC5jcmVhdGVFbGVtZW50KFwiY2xpcFBhdGhcIix7XCJpZFwiOlwiY2xpcDBcIn0sUmVhY3QuY3JlYXRlRWxlbWVudChcInJlY3RcIix7XCJ3aWR0aFwiOlwiMTAuNVwiLFwiaGVpZ2h0XCI6XCI5XCIsXCJmaWxsXCI6XCJ3aGl0ZVwiLFwidHJhbnNmb3JtXCI6XCJ0cmFuc2xhdGUoNi43NSA3LjUpXCJ9KSkpXSk7XG59XG5cbkdyZWVuQ2hlY2suZGVmYXVsdFByb3BzID0ge1wid2lkdGhcIjpcIjI0XCIsXCJoZWlnaHRcIjpcIjI0XCIsXCJ2aWV3Qm94XCI6XCIwIDAgMjQgMjRcIixcImZpbGxcIjpcIm5vbmVcIn07XG5cbm1vZHVsZS5leHBvcnRzID0gR3JlZW5DaGVjaztcblxuR3JlZW5DaGVjay5kZWZhdWx0ID0gR3JlZW5DaGVjaztcbiIsImltcG9ydCBSZWFjdCwge0ZDfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQge2Nzc30gZnJvbSAnQGVtb3Rpb24vY29yZSc7XG5cbmltcG9ydCB7Z2V0RGlzcGxheUNvdW50fSBmcm9tICcuLi8uLi8uLi91dGlscy9kaXNwbGF5JztcblxuLy8gQHRzLWV4cGVjdC1lcnJvclxuaW1wb3J0IEdyZWVuQ2hlY2sgZnJvbSAnLi4vLi4vLi4vaWNvbnMvZ3JlZW4tY2hlY2suc3ZnJztcblxuY29uc3QgdG90YWxEaXNwbGF5Q3NzID0gY3NzYFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZWJlY2Y3O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgaGVpZ2h0OiA3MHB4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBsaW5lLWhlaWdodDogMTtcbiAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xuICAgIHBhZGRpbmc6IDI0cHggMjNweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHdpZHRoOiAxMTJweDtcbmA7XG5cbmNvbnN0IGNvdW50Q3NzID0gY3NzYFxuICAgIGZvbnQtc2l6ZTogNDZweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuYDtcblxuY29uc3Qgc3VidGl0bGVDc3MgPSBjc3NgXG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG5gO1xuXG5jb25zdCBjaGVja21hcmtDc3MgPSBjc3NgXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDI0cHg7XG4gICAgd2lkdGg6IDI0cHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogLTEycHg7XG5gO1xuXG50eXBlIFRvdGFsRGlzcGxheVByb3BzID0ge1xuICAgIHN1YnRpdGxlOiBzdHJpbmc7XG4gICAgY291bnQ6IG51bWJlcjtcbn07XG5cbmV4cG9ydCBjb25zdCBUb3RhbERpc3BsYXk6IEZDPFRvdGFsRGlzcGxheVByb3BzPiA9ICh7c3VidGl0bGUsIGNvdW50fSkgPT4ge1xuICAgIHJldHVybiAoXG4gICAgICAgIDxkaXYgY3NzPXt0b3RhbERpc3BsYXlDc3N9PlxuICAgICAgICAgICAgPGRpdiBjc3M9e2NvdW50Q3NzfT57Z2V0RGlzcGxheUNvdW50KHtjb3VudCwga1N0YXJ0OiAxMDAwMH0pfTwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjc3M9e3N1YnRpdGxlQ3NzfT57c3VidGl0bGV9PC9kaXY+XG4gICAgICAgICAgICA8R3JlZW5DaGVjayBjc3M9e2NoZWNrbWFya0Nzc30gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgKTtcbn07XG4iLCIvKiBlc2xpbnQtZGlzYWJsZSBAdHlwZXNjcmlwdC1lc2xpbnQvbm8tdW51c2VkLXZhcnMgKi9cbmltcG9ydCBSZWFjdCwge0ZDfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQge2Nzc30gZnJvbSAnQGVtb3Rpb24vY29yZSc7XG5pbXBvcnQge3VzZVNlbGVjdG9yfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB7Z2V0TWVzc2FnZX0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvbG9jYWxpemF0aW9uJztcbmltcG9ydCB7bWV0cmljc1NlbGVjdG9yfSBmcm9tICcuLi8uLi8uLi9zZWxlY3RvcnMvaW5kZXgnO1xuaW1wb3J0IHtUb3RhbERpc3BsYXl9IGZyb20gJy4vVG90YWxEaXNwbGF5JztcblxuY29uc3QgY291bnRzU3VtbWFyeUNzcyA9IGNzc2BcbiAgICBwYWRkaW5nOiAxNnB4IDE2cHggMDtcbmA7XG5cbmNvbnN0IGlubmVyQ29udGFpbmVyQ3NzID0gY3NzYFxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlYmVjZjc7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGJhY2tncm91bmQ6ICNmYmZiZmQ7XG4gICAgcGFkZGluZzogMTZweCA3cHg7XG5gO1xuXG5jb25zdCB0aXRsZUNzcyA9IGNzc2BcbiAgICBjb2xvcjogIzIwMjk0NTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xuYDtcblxuY29uc3QgdG90YWxDb3VudHNDc3MgPSBjc3NgXG4gICAgYmFja2dyb3VuZDogI2ZiZmJmZDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBoZWlnaHQ6IDEzMnB4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbmA7XG5cbmV4cG9ydCBjb25zdCBDb3VudHNTdW1tYXJ5OiBGQyA9ICgpID0+IHtcbiAgICBjb25zdCBtZXRyaWNzID0gdXNlU2VsZWN0b3IobWV0cmljc1NlbGVjdG9yKTtcbiAgICBjb25zdCBibG9ja2VkVHJhY2tlckNvdW50ID0gbWV0cmljcy5ibG9ja2VkTWFqb3JUcmFja2VyQ291bnRzICsgbWV0cmljcy5ibG9ja2VkTWlub3JUcmFja2VyQ291bnRzO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPGRpdiBjc3M9e2NvdW50c1N1bW1hcnlDc3N9PlxuICAgICAgICAgICAgPGRpdiBjc3M9e2lubmVyQ29udGFpbmVyQ3NzfT5cbiAgICAgICAgICAgICAgICA8ZGl2IGNzcz17dGl0bGVDc3N9PntnZXRNZXNzYWdlKCdwb3B1cFJlcG9ydEJsb2NrZWRUaXRsZScpfTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY3NzPXt0b3RhbENvdW50c0Nzc30+XG4gICAgICAgICAgICAgICAgICAgIDxUb3RhbERpc3BsYXlcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1YnRpdGxlPXtnZXRNZXNzYWdlKCdwb3B1cFJlcG9ydEJsb2NrZWRUcmFja2Vyc0xhYmVsJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICBjb3VudD17YmxvY2tlZFRyYWNrZXJDb3VudH1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPFRvdGFsRGlzcGxheVxuICAgICAgICAgICAgICAgICAgICAgICAgc3VidGl0bGU9e2dldE1lc3NhZ2UoJ3BvcHVwUmVwb3J0QmxvY2tlZENvb2tpZXNMYWJlbCcpfVxuICAgICAgICAgICAgICAgICAgICAgICAgY291bnQ9e21ldHJpY3MuYmxvY2tlZENvb2tpZXNDb3VudHN9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgKTtcbn07XG4iLCIvKiBlc2xpbnQtZGlzYWJsZSBAdHlwZXNjcmlwdC1lc2xpbnQvbm8tdW51c2VkLXZhcnMgKi9cbmltcG9ydCBSZWFjdCwge0ZDfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQge2Nzc30gZnJvbSAnQGVtb3Rpb24vY29yZSc7XG4vLyBodHRwczovL3d3dy5jaGFydGpzLm9yZy9kb2NzL2xhdGVzdC9jaGFydHMvZG91Z2hudXQuaHRtbCAtIGNoYXJ0anMgZG9jc1xuLy8gaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0Y2hhcnRqcy9yZWFjdC1jaGFydGpzLTIjZG9jcyAtIGRvY3MgZm9yIHJlYWN0IHdyYXBwZXIgZm9yIGNoYXJ0anNcbmltcG9ydCB7RG91Z2hudXR9IGZyb20gJ3JlYWN0LWNoYXJ0anMtMic7XG5pbXBvcnQge3VzZVNlbGVjdG9yfSBmcm9tICdyZWFjdC1yZWR1eCc7XG4vLyBodHRwczovL3N0YWNrb3ZlcmZsb3cuY29tL2EvNzAwOTg1NDMvNTM0MDY0NlxuaW1wb3J0IHtDaGFydCwgQXJjRWxlbWVudH0gZnJvbSAnY2hhcnQuanMnO1xuXG5pbXBvcnQge2dldE1lc3NhZ2V9IGZyb20gJy4uLy4uLy4uL3V0aWxzL2xvY2FsaXphdGlvbic7XG5pbXBvcnQge21ldHJpY3NTZWxlY3Rvcn0gZnJvbSAnLi4vLi4vLi4vc2VsZWN0b3JzL2luZGV4JztcblxuLy8gaHR0cHM6Ly9zdGFja292ZXJmbG93LmNvbS9hLzcwMDk4NTQzLzUzNDA2NDZcbkNoYXJ0LnJlZ2lzdGVyKEFyY0VsZW1lbnQpO1xuXG5jb25zdCBzY29yZVN1bW1hcnlDc3MgPSBjc3NgXG4gICAgcGFkZGluZzogMTZweCAxNnB4IDA7XG5gO1xuXG5jb25zdCBpbm5lckNvbnRhaW5lckNzcyA9IGNzc2BcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZWJlY2Y3O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmJmYmZkO1xuICAgIHBhZGRpbmc6IDE2cHggN3B4O1xuYDtcblxuY29uc3QgdGl0bGVDc3MgPSBjc3NgXG4gICAgY29sb3I6ICMyMDI5NDU7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTJweDtcbmA7XG5cbmNvbnN0IGRlc2NyaXB0aW9uQ3NzID0gY3NzYFxuICAgIGNvbG9yOiAjN2Y4NjlmO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xuYDtcblxuY29uc3QgY2hhcnRDb250YWluZXJDc3MgPSBjc3NgXG4gICAgZGlzcGxheTogZmxleDtcbmA7XG5cbmNvbnN0IGNoYXJ0TGVnZW5kQ3NzID0gY3NzYFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gICAgbWFyZ2luLWxlZnQ6IDEycHg7XG5gO1xuXG50eXBlIENvbG9yTWFwID0ge1xuICAgIFtrZXk6IHN0cmluZ106IHN0cmluZztcbn07XG5cbnR5cGUgQ2hhcnREYXRhID0ge3Njb3JlOiBzdHJpbmc7IHZhbDogbnVtYmVyfVtdO1xuXG50eXBlIENvbG9ycyA9IHN0cmluZ1tdO1xuXG50eXBlIENoYXJ0SnNPcHRpb25zID0ge1xuICAgIGxlZ2VuZDoge1xuICAgICAgICBkaXNwbGF5OiBib29sZWFuO1xuICAgIH07XG4gICAgZWxlbWVudHM6IHtcbiAgICAgICAgYXJjOiB7XG4gICAgICAgICAgICBib3JkZXJXaWR0aDogbnVtYmVyO1xuICAgICAgICB9O1xuICAgIH07XG4gICAgdG9vbHRpcHM6IHtcbiAgICAgICAgZW5hYmxlZDogYm9vbGVhbjtcbiAgICB9O1xufTtcblxudHlwZSBDaGFydEpzRGF0YSA9IHtcbiAgICBtYWludGFpbkFzcGVjdFJhdGlvOiBib29sZWFuO1xuICAgIHJlc3BvbnNpdmU6IGJvb2xlYW47XG4gICAgZGF0YXNldHM6IFtcbiAgICAgICAge1xuICAgICAgICAgICAgZGF0YTogbnVtYmVyW107XG4gICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IENvbG9ycztcbiAgICAgICAgICAgIGhvdmVyQmFja2dyb3VuZENvbG9yOiBDb2xvcnM7XG4gICAgICAgIH1cbiAgICBdO1xufTtcblxuY29uc3Qgc2NvcmVDb2xvcnM6IENvbG9yTWFwID0ge1xuICAgIDU6ICcjMjlERENDJyxcbiAgICA0OiAnI0E0RDQxMScsXG4gICAgMzogJyNGQkM5MTcnLFxuICAgIDI6ICcjRkY5ODM5JyxcbiAgICAxOiAnI0VCNTc1Nydcbn07XG5cbmV4cG9ydCBjb25zdCBtYWtlQ2hhcnREYXRhID0gKHNjb3JlRGlzdHJpYnV0aW9uOiB7W3Njb3JlOiBudW1iZXJdOiBudW1iZXJ9LCB0b3RhbFNjb3JlczogbnVtYmVyKTogQ2hhcnREYXRhID0+IHtcbiAgICByZXR1cm4gT2JqZWN0LmVudHJpZXMoc2NvcmVEaXN0cmlidXRpb24pXG4gICAgICAgIC5maWx0ZXIoKFtzY29yZSwgdmFsXSkgPT4gdmFsID4gMClcbiAgICAgICAgLm1hcCgoW3Njb3JlLCB2YWxdKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHRvdGFsU2NvcmVzICE9PSAwID8gKHZhbCAvIHRvdGFsU2NvcmVzKSAqIDEwMCA6IDA7XG4gICAgICAgICAgICByZXR1cm4ge3Njb3JlLCB2YWw6IE1hdGgucm91bmQodmFsdWUpfTtcbiAgICAgICAgfSlcbiAgICAgICAgLnNvcnQoKGEsIGIpID0+IHtcbiAgICAgICAgICAgIHJldHVybiBwYXJzZUludChiLnNjb3JlLCAxMCkgLSBwYXJzZUludChhLnNjb3JlLCAxMCk7XG4gICAgICAgIH0pO1xufTtcblxuZXhwb3J0IGNvbnN0IG1ha2VDaGFydENvbG9ycyA9IChjaGFydERhdGE6IENoYXJ0RGF0YSwgY29sb3JzOiBDb2xvck1hcCk6IENvbG9ycyA9PiB7XG4gICAgcmV0dXJuIGNoYXJ0RGF0YVxuICAgICAgICAubWFwKCh7c2NvcmUsIHZhbH0pID0+IHtcbiAgICAgICAgICAgIGlmICh2YWwgPiAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGNvbG9yc1tzY29yZV07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gJyc7XG4gICAgICAgIH0pXG4gICAgICAgIC5maWx0ZXIoKGJydXNoKSA9PiBicnVzaCk7XG59O1xuXG5leHBvcnQgY29uc3QgU2NvcmVTdW1tYXJ5OiBGQyA9ICgpID0+IHtcbiAgICBjb25zdCBtZXRyaWNzID0gdXNlU2VsZWN0b3IobWV0cmljc1NlbGVjdG9yKTtcbiAgICBjb25zdCB7c2NvcmVEaXN0cmlidXRpb259ID0gbWV0cmljcztcblxuICAgIGNvbnN0IHRvdGFsU2NvcmVzID0gT2JqZWN0LnZhbHVlcyhzY29yZURpc3RyaWJ1dGlvbikucmVkdWNlKChwcmV2LCBuZXh0KSA9PiBwcmV2ICsgbmV4dCwgMCk7XG5cbiAgICBjb25zdCBjaGFydERhdGE6IENoYXJ0RGF0YSA9IG1ha2VDaGFydERhdGEoc2NvcmVEaXN0cmlidXRpb24sIHRvdGFsU2NvcmVzKTtcbiAgICBjb25zdCBjaGFydENvbG9yczogQ29sb3JzID0gbWFrZUNoYXJ0Q29sb3JzKGNoYXJ0RGF0YSwgc2NvcmVDb2xvcnMpO1xuXG4gICAgY29uc3Qgb3B0aW9uczogQ2hhcnRKc09wdGlvbnMgPSB7XG4gICAgICAgIGxlZ2VuZDoge1xuICAgICAgICAgICAgZGlzcGxheTogZmFsc2VcbiAgICAgICAgfSxcbiAgICAgICAgZWxlbWVudHM6IHtcbiAgICAgICAgICAgIGFyYzoge1xuICAgICAgICAgICAgICAgIGJvcmRlcldpZHRoOiAwXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHRvb2x0aXBzOiB7XG4gICAgICAgICAgICBlbmFibGVkOiBmYWxzZVxuICAgICAgICB9XG4gICAgfTtcblxuICAgIGNvbnN0IGRhdGE6IENoYXJ0SnNEYXRhID0ge1xuICAgICAgICBtYWludGFpbkFzcGVjdFJhdGlvOiBmYWxzZSxcbiAgICAgICAgcmVzcG9uc2l2ZTogZmFsc2UsXG4gICAgICAgIGRhdGFzZXRzOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgZGF0YTogY2hhcnREYXRhLm1hcCgoe3ZhbH0pID0+IHZhbCksXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiBjaGFydENvbG9ycyxcbiAgICAgICAgICAgICAgICBob3ZlckJhY2tncm91bmRDb2xvcjogY2hhcnRDb2xvcnNcbiAgICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgIH07XG5cbiAgICBjb25zdCBkZXNjU2NvcmVzID0gT2JqZWN0LmVudHJpZXMoc2NvcmVEaXN0cmlidXRpb24pXG4gICAgICAgIC5tYXAoKHMpID0+IHMpXG4gICAgICAgIC5zb3J0KChhLCBiKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gcGFyc2VJbnQoYlswXSwgMTApIC0gcGFyc2VJbnQoYVswXSwgMTApO1xuICAgICAgICB9KTtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxkaXYgY3NzPXtzY29yZVN1bW1hcnlDc3N9PlxuICAgICAgICAgICAgPGRpdiBjc3M9e2lubmVyQ29udGFpbmVyQ3NzfT5cbiAgICAgICAgICAgICAgICA8ZGl2IGNzcz17dGl0bGVDc3N9PntnZXRNZXNzYWdlKCdwb3B1cFJlcG9ydFNjb3JlVGl0bGUnKX08L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNzcz17ZGVzY3JpcHRpb25Dc3N9PntnZXRNZXNzYWdlKCdwb3B1cFJlcG9ydFNjb3JlRGVzY3JpcHRpb24nKX08L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNzcz17Y2hhcnRDb250YWluZXJDc3N9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7aGVpZ2h0OiAnMTkwcHgnLCB3aWR0aDogJzE5MHB4J319PlxuICAgICAgICAgICAgICAgICAgICAgICAgPERvdWdobnV0IGRhdGE9e2RhdGF9IG9wdGlvbnM9e29wdGlvbnN9IC8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNzcz17Y2hhcnRMZWdlbmRDc3N9PlxuICAgICAgICAgICAgICAgICAgICAgICAge2Rlc2NTY29yZXMubWFwKChbc2NvcmUsIGNvdW50XSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBlcmNlbnQgPSB0b3RhbFNjb3JlcyAhPT0gMCA/ICgoY291bnQgLyB0b3RhbFNjb3JlcykgKiAxMDApLnRvRml4ZWQoKSA6IDA7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjc3M9e2Nzc2BcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNzcz17Y3NzYFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAke3Njb3JlQ29sb3JzW3Njb3JlXX07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxNHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDhweDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7c2NvcmV9Jm5ic3A7OiZuYnNwOzxiPntgJHtwZXJjZW50fSVgfTwvYj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pfVxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICApO1xufTtcbiIsIi8qIGVzbGludC1kaXNhYmxlIEB0eXBlc2NyaXB0LWVzbGludC9uby11bnVzZWQtdmFycyAqL1xuaW1wb3J0IFJlYWN0LCB7RkN9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7Y3NzfSBmcm9tICdAZW1vdGlvbi9jb3JlJztcbmltcG9ydCB7dXNlU2VsZWN0b3J9IGZyb20gJ3JlYWN0LXJlZHV4JztcblxuaW1wb3J0IHtnZXRNZXNzYWdlfSBmcm9tICcuLi8uLi8uLi91dGlscy9sb2NhbGl6YXRpb24nO1xuaW1wb3J0IHttZXRyaWNzU2VsZWN0b3J9IGZyb20gJy4uLy4uLy4uL3NlbGVjdG9ycy9pbmRleCc7XG5cbmNvbnN0IHR5cGVzU3VtbWFyeUNzcyA9IGNzc2BcbiAgICBwYWRkaW5nOiAxNnB4IDE2cHggMDtcbmA7XG5cbmNvbnN0IGlubmVyQ29udGFpbmVyQ3NzID0gY3NzYFxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlYmVjZjc7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGJhY2tncm91bmQ6ICNmYmZiZmQ7XG4gICAgcGFkZGluZzogMTZweCA3cHg7XG5gO1xuXG5jb25zdCB0aXRsZUNzcyA9IGNzc2BcbiAgICBjb2xvcjogIzIwMjk0NTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMnB4O1xuYDtcblxuY29uc3QgZGVzY3JpcHRpb25Dc3MgPSBjc3NgXG4gICAgY29sb3I6ICM3Zjg2OWY7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDE5cHg7XG5gO1xuXG5jb25zdCB0cmFja2VyQmFyQ3NzID0gY3NzYFxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTAwJTtcbmA7XG5cbmV4cG9ydCBjb25zdCBUeXBlc1N1bW1hcnk6IEZDID0gKCkgPT4ge1xuICAgIGNvbnN0IG1ldHJpY3MgPSB1c2VTZWxlY3RvcihtZXRyaWNzU2VsZWN0b3IpO1xuICAgIGNvbnN0IHtibG9ja2VkTWFqb3JUcmFja2VyQ291bnRzLCBibG9ja2VkTWlub3JUcmFja2VyQ291bnRzfSA9IG1ldHJpY3M7XG4gICAgY29uc3QgYmxvY2tlZFRyYWNrZXJDb3VudCA9IGJsb2NrZWRNYWpvclRyYWNrZXJDb3VudHMgKyBibG9ja2VkTWlub3JUcmFja2VyQ291bnRzO1xuXG4gICAgY29uc3QgbWFqb3JQZXJjZW50ID1cbiAgICAgICAgYmxvY2tlZFRyYWNrZXJDb3VudCAhPT0gMCA/ICgoYmxvY2tlZE1ham9yVHJhY2tlckNvdW50cyAvIGJsb2NrZWRUcmFja2VyQ291bnQpICogMTAwKS50b0ZpeGVkKCkgOiAwO1xuICAgIGNvbnN0IG1pbm9yUGVyY2VudCA9XG4gICAgICAgIGJsb2NrZWRUcmFja2VyQ291bnQgIT09IDAgPyAoKGJsb2NrZWRNaW5vclRyYWNrZXJDb3VudHMgLyBibG9ja2VkVHJhY2tlckNvdW50KSAqIDEwMCkudG9GaXhlZCgpIDogMDtcblxuICAgIGNvbnN0IG1ham9yQmFyQ3NzID0gY3NzYFxuICAgICAgICBiYWNrZ3JvdW5kOiAjNjU3M2ZmO1xuICAgICAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjZmJmYmZkO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIGxlZnQ6IDA7XG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDA7XG4gICAgICAgIHdpZHRoOiAke21ham9yUGVyY2VudH0lO1xuICAgICAgICB6LWluZGV4OiAxO1xuICAgIGA7XG5cbiAgICBjb25zdCBtaW5vckJhckNzcyA9IGNzc2BcbiAgICAgICAgYmFja2dyb3VuZDogIzM3NDY3NztcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICByaWdodDogMDtcbiAgICAgICAgdG9wOiAwO1xuICAgICAgICB3aWR0aDogJHttaW5vclBlcmNlbnR9JTtcbiAgICBgO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPGRpdiBjc3M9e3R5cGVzU3VtbWFyeUNzc30+XG4gICAgICAgICAgICA8ZGl2IGNzcz17aW5uZXJDb250YWluZXJDc3N9PlxuICAgICAgICAgICAgICAgIDxkaXYgY3NzPXt0aXRsZUNzc30+e2dldE1lc3NhZ2UoJ3BvcHVwUmVwb3J0QnJlYWtkb3duVGl0bGUnKX08L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNzcz17ZGVzY3JpcHRpb25Dc3N9PntnZXRNZXNzYWdlKCdwb3B1cFJlcG9ydEJyZWFrZG93bkRlc2NyaXB0aW9uJyl9PC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjc3M9e3RyYWNrZXJCYXJDc3N9PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNzcz17bWFqb3JCYXJDc3N9IC8+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY3NzPXttaW5vckJhckNzc30gLz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgIGNzcz17Y3NzYFxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTMuNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgICAgICAgICAgICBgfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgICAgICAgY3NzPXtjc3NgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICAgICAgYH1cbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjc3M9e2Nzc2BcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzY1NzNmZjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxNHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDRweDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYH1cbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICB7Z2V0TWVzc2FnZSgncG9wdXBSZXBvcnRCcmVha2Rvd25NYWpvckxhYmVsJyl9Jm5ic3A7OiZuYnNwOzxiPnttYWpvclBlcmNlbnR9JTwvYj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICAgIGNzcz17Y3NzYFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGB9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3NzPXtjc3NgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICMzNzQ2Nzc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTRweDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA0cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGB9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAge2dldE1lc3NhZ2UoJ3BvcHVwUmVwb3J0QnJlYWtkb3duTWlub3JMYWJlbCcpfSZuYnNwOzombmJzcDs8Yj57bWlub3JQZXJjZW50fSU8L2I+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICk7XG59O1xuIiwiLyogZXNsaW50LWRpc2FibGUgQHR5cGVzY3JpcHQtZXNsaW50L25vLXVudXNlZC12YXJzICovXG5pbXBvcnQgUmVhY3QsIHtGQ30gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHtjc3N9IGZyb20gJ0BlbW90aW9uL2NvcmUnO1xuaW1wb3J0IFBlcmZlY3RTY3JvbGxiYXIgZnJvbSAncmVhY3QtcGVyZmVjdC1zY3JvbGxiYXInO1xuaW1wb3J0IHNjcm9sbGJhclN0eWxlIGZyb20gJ3JlYWN0LXBlcmZlY3Qtc2Nyb2xsYmFyL2Rpc3QvY3NzL3N0eWxlcy5jc3MnO1xuaW1wb3J0IHt1c2VTZWxlY3Rvcn0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQge0NvdW50c1N1bW1hcnl9IGZyb20gJy4vQ291bnRzU3VtbWFyeSc7XG5pbXBvcnQge2dldE1lc3NhZ2V9IGZyb20gJy4uLy4uLy4uL3V0aWxzL2xvY2FsaXphdGlvbic7XG5pbXBvcnQge2luaXRpYWxTdGF0ZSBhcyBpbml0aWFsTWV0cmljc30gZnJvbSAnLi4vLi4vLi4vcmVkdWNlcnMvTWV0cmljcyc7XG5pbXBvcnQge21ldHJpY3NTZWxlY3Rvcn0gZnJvbSAnLi4vLi4vLi4vc2VsZWN0b3JzL2luZGV4JztcbmltcG9ydCB7cmVzZXRNZXRyaWNzfSBmcm9tICcuLi8uLi8uLi9wcml2YWN5IHRvb2xzL21ldHJpY3MnO1xuaW1wb3J0IHtTY29yZVN1bW1hcnl9IGZyb20gJy4vU2NvcmVTdW1tYXJ5JztcbmltcG9ydCB7VHlwZXNTdW1tYXJ5fSBmcm9tICcuL1R5cGVzU3VtbWFyeSc7XG5cbmNvbnN0IHRpdGxlQ3NzID0gY3NzYFxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWJlY2Y3O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAyMnB4O1xuICAgIHBhZGRpbmc6IDE2cHg7XG5gO1xuXG5jb25zdCByZXNldEJ0bkNzcyA9IGNzc2BcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM2NTczZmY7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGNvbG9yOiAjNjU3M2ZmO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBoZWlnaHQ6IDM4cHg7XG4gICAgbWFyZ2luLXRvcDogYXV0bztcbiAgICB0cmFuc2l0aW9uOiAwLjRzO1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgJjpmb2N1cyB7XG4gICAgICAgIG91dGxpbmU6IG5vbmU7XG4gICAgfVxuXG4gICAgJjpkaXNhYmxlZCB7XG4gICAgICAgIGNvbG9yOiBsaWdodGdyYXk7XG4gICAgICAgIGN1cnNvcjogaW5pdGlhbDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgIH1cbmA7XG5cbmNvbnN0IGRpc2NsYWltZXJDc3MgPSBjc3NgXG4gICAgY29sb3I6ICM3Zjg2OWY7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIHBhZGRpbmc6IDE2cHggNDJweCAyNHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbmA7XG5cbmNvbnN0IFByaXZhY3lSZXBvcnQ6IEZDID0gKCkgPT4ge1xuICAgIGNvbnN0IG1ldHJpY3MgPSB1c2VTZWxlY3RvcihtZXRyaWNzU2VsZWN0b3IpO1xuXG4gICAgLy8gVE9ETzogQmVlZiB1cCB0aGlzIGNoZWNrIChvcmRlciBvZiBrZXlzIG1hdHRlcnMgYXRtKS5cbiAgICBjb25zdCBpc0luaXRpYWxNZXRyaWNzID0gT2JqZWN0LmVudHJpZXMobWV0cmljcykudG9TdHJpbmcoKSA9PT0gT2JqZWN0LmVudHJpZXMoaW5pdGlhbE1ldHJpY3MpLnRvU3RyaW5nKCk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2XG4gICAgICAgICAgICBjbGFzc05hbWU9XCJwcml2YWN5LXJlcG9ydFwiXG4gICAgICAgICAgICBjc3M9e2Nzc2BcbiAgICAgICAgICAgICAgICAke3Njcm9sbGJhclN0eWxlfVxuICAgICAgICAgICAgYH1cbiAgICAgICAgPlxuICAgICAgICAgICAgPFBlcmZlY3RTY3JvbGxiYXIgc3R5bGU9e3toZWlnaHQ6ICc0MjBweCd9fT5cbiAgICAgICAgICAgICAgICA8ZGl2IGNzcz17dGl0bGVDc3N9PntnZXRNZXNzYWdlKCdwb3B1cFJlcG9ydFRpdGxlJyl9PC9kaXY+XG4gICAgICAgICAgICAgICAgPFNjb3JlU3VtbWFyeSAvPlxuICAgICAgICAgICAgICAgIDxDb3VudHNTdW1tYXJ5IC8+XG4gICAgICAgICAgICAgICAgPFR5cGVzU3VtbWFyeSAvPlxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgY3NzPXtjc3NgXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAxNnB4IDE2cHggMDtcbiAgICAgICAgICAgICAgICAgICAgYH1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgICAgICAgICAgIGNzcz17cmVzZXRCdG5Dc3N9XG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNldE1ldHJpY3MoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17aXNJbml0aWFsTWV0cmljc31cbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAge2dldE1lc3NhZ2UoJ3BvcHVwUmVwb3J0UmVzZXRCdG4nKX1cbiAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjc3M9e2Rpc2NsYWltZXJDc3N9PntnZXRNZXNzYWdlKCdwb3B1cFJlcG9ydERpc2NsYWltZXInKX08L2Rpdj5cbiAgICAgICAgICAgIDwvUGVyZmVjdFNjcm9sbGJhcj5cbiAgICAgICAgPC9kaXY+XG4gICAgKTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFByaXZhY3lSZXBvcnQ7XG4iXSwic291cmNlUm9vdCI6IiJ9