(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _emotion_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);




var Toggle = (_ref) => {
  var {
    id,
    height,
    width,
    hexColor,
    checked = false,
    animate = true,
    onChange: _onChange
  } = _ref;
  var sliderDiameter = height * 0.75;
  var toggleCss = /*#__PURE__*/Object(_emotion_core__WEBPACK_IMPORTED_MODULE_1__[/* css */ "a"])(".switch{position:relative;display:inline-block;height:", height, "px;width:", width, "px;}.switch input{opacity:0;width:0;height:0;}.slider{background-color:#ced1dd;border-radius:", height, "px;cursor:pointer;inset:0;position:absolute;", animate && 'transition: 0.4s;', "}input:checked + .slider{background-color:", hexColor, ";}.slider:before{background-color:#ebecf7;border-radius:", sliderDiameter, "px;box-shadow:1px 2px 4px rgba(0,0,0,0.1);content:'';height:", sliderDiameter, "px;margin:calc(", sliderDiameter, "px / 6);position:absolute;width:", sliderDiameter, "px;", animate && 'transition: 0.4s;', "}input:checked + .slider:before{background-color:#ffffff;transform:translateX(", width - height, "px);};label:toggleCss;" + ( true ? "" : undefined));
  return Object(_emotion_core__WEBPACK_IMPORTED_MODULE_1__[/* jsx */ "b"])("div", {
    css: toggleCss
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_1__[/* jsx */ "b"])("label", {
    className: "switch",
    htmlFor: id
  }, Object(_emotion_core__WEBPACK_IMPORTED_MODULE_1__[/* jsx */ "b"])("input", {
    id: id,
    type: "checkbox",
    checked: checked // eslint-disable-next-line @typescript-eslint/no-unused-vars
    ,
    onChange: event => {
      if (_onChange) {
        _onChange();
      }
    }
  }), Object(_emotion_core__WEBPACK_IMPORTED_MODULE_1__[/* jsx */ "b"])("span", {
    className: "slider"
  })));
};

/* harmony default export */ __webpack_exports__["a"] = (Toggle);

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getDisplayCount; });
var getDisplayCount = (_ref) => {
  var {
    count,
    kStart = 1000,
    mStart = 1000000
  } = _ref;

  // Format millions
  if (count >= mStart) {
    return "".concat(Math.floor(count / 1000000), "M");
  } // Format thousands


  if (count >= kStart) {
    return "".concat(Math.floor(count / 1000), "k");
  }

  return count;
};

/***/ }),

/***/ 339:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function HiddenIcon (props) {
    return React.createElement("svg",props,[React.createElement("path",{"d":"M21.3302 2.26024C20.9849 1.90365 20.4324 1.90365 20.087 2.26024L16.9561 5.49327C13.0655 3.68657 8.92157 4.12636 4.97337 6.78886C2.0266 8.77385 0.242431 11.2699 0.173366 11.3769C-0.0568498 11.7097 -0.0568498 12.1614 0.196388 12.4942C1.86545 14.6099 3.6266 16.2621 5.41078 17.4151L2.64819 20.2678C2.30286 20.6243 2.30286 21.1949 2.64819 21.5633C2.82085 21.7416 3.03955 21.8367 3.26977 21.8367C3.49998 21.8367 3.71869 21.7416 3.89135 21.5633L21.3302 3.55583C21.6755 3.19924 21.6755 2.62871 21.3302 2.26024ZM9.81941 12.8746C9.69279 12.5774 9.62373 12.2565 9.62373 11.9118C9.62373 11.2581 9.86545 10.64 10.3144 10.1764C11.0165 9.45136 12.064 9.27307 12.9273 9.65342L9.81941 12.8746ZM14.2396 8.31029C12.6396 7.25242 10.4755 7.4426 9.08272 8.88082C8.29998 9.68908 7.87409 10.7588 7.87409 11.8999C7.87409 12.7319 8.1043 13.5283 8.5302 14.2058L6.7115 16.0838C5.12301 15.1329 3.55754 13.7304 2.0266 11.888C2.68272 11.1035 4.0525 9.58211 5.94027 8.31029C9.19783 6.11135 12.4439 5.6359 15.6093 6.88395L14.2396 8.31029Z","fill":"#CED1DD","key":0}),React.createElement("path",{"d":"M23.8043 11.3307C22.5957 9.79734 21.341 8.50175 20.0633 7.46766C19.6719 7.15862 19.1194 7.22993 18.8201 7.62217C18.5209 8.01442 18.5899 8.58495 18.9698 8.90588C19.9827 9.71414 20.9842 10.7363 21.9626 11.9131C21.3871 12.6144 20.259 13.8624 18.7281 15.0154C15.7813 17.2262 12.8115 18.0226 9.89928 17.3926C9.42734 17.2856 8.9554 17.6065 8.8518 18.0939C8.7482 18.5812 9.05899 19.0685 9.53093 19.1755C10.2906 19.3419 11.0504 19.4251 11.8216 19.4251C12.9727 19.4251 14.1468 19.2349 15.2978 18.8665C16.8173 18.3791 18.3367 17.5709 19.7986 16.4655C22.2619 14.5993 23.7353 12.543 23.8043 12.4598C24.0576 12.1151 24.0576 11.6635 23.8043 11.3307Z","fill":"#CED1DD","key":1}),React.createElement("defs",{"key":2},React.createElement("clipPath",{"id":"clip0"},React.createElement("rect",{"width":"24","height":"19.8261","fill":"white","transform":"translate(0 2)"})))]);
}

HiddenIcon.defaultProps = {"width":"24","height":"24","viewBox":"0 0 24 24","fill":"none"};

module.exports = HiddenIcon;

HiddenIcon.default = HiddenIcon;


/***/ }),

/***/ 340:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function ProtectionOn (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"8","cy":"8","r":"8","fill":"#29DDCC","key":0}),React.createElement("g",{"clipPath":"url(#clip0)","key":1},React.createElement("path",{"d":"M11.5 6.0038L10.0242 5L7.25806 8.61217L5.56452 7.45627L4.5 8.84791L7.66129 11V10.9924L7.66935 11L11.5 6.0038Z","fill":"#202945"})),React.createElement("defs",{"key":2},React.createElement("clipPath",{"id":"clip0"},React.createElement("rect",{"width":"7","height":"6","fill":"white","transform":"translate(4.5 5)"})))]);
}

ProtectionOn.defaultProps = {"width":"16","height":"16","viewBox":"0 0 16 16","fill":"none"};

module.exports = ProtectionOn;

ProtectionOn.default = ProtectionOn;


/***/ }),

/***/ 341:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function ProtectionOff (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"8","cy":"8","r":"8","fill":"#EB5757","key":0}),React.createElement("path",{"d":"M7 4.5H9L8.73946 8.5H7.27586L7 4.5Z","fill":"white","key":1}),React.createElement("circle",{"cx":"8","cy":"10.5","r":"1","fill":"white","key":2})]);
}

ProtectionOff.defaultProps = {"width":"16","height":"16","viewBox":"0 0 16 16","fill":"none"};

module.exports = ProtectionOff;

ProtectionOff.default = ProtectionOff;


/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function ProtectionOffGray (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"8","cy":"8","r":"8","fill":"#7F869F","key":0}),React.createElement("path",{"d":"M7 4.5H9L8.73946 8.5H7.27586L7 4.5Z","fill":"white","key":1}),React.createElement("circle",{"cx":"8","cy":"10.5","r":"1","fill":"white","key":2})]);
}

ProtectionOffGray.defaultProps = {"width":"16","height":"16","viewBox":"0 0 16 16","fill":"none"};

module.exports = ProtectionOffGray;

ProtectionOffGray.default = ProtectionOffGray;


/***/ }),

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScore1 (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36","cy":"36","r":"28","fill":"#6573FF","key":0}),React.createElement("path",{"d":"M38.1832 24.5V46.3182H34.2308V28.3459H34.103L29 31.6058V27.9837L34.4226 24.5H38.1832Z","fill":"white","key":1}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M36 72C55.8823 72 72 55.8823 72 36C72 16.1177 55.8823 0 36 0C16.1177 0 0 16.1177 0 36C0 55.8823 16.1177 72 36 72ZM36 68C53.6731 68 68 53.6731 68 36C68 18.3269 53.6731 4 36 4C18.3269 4 4 18.3269 4 36C4 53.6731 18.3269 68 36 68Z","fill":"#EBECF7","key":2}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M35.9286 71.9999L36 71.5V68C21.769 68 9.70765 58.7104 5.54926 45.8643L1.74292 47.0974C6.41415 61.5277 19.9498 71.9689 35.9286 71.9999Z","fill":"#EB5757","key":3})]);
}

PrivacyScore1.defaultProps = {"width":"72","height":"72","viewBox":"0 0 72 72","fill":"none"};

module.exports = PrivacyScore1;

PrivacyScore1.default = PrivacyScore1;


/***/ }),

/***/ 344:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScore2 (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36","cy":"36","r":"28","fill":"#6573FF","key":0}),React.createElement("path",{"d":"M28.1278 46.6165V43.7614L35.7024 36.3359C36.4268 35.6044 37.0305 34.9545 37.5135 34.3864C37.9964 33.8182 38.3587 33.2678 38.6001 32.7351C38.8416 32.2024 38.9624 31.6342 38.9624 31.0305C38.9624 30.3416 38.8061 29.7521 38.4936 29.2621C38.1811 28.7649 37.7514 28.3814 37.2045 28.1115C36.6577 27.8416 36.0362 27.7067 35.3402 27.7067C34.6229 27.7067 33.9943 27.8558 33.4545 28.1541C32.9148 28.4453 32.4957 28.8608 32.1974 29.4006C31.9063 29.9403 31.7607 30.5831 31.7607 31.3288H28C28 29.9439 28.3161 28.7401 28.9482 27.7173C29.5803 26.6946 30.4503 25.9027 31.5582 25.3416C32.6733 24.7805 33.9517 24.5 35.3935 24.5C36.8565 24.5 38.142 24.7734 39.25 25.3203C40.358 25.8672 41.2173 26.6165 41.8281 27.5682C42.446 28.5199 42.755 29.6065 42.755 30.8281C42.755 31.6449 42.5987 32.4474 42.2862 33.2358C41.9737 34.0241 41.4233 34.8977 40.6349 35.8565C39.8537 36.8153 38.7564 37.9766 37.343 39.3402L33.5824 43.1648V43.3139H43.0852V46.6165H28.1278Z","fill":"white","key":1}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M36 72C55.8823 72 72 55.8823 72 36C72 16.1177 55.8823 0 36 0C16.1177 0 0 16.1177 0 36C0 55.8823 16.1177 72 36 72ZM36 68C53.6731 68 68 53.6731 68 36C68 18.3269 53.6731 4 36 4C18.3269 4 4 18.3269 4 36C4 53.6731 18.3269 68 36 68Z","fill":"#EBECF7","key":2}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M35.9273 71.9999L36 71.7636V68C18.3269 68 4 53.6731 4 36C4 25.3374 9.21496 15.8928 17.2334 10.0777L14.9032 6.82617C5.87357 13.3672 0 23.9977 0 36C0 55.858 16.0785 71.9607 35.9273 71.9999Z","fill":"#F18B1F","key":3})]);
}

PrivacyScore2.defaultProps = {"width":"72","height":"72","viewBox":"0 0 72 72","fill":"none"};

module.exports = PrivacyScore2;

PrivacyScore2.default = PrivacyScore2;


/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScore3 (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36","cy":"36","r":"28","fill":"#6573FF","key":0}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M36 72C55.8823 72 72 55.8823 72 36C72 16.1177 55.8823 0 36 0C16.1177 0 0 16.1177 0 36C0 55.8823 16.1177 72 36 72ZM36 68C53.6731 68 68 53.6731 68 36C68 18.3269 53.6731 4 36 4C18.3269 4 4 18.3269 4 36C4 53.6731 18.3269 68 36 68Z","fill":"#EBECF7","key":1}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M54.925 10.1934C49.6247 6.29987 43.0809 4 36 4C18.3269 4 4 18.3269 4 36C4 53.6731 18.3269 68 36 68V71.5002L35.889 71.9998C16.0578 71.94 0 55.8452 0 36C0 16.1177 16.1177 0 36 0C43.966 0 51.3277 2.58736 57.2906 6.96754L54.925 10.1934Z","fill":"#FFCF25","key":2}),React.createElement("path",{"d":"M36.0007 46.9148C34.4666 46.9148 33.103 46.652 31.9098 46.1264C30.7237 45.6009 29.7862 44.8693 29.0973 43.9318C28.4084 42.9943 28.0426 41.9112 28 40.6825H32.0057C32.0412 41.272 32.2365 41.7869 32.5916 42.2273C32.9467 42.6605 33.419 42.9979 34.0085 43.2393C34.598 43.4808 35.2585 43.6016 35.9901 43.6016C36.7713 43.6016 37.4638 43.4666 38.0675 43.1967C38.6712 42.9197 39.1435 42.5362 39.4844 42.0462C39.8253 41.5561 39.9922 40.9915 39.9851 40.3523C39.9922 39.6918 39.8217 39.1094 39.4737 38.6051C39.1257 38.1009 38.6214 37.7067 37.9609 37.4226C37.3075 37.1385 36.5192 36.9964 35.5959 36.9964H33.6676V33.9496H35.5959C36.3558 33.9496 37.0199 33.8182 37.5881 33.5554C38.1634 33.2926 38.6143 32.9233 38.9411 32.4474C39.2678 31.9645 39.4276 31.407 39.4205 30.7749C39.4276 30.157 39.2891 29.6207 39.005 29.1662C38.728 28.7045 38.3338 28.3459 37.8224 28.0902C37.3182 27.8345 36.7251 27.7067 36.0433 27.7067C35.3757 27.7067 34.7578 27.8274 34.1896 28.0689C33.6214 28.3104 33.1634 28.6548 32.8153 29.1023C32.4673 29.5426 32.2827 30.0682 32.2614 30.679H28.4581C28.4865 29.4574 28.8381 28.3849 29.5128 27.4616C30.1946 26.5313 31.1037 25.8068 32.2401 25.2884C33.3764 24.7628 34.6513 24.5 36.0646 24.5C37.5206 24.5 38.7848 24.7734 39.8572 25.3203C40.9368 25.8601 41.7713 26.5881 42.3608 27.5043C42.9503 28.4205 43.245 29.4325 43.245 30.5405C43.2521 31.7692 42.8899 32.799 42.1584 33.63C41.4339 34.4609 40.4822 35.0043 39.3033 35.2599V35.4304C40.8374 35.6435 42.0128 36.2116 42.8295 37.1349C43.6534 38.0511 44.0618 39.1911 44.0547 40.5547C44.0547 41.7763 43.7067 42.87 43.0107 43.8359C42.3217 44.7947 41.37 45.5476 40.1555 46.0945C38.9482 46.6413 37.5632 46.9148 36.0007 46.9148Z","fill":"white","key":3})]);
}

PrivacyScore3.defaultProps = {"width":"73","height":"72","viewBox":"0 0 73 72","fill":"none"};

module.exports = PrivacyScore3;

PrivacyScore3.default = PrivacyScore3;


/***/ }),

/***/ 346:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScore4 (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36.7911","cy":"36","r":"28","fill":"#6573FF","key":0}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M36.7911 72C56.6734 72 72.7911 55.8823 72.7911 36C72.7911 16.1177 56.6734 0 36.7911 0C16.9089 0 0.791138 16.1177 0.791138 36C0.791138 55.8823 16.9089 72 36.7911 72ZM36.7911 68C54.4642 68 68.7911 53.6731 68.7911 36C68.7911 18.3269 54.4642 4 36.7911 4C19.118 4 4.79114 18.3269 4.79114 36C4.79114 53.6731 19.118 68 36.7911 68Z","fill":"#EBECF7","key":1}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M67.1185 46.2371C68.2031 43.0229 68.791 39.5801 68.791 36C68.791 18.3269 54.4641 4 36.791 4C19.1179 4 4.79102 18.3269 4.79102 36C4.79102 53.6034 19.0051 67.8869 36.582 67.9993V71.0009L36.333 71.9971C16.6618 71.7519 0.791016 55.7293 0.791016 36C0.791016 16.1177 16.9088 0 36.791 0C56.6733 0 72.791 16.1177 72.791 36C72.791 40.0039 72.1374 43.8551 70.931 47.4527L67.1185 46.2371Z","fill":"#A4D411","key":2}),React.createElement("path",{"d":"M26.791 42.2699V39.1271L36.0488 24.5H38.6696V28.9744H37.0716L30.8393 38.8501V39.0206H43.7619V42.2699H26.791ZM37.1994 46.3182V41.3111L37.242 39.9048V24.5H40.9707V46.3182H37.1994Z","fill":"white","key":3})]);
}

PrivacyScore4.defaultProps = {"width":"73","height":"72","viewBox":"0 0 73 72","fill":"none"};

module.exports = PrivacyScore4;

PrivacyScore4.default = PrivacyScore4;


/***/ }),

/***/ 347:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScore5 (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36","cy":"36","r":"36","fill":"#29DDCC","key":0}),React.createElement("circle",{"cx":"36","cy":"36","r":"30","fill":"#6573FF","stroke":"white","strokeWidth":"4","key":1}),React.createElement("path",{"d":"M36.032 47.2983C34.6115 47.2983 33.3402 47.032 32.218 46.4993C31.0959 45.9595 30.2045 45.2209 29.544 44.2834C28.8906 43.3459 28.5426 42.2734 28.5 41.0661H32.3352C32.4062 41.9609 32.7933 42.6925 33.4964 43.2607C34.1996 43.8217 35.0447 44.1023 36.032 44.1023C36.8061 44.1023 37.495 43.9247 38.0987 43.5696C38.7024 43.2145 39.1783 42.7209 39.5263 42.0888C39.8743 41.4567 40.0447 40.7358 40.0376 39.9261C40.0447 39.1023 39.8707 38.3707 39.5156 37.7315C39.1605 37.0923 38.674 36.5916 38.0561 36.2294C37.4382 35.8601 36.728 35.6754 35.9254 35.6754C35.272 35.6683 34.6293 35.7891 33.9972 36.0376C33.3651 36.2862 32.8643 36.6129 32.495 37.0178L28.9261 36.4318L30.0661 25.1818H42.7223V28.4844H33.3366L32.7081 34.2692H32.8359C33.2408 33.7933 33.8125 33.3992 34.5511 33.0867C35.2898 32.7671 36.0994 32.6073 36.9801 32.6073C38.3011 32.6073 39.4801 32.9198 40.517 33.5448C41.554 34.1626 42.3707 35.0149 42.9673 36.1016C43.5639 37.1882 43.8622 38.4311 43.8622 39.8303C43.8622 41.272 43.5284 42.5575 42.8608 43.6868C42.2003 44.809 41.2805 45.6932 40.1016 46.3395C38.9297 46.9787 37.5732 47.2983 36.032 47.2983Z","fill":"white","key":2})]);
}

PrivacyScore5.defaultProps = {"width":"72","height":"72","viewBox":"0 0 72 72","fill":"none"};

module.exports = PrivacyScore5;

PrivacyScore5.default = PrivacyScore5;


/***/ }),

/***/ 348:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScoreDisabled1 (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36","cy":"36","r":"28","fill":"#CED1DD","key":0}),React.createElement("path",{"d":"M38.1832 24.5V46.3182H34.2308V28.3459H34.103L29 31.6058V27.9837L34.4226 24.5H38.1832Z","fill":"white","key":1}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M36 72C55.8823 72 72 55.8823 72 36C72 16.1177 55.8823 0 36 0C16.1177 0 0 16.1177 0 36C0 55.8823 16.1177 72 36 72ZM36 68C53.6731 68 68 53.6731 68 36C68 18.3269 53.6731 4 36 4C18.3269 4 4 18.3269 4 36C4 53.6731 18.3269 68 36 68Z","fill":"#FBFBFD","key":2}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M35.9286 71.9999L36 71.5V68C21.769 68 9.70765 58.7104 5.54926 45.8643L1.74292 47.0974C6.41415 61.5277 19.9498 71.9689 35.9286 71.9999Z","fill":"#CED1DD","key":3})]);
}

PrivacyScoreDisabled1.defaultProps = {"width":"72","height":"72","viewBox":"0 0 72 72","fill":"none"};

module.exports = PrivacyScoreDisabled1;

PrivacyScoreDisabled1.default = PrivacyScoreDisabled1;


/***/ }),

/***/ 349:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScoreDisabled2 (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36","cy":"36","r":"28","fill":"#CED1DD","key":0}),React.createElement("path",{"d":"M28.1278 46.6165V43.7614L35.7024 36.3359C36.4268 35.6044 37.0305 34.9545 37.5135 34.3864C37.9964 33.8182 38.3587 33.2678 38.6001 32.7351C38.8416 32.2024 38.9624 31.6342 38.9624 31.0305C38.9624 30.3416 38.8061 29.7521 38.4936 29.2621C38.1811 28.7649 37.7514 28.3814 37.2045 28.1115C36.6577 27.8416 36.0362 27.7067 35.3402 27.7067C34.6229 27.7067 33.9943 27.8558 33.4545 28.1541C32.9148 28.4453 32.4957 28.8608 32.1974 29.4006C31.9063 29.9403 31.7607 30.5831 31.7607 31.3288H28C28 29.9439 28.3161 28.7401 28.9482 27.7173C29.5803 26.6946 30.4503 25.9027 31.5582 25.3416C32.6733 24.7805 33.9517 24.5 35.3935 24.5C36.8565 24.5 38.142 24.7734 39.25 25.3203C40.358 25.8672 41.2173 26.6165 41.8281 27.5682C42.446 28.5199 42.755 29.6065 42.755 30.8281C42.755 31.6449 42.5987 32.4474 42.2862 33.2358C41.9737 34.0241 41.4233 34.8977 40.6349 35.8565C39.8537 36.8153 38.7564 37.9766 37.343 39.3402L33.5824 43.1648V43.3139H43.0852V46.6165H28.1278Z","fill":"white","key":1}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M36 72C55.8823 72 72 55.8823 72 36C72 16.1177 55.8823 0 36 0C16.1177 0 0 16.1177 0 36C0 55.8823 16.1177 72 36 72ZM36 68C53.6731 68 68 53.6731 68 36C68 18.3269 53.6731 4 36 4C18.3269 4 4 18.3269 4 36C4 53.6731 18.3269 68 36 68Z","fill":"#FBFBFD","key":2}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M35.9273 71.9999L36 71.7636V68C18.3269 68 4 53.6731 4 36C4 25.3374 9.21496 15.8928 17.2334 10.0777L14.9032 6.82617C5.87357 13.3672 0 23.9977 0 36C0 55.858 16.0785 71.9607 35.9273 71.9999Z","fill":"#CED1DD","key":3})]);
}

PrivacyScoreDisabled2.defaultProps = {"width":"72","height":"72","viewBox":"0 0 72 72","fill":"none"};

module.exports = PrivacyScoreDisabled2;

PrivacyScoreDisabled2.default = PrivacyScoreDisabled2;


/***/ }),

/***/ 350:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScoreDisabled3 (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36","cy":"36","r":"28","fill":"#CED1DD","key":0}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M36 72C55.8823 72 72 55.8823 72 36C72 16.1177 55.8823 0 36 0C16.1177 0 0 16.1177 0 36C0 55.8823 16.1177 72 36 72ZM36 68C53.6731 68 68 53.6731 68 36C68 18.3269 53.6731 4 36 4C18.3269 4 4 18.3269 4 36C4 53.6731 18.3269 68 36 68Z","fill":"#FBFBFD","key":1}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M54.925 10.1934C49.6247 6.29987 43.0809 4 36 4C18.3269 4 4 18.3269 4 36C4 53.6731 18.3269 68 36 68V71.5002L35.889 71.9998C16.0578 71.94 0 55.8452 0 36C0 16.1177 16.1177 0 36 0C43.966 0 51.3277 2.58736 57.2906 6.96754L54.925 10.1934Z","fill":"#CED1DD","key":2}),React.createElement("path",{"d":"M36.0007 46.9148C34.4666 46.9148 33.103 46.652 31.9098 46.1264C30.7237 45.6009 29.7862 44.8693 29.0973 43.9318C28.4084 42.9943 28.0426 41.9112 28 40.6825H32.0057C32.0412 41.272 32.2365 41.7869 32.5916 42.2273C32.9467 42.6605 33.419 42.9979 34.0085 43.2393C34.598 43.4808 35.2585 43.6016 35.9901 43.6016C36.7713 43.6016 37.4638 43.4666 38.0675 43.1967C38.6712 42.9197 39.1435 42.5362 39.4844 42.0462C39.8253 41.5561 39.9922 40.9915 39.9851 40.3523C39.9922 39.6918 39.8217 39.1094 39.4737 38.6051C39.1257 38.1009 38.6214 37.7067 37.9609 37.4226C37.3075 37.1385 36.5192 36.9964 35.5959 36.9964H33.6676V33.9496H35.5959C36.3558 33.9496 37.0199 33.8182 37.5881 33.5554C38.1634 33.2926 38.6143 32.9233 38.9411 32.4474C39.2678 31.9645 39.4276 31.407 39.4205 30.7749C39.4276 30.157 39.2891 29.6207 39.005 29.1662C38.728 28.7045 38.3338 28.3459 37.8224 28.0902C37.3182 27.8345 36.7251 27.7067 36.0433 27.7067C35.3757 27.7067 34.7578 27.8274 34.1896 28.0689C33.6214 28.3104 33.1634 28.6548 32.8153 29.1023C32.4673 29.5426 32.2827 30.0682 32.2614 30.679H28.4581C28.4865 29.4574 28.8381 28.3849 29.5128 27.4616C30.1946 26.5313 31.1037 25.8068 32.2401 25.2884C33.3764 24.7628 34.6513 24.5 36.0646 24.5C37.5206 24.5 38.7848 24.7734 39.8572 25.3203C40.9368 25.8601 41.7713 26.5881 42.3608 27.5043C42.9503 28.4205 43.245 29.4325 43.245 30.5405C43.2521 31.7692 42.8899 32.799 42.1584 33.63C41.4339 34.4609 40.4822 35.0043 39.3033 35.2599V35.4304C40.8374 35.6435 42.0128 36.2116 42.8295 37.1349C43.6534 38.0511 44.0618 39.1911 44.0547 40.5547C44.0547 41.7763 43.7067 42.87 43.0107 43.8359C42.3217 44.7947 41.37 45.5476 40.1555 46.0945C38.9482 46.6413 37.5632 46.9148 36.0007 46.9148Z","fill":"white","key":3})]);
}

PrivacyScoreDisabled3.defaultProps = {"width":"73","height":"72","viewBox":"0 0 73 72","fill":"none"};

module.exports = PrivacyScoreDisabled3;

PrivacyScoreDisabled3.default = PrivacyScoreDisabled3;


/***/ }),

/***/ 351:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScoreDisabled4 (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36.7911","cy":"36","r":"28","fill":"#CED1DD","key":0}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M36.7911 72C56.6734 72 72.7911 55.8823 72.7911 36C72.7911 16.1177 56.6734 0 36.7911 0C16.9089 0 0.791138 16.1177 0.791138 36C0.791138 55.8823 16.9089 72 36.7911 72ZM36.7911 68C54.4642 68 68.7911 53.6731 68.7911 36C68.7911 18.3269 54.4642 4 36.7911 4C19.118 4 4.79114 18.3269 4.79114 36C4.79114 53.6731 19.118 68 36.7911 68Z","fill":"#FBFBFD","key":1}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M67.1185 46.2371C68.2031 43.0229 68.791 39.5801 68.791 36C68.791 18.3269 54.4641 4.00002 36.791 4.00002C19.1179 4.00002 4.79102 18.3269 4.79102 36C4.79102 53.6034 19.0051 67.8869 36.582 67.9993V71.0009L36.333 71.9972C16.6618 71.7519 0.791016 55.7294 0.791016 36C0.791016 16.1178 16.9088 1.52588e-05 36.791 1.52588e-05C56.6733 1.52588e-05 72.791 16.1178 72.791 36C72.791 40.0039 72.1374 43.8551 70.931 47.4527L67.1185 46.2371Z","fill":"#CED1DD","key":2}),React.createElement("path",{"d":"M26.791 42.2699V39.1271L36.0488 24.5H38.6696V28.9744H37.0716L30.8393 38.8501V39.0206H43.7619V42.2699H26.791ZM37.1994 46.3182V41.3111L37.242 39.9048V24.5H40.9707V46.3182H37.1994Z","fill":"white","key":3})]);
}

PrivacyScoreDisabled4.defaultProps = {"width":"73","height":"72","viewBox":"0 0 73 72","fill":"none"};

module.exports = PrivacyScoreDisabled4;

PrivacyScoreDisabled4.default = PrivacyScoreDisabled4;


/***/ }),

/***/ 352:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScoreDisabled5 (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36.0001","cy":"36","r":"28","fill":"#CED1DD","key":0}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M36.0001 72C55.8824 72 72.0001 55.8823 72.0001 36C72.0001 16.1177 55.8824 0 36.0001 0C16.1179 0 0.00012207 16.1177 0.00012207 36C0.00012207 55.8823 16.1179 72 36.0001 72ZM36.0001 68C53.6732 68 68.0001 53.6731 68.0001 36C68.0001 18.3269 53.6732 4 36.0001 4C18.327 4 4.00012 18.3269 4.00012 36C4.00012 53.6731 18.327 68 36.0001 68Z","fill":"#CED1DD","key":1}),React.createElement("path",{"d":"M35.532 46.1165C34.1115 46.1165 32.8402 45.8501 31.718 45.3175C30.5959 44.7777 29.7045 44.0391 29.044 43.1016C28.3906 42.1641 28.0426 41.0916 28 39.8842H31.8352C31.9062 40.7791 32.2933 41.5107 32.9964 42.0788C33.6996 42.6399 34.5447 42.9205 35.532 42.9205C36.3061 42.9205 36.995 42.7429 37.5987 42.3878C38.2024 42.0327 38.6783 41.5391 39.0263 40.907C39.3743 40.2749 39.5447 39.554 39.5376 38.7443C39.5447 37.9205 39.3707 37.1889 39.0156 36.5497C38.6605 35.9105 38.174 35.4098 37.5561 35.0476C36.9382 34.6783 36.228 34.4936 35.4254 34.4936C34.772 34.4865 34.1293 34.6072 33.4972 34.8558C32.8651 35.1044 32.3643 35.4311 31.995 35.8359L28.4261 35.25L29.5661 24H42.2223V27.3026H32.8366L32.2081 33.0874H32.3359C32.7408 32.6115 33.3125 32.2173 34.0511 31.9048C34.7898 31.5852 35.5994 31.4254 36.4801 31.4254C37.8011 31.4254 38.9801 31.7379 40.017 32.3629C41.054 32.9808 41.8707 33.8331 42.4673 34.9197C43.0639 36.0064 43.3622 37.2493 43.3622 38.6484C43.3622 40.0902 43.0284 41.3757 42.3608 42.505C41.7003 43.6271 40.7805 44.5114 39.6016 45.1577C38.4297 45.7969 37.0732 46.1165 35.532 46.1165Z","fill":"white","key":2})]);
}

PrivacyScoreDisabled5.defaultProps = {"width":"72","height":"72","viewBox":"0 0 72 72","fill":"none"};

module.exports = PrivacyScoreDisabled5;

PrivacyScoreDisabled5.default = PrivacyScoreDisabled5;


/***/ }),

/***/ 353:
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__(0);

function PrivacyScoreNa (props) {
    return React.createElement("svg",props,[React.createElement("circle",{"cx":"36","cy":"36","r":"28","fill":"#6573FF","key":0}),React.createElement("path",{"fillRule":"evenodd","clipRule":"evenodd","d":"M36 72C55.8823 72 72 55.8823 72 36C72 16.1177 55.8823 0 36 0C16.1177 0 0 16.1177 0 36C0 55.8823 16.1177 72 36 72ZM36 68C53.6731 68 68 53.6731 68 36C68 18.3269 53.6731 4 36 4C18.3269 4 4 18.3269 4 36C4 53.6731 18.3269 68 36 68Z","fill":"#EBECF7","key":1}),React.createElement("path",{"d":"M33.2727 40.4588V40.0966C33.2798 38.8537 33.3899 37.8629 33.603 37.1243C33.8232 36.3857 34.1428 35.7891 34.5618 35.3345C34.9808 34.88 35.4851 34.468 36.0746 34.0987C36.5149 33.8146 36.9091 33.5199 37.2571 33.2145C37.6051 32.9091 37.8821 32.5717 38.0881 32.2024C38.294 31.826 38.397 31.407 38.397 30.9453C38.397 30.4553 38.2798 30.0256 38.0455 29.6562C37.8111 29.2869 37.495 29.0028 37.0973 28.804C36.7067 28.6051 36.2734 28.5057 35.7976 28.5057C35.3359 28.5057 34.8991 28.6087 34.4872 28.8146C34.0753 29.0135 33.7379 29.3118 33.4751 29.7095C33.2124 30.1001 33.0703 30.5866 33.049 31.169H28.7024C28.7379 29.7486 29.0788 28.5767 29.7251 27.6534C30.3714 26.723 31.2273 26.0305 32.2926 25.576C33.358 25.1143 34.5334 24.8835 35.8189 24.8835C37.2322 24.8835 38.4822 25.1179 39.5689 25.5866C40.6555 26.0483 41.5078 26.7195 42.1257 27.6001C42.7436 28.4808 43.0526 29.5426 43.0526 30.7855C43.0526 31.6165 42.9141 32.3551 42.6371 33.0014C42.3672 33.6406 41.9872 34.2088 41.4972 34.706C41.0071 35.196 40.4283 35.6399 39.7607 36.0376C39.1996 36.3714 38.7379 36.7195 38.3757 37.0817C38.0206 37.4439 37.7543 37.8629 37.5767 38.3388C37.4063 38.8146 37.3175 39.4006 37.3104 40.0966V40.4588H33.2727ZM35.3821 47.277C34.6719 47.277 34.0646 47.0284 33.5604 46.5312C33.0632 46.027 32.8182 45.4233 32.8253 44.7202C32.8182 44.0241 33.0632 43.4276 33.5604 42.9304C34.0646 42.4332 34.6719 42.1847 35.3821 42.1847C36.0568 42.1847 36.6499 42.4332 37.1612 42.9304C37.6726 43.4276 37.9318 44.0241 37.9389 44.7202C37.9318 45.1889 37.8075 45.6186 37.5661 46.0092C37.3317 46.3928 37.0227 46.7017 36.6392 46.9361C36.2557 47.1634 35.8366 47.277 35.3821 47.277Z","fill":"#2E39B3","key":2})]);
}

PrivacyScoreNa.defaultProps = {"width":"72","height":"72","viewBox":"0 0 72 72","fill":"none"};

module.exports = PrivacyScoreNa;

PrivacyScoreNa.default = PrivacyScoreNa;


/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(0);
var react_default = /*#__PURE__*/__webpack_require__.n(react);

// EXTERNAL MODULE: ./node_modules/@emotion/core/dist/core.browser.esm.js + 5 modules
var core_browser_esm = __webpack_require__(2);

// EXTERNAL MODULE: ./node_modules/react-redux/es/index.js + 24 modules
var es = __webpack_require__(39);

// EXTERNAL MODULE: ./src/privacy-search/utils/localization.ts
var localization = __webpack_require__(6);

// EXTERNAL MODULE: ./src/privacy-search/reducers/Screens.ts
var Screens = __webpack_require__(31);

// EXTERNAL MODULE: ./src/privacy-search/selectors/index.ts
var selectors = __webpack_require__(54);

// EXTERNAL MODULE: ./src/privacy-search/utils/display.ts
var display = __webpack_require__(334);

// CONCATENATED MODULE: ./src/privacy-search/components/Popup/Home/TrackerDisplay.tsx
function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }







var _ref =  true ? {
  name: "145uo23-trackerDisplayCss",
  styles: "align-items:center;background:#ffffff;border:1px solid #ebecf7;border-radius:4px;display:flex;flex-direction:column;height:87px;padding:24px 16px;position:relative;width:106px;margin-top:11px;;label:trackerDisplayCss;"
} : undefined;

var _ref2 =  true ? {
  name: "nsrf68-countCss",
  styles: "font-size:60px;font-weight:700;;label:countCss;"
} : undefined;

var _ref3 =  true ? {
  name: "vi1uy2-subtitleCss",
  styles: "font-size:14px;font-weight:600;;label:subtitleCss;"
} : undefined;

var TrackerDisplay = (_ref4) => {
  var {
    blocked,
    count,
    subtitle
  } = _ref4;
  var trackerDisplayCss = _ref;

  var resolveTitleBgColor = () => {
    if (blocked) {
      return '#29DDCC';
    }

    if (count > 0) {
      return '#eb5757';
    }

    return '#7f869f';
  };

  var titleCss = /*#__PURE__*/Object(core_browser_esm["a" /* css */])("align-items:center;background:", resolveTitleBgColor(), ";border-radius:17px;color:", blocked ? '#202945' : '#ffffff', ";display:flex;font-size:14px;font-weight:600;height:22px;line-height:1;padding:0 12px;position:absolute;top:-11px;transition:0.4s;white-space:nowrap;;label:titleCss;" + ( true ? "" : undefined));
  var countCss = _ref2;
  var subtitleCss = _ref3;
  return Object(core_browser_esm["b" /* jsx */])("div", {
    css: trackerDisplayCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: titleCss
  }, blocked ? Object(localization["b" /* getMessage */])('popupHomeBlockedLabel') : Object(localization["b" /* getMessage */])('popupHomeNotBlockedLabel')), Object(core_browser_esm["b" /* jsx */])("div", {
    css: countCss
  }, Object(display["a" /* getDisplayCount */])({
    count
  })), Object(core_browser_esm["b" /* jsx */])("div", {
    css: subtitleCss
  }, subtitle));
};
// EXTERNAL MODULE: ./src/privacy-search/store.ts + 13 modules
var store = __webpack_require__(55);

// EXTERNAL MODULE: ./src/privacy-search/icons/hidden-icon.svg
var hidden_icon = __webpack_require__(339);
var hidden_icon_default = /*#__PURE__*/__webpack_require__.n(hidden_icon);

// CONCATENATED MODULE: ./src/privacy-search/components/Popup/Home/BottomPanel.tsx
function BottomPanel_EMOTION_STRINGIFIED_CSS_ERROR_() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }








 // @ts-expect-error



var bottomPanelCss =  true ? {
  name: "19b7h2w-bottomPanelCss",
  styles: "height:232px;padding:12px;;label:bottomPanelCss;"
} : undefined;
var innerContainerCss =  true ? {
  name: "1qlp1hw-innerContainerCss",
  styles: "background:#fbfbfd;border:1px solid #ebecf7;border-radius:4px;display:flex;flex-wrap:wrap;height:200px;justify-content:space-between;padding:16px;;label:innerContainerCss;"
} : undefined;
var manageBtnCss =  true ? {
  name: "qmwges-manageBtnCss",
  styles: "background:#ffffff;border:1px solid #6573ff;border-radius:4px;color:#6573ff;cursor:pointer;font-size:14px;font-weight:500;height:38px;margin-top:auto;transition:0.4s;width:100%;&:focus{outline:none;}&:disabled{color:lightgray;cursor:initial;border:1px solid lightgray;};label:manageBtnCss;"
} : undefined;
var specialPageIcon =  true ? {
  name: "19qyi6-specialPageIcon",
  styles: "margin-bottom:14px;;label:specialPageIcon;"
} : undefined;
var specialPageDescriptionCss =  true ? {
  name: "16j8n8r-specialPageDescriptionCss",
  styles: "align-items:center;display:flex;flex-direction:column;font-size:14px;justify-content:center;padding:0 35px;line-height:18px;text-align:center;;label:specialPageDescriptionCss;"
} : undefined;
var BottomPanel = () => {
  var tabData = Object(es["c" /* useSelector */])(selectors["d" /* tabDataSelector */]);
  var {
    trackersBlocked,
    cookiesBlocked,
    isSpecialPage,
    isPrivacyEnabled
  } = tabData;
  var dispatch = Object(store["b" /* useAppDispatch */])();
  return Object(core_browser_esm["b" /* jsx */])("div", {
    css: bottomPanelCss
  }, Object(core_browser_esm["b" /* jsx */])("div", {
    css: innerContainerCss
  }, !isSpecialPage && Object(core_browser_esm["b" /* jsx */])(react_default.a.Fragment, null, Object(core_browser_esm["b" /* jsx */])(TrackerDisplay, {
    blocked: isPrivacyEnabled,
    count: trackersBlocked,
    subtitle: Object(localization["b" /* getMessage */])('popupHomeTrackersLabel')
  }), Object(core_browser_esm["b" /* jsx */])(TrackerDisplay, {
    blocked: isPrivacyEnabled,
    count: cookiesBlocked,
    subtitle: Object(localization["b" /* getMessage */])('popupHomeCookiesLabel')
  }), Object(core_browser_esm["b" /* jsx */])("button", {
    css: manageBtnCss,
    type: "submit",
    onClick: () => {
      dispatch(Screens["b" /* default */].actions.setActiveScreen(Screens["a" /* Screens */].ManageTrackers));
    },
    disabled: !isPrivacyEnabled || trackersBlocked === 0 && cookiesBlocked === 0
  }, Object(localization["b" /* getMessage */])('popupHomeManageTrackersBtn'))), isSpecialPage && Object(core_browser_esm["b" /* jsx */])("div", {
    css: specialPageDescriptionCss
  }, Object(core_browser_esm["b" /* jsx */])(hidden_icon_default.a, {
    css: specialPageIcon
  }), Object(localization["b" /* getMessage */])('popupHomeNACallout'))));
};
// EXTERNAL MODULE: ./src/privacy-search/icons/protection-on.svg
var protection_on = __webpack_require__(340);
var protection_on_default = /*#__PURE__*/__webpack_require__.n(protection_on);

// EXTERNAL MODULE: ./src/privacy-search/icons/protection-off.svg
var protection_off = __webpack_require__(341);
var protection_off_default = /*#__PURE__*/__webpack_require__.n(protection_off);

// EXTERNAL MODULE: ./src/privacy-search/icons/protection-off-gray.svg
var protection_off_gray = __webpack_require__(342);
var protection_off_gray_default = /*#__PURE__*/__webpack_require__.n(protection_off_gray);

// EXTERNAL MODULE: ./src/common/components/Toggle.tsx
var Toggle = __webpack_require__(182);

// CONCATENATED MODULE: ./src/privacy-search/components/Popup/Home/ProtectionToggleBar.tsx
function ProtectionToggleBar_EMOTION_STRINGIFIED_CSS_ERROR_() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }



 // @ts-expect-error

 // @ts-expect-error

 // @ts-expect-error






var protectionToggleBarCss =  true ? {
  name: "7q31hu-protectionToggleBarCss",
  styles: "align-items:center;border-top:1px solid #ebecf7;border-bottom:1px solid #ebecf7;display:flex;height:46px;padding:0 12px;;label:protectionToggleBarCss;"
} : undefined;
var protectionIconCss =  true ? {
  name: "reuvw2-protectionIconCss",
  styles: "margin-right:8px;;label:protectionIconCss;"
} : undefined;
var protectionDescriptionCss =  true ? {
  name: "1kyvj95-protectionDescriptionCss",
  styles: "font-size:14px;> span{font-weight:600;};label:protectionDescriptionCss;"
} : undefined;

var ProtectionToggleBar_ref =  true ? {
  name: "jhj8f5-ProtectionToggleBar",
  styles: "margin-left:auto;;label:ProtectionToggleBar;"
} : undefined;

var ProtectionToggleBar = () => {
  var tabData = Object(es["c" /* useSelector */])(selectors["d" /* tabDataSelector */]);
  var {
    isPrivacyEnabled,
    isSpecialPage
  } = tabData;
  return Object(core_browser_esm["b" /* jsx */])("div", {
    css: protectionToggleBarCss
  }, isPrivacyEnabled && Object(core_browser_esm["b" /* jsx */])(protection_on_default.a, {
    css: protectionIconCss
  }), !isPrivacyEnabled && !isSpecialPage && Object(core_browser_esm["b" /* jsx */])(protection_off_default.a, {
    css: protectionIconCss
  }), !isPrivacyEnabled && isSpecialPage && Object(core_browser_esm["b" /* jsx */])(protection_off_gray_default.a, {
    css: protectionIconCss
  }), Object(core_browser_esm["b" /* jsx */])("div", {
    css: protectionDescriptionCss
  }, Object(localization["b" /* getMessage */])('popupHomeToggleLabel'), ' ', Object(core_browser_esm["b" /* jsx */])("span", null, isPrivacyEnabled ? Object(localization["b" /* getMessage */])('popupHomeToggleOn') : Object(localization["b" /* getMessage */])('popupHomeToggleOff'))), Object(core_browser_esm["b" /* jsx */])("div", {
    css: ProtectionToggleBar_ref
  }, Object(core_browser_esm["b" /* jsx */])(Toggle["a" /* default */], {
    id: "privacy-toggle",
    height: 24,
    width: 40,
    hexColor: "#6573ff",
    checked: isPrivacyEnabled,
    animate: false,
    onChange: () => {
      if (!isSpecialPage) {
        var togglePrivacy = isPrivacyEnabled ? 'deactivatePrivacy' : 'activatePrivacy';
        browser.runtime.sendMessage({
          message: togglePrivacy
        }).then(() => {
          browser.tabs.reload();
          window.close();
        });
      }
    }
  })));
};
// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-1.svg
var privacy_score_1 = __webpack_require__(343);
var privacy_score_1_default = /*#__PURE__*/__webpack_require__.n(privacy_score_1);

// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-2.svg
var privacy_score_2 = __webpack_require__(344);
var privacy_score_2_default = /*#__PURE__*/__webpack_require__.n(privacy_score_2);

// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-3.svg
var privacy_score_3 = __webpack_require__(345);
var privacy_score_3_default = /*#__PURE__*/__webpack_require__.n(privacy_score_3);

// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-4.svg
var privacy_score_4 = __webpack_require__(346);
var privacy_score_4_default = /*#__PURE__*/__webpack_require__.n(privacy_score_4);

// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-5.svg
var privacy_score_5 = __webpack_require__(347);
var privacy_score_5_default = /*#__PURE__*/__webpack_require__.n(privacy_score_5);

// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-disabled-1.svg
var privacy_score_disabled_1 = __webpack_require__(348);
var privacy_score_disabled_1_default = /*#__PURE__*/__webpack_require__.n(privacy_score_disabled_1);

// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-disabled-2.svg
var privacy_score_disabled_2 = __webpack_require__(349);
var privacy_score_disabled_2_default = /*#__PURE__*/__webpack_require__.n(privacy_score_disabled_2);

// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-disabled-3.svg
var privacy_score_disabled_3 = __webpack_require__(350);
var privacy_score_disabled_3_default = /*#__PURE__*/__webpack_require__.n(privacy_score_disabled_3);

// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-disabled-4.svg
var privacy_score_disabled_4 = __webpack_require__(351);
var privacy_score_disabled_4_default = /*#__PURE__*/__webpack_require__.n(privacy_score_disabled_4);

// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-disabled-5.svg
var privacy_score_disabled_5 = __webpack_require__(352);
var privacy_score_disabled_5_default = /*#__PURE__*/__webpack_require__.n(privacy_score_disabled_5);

// EXTERNAL MODULE: ./src/privacy-search/icons/privacy-score-na.svg
var privacy_score_na = __webpack_require__(353);
var privacy_score_na_default = /*#__PURE__*/__webpack_require__.n(privacy_score_na);

// CONCATENATED MODULE: ./src/privacy-search/components/Popup/Home/TopPanel.tsx
function TopPanel_EMOTION_STRINGIFIED_CSS_ERROR_() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }




 // @ts-expect-error

 // @ts-expect-error

 // @ts-expect-error

 // @ts-expect-error

 // @ts-expect-error

 // @ts-expect-error

 // @ts-expect-error

 // @ts-expect-error

 // @ts-expect-error

 // @ts-expect-error

 // @ts-expect-error




var topPanelCss =  true ? {
  name: "16y3fpj-topPanelCss",
  styles: "align-items:center;display:flex;height:100px;padding:12px;position:relative;;label:topPanelCss;"
} : undefined;
var summaryCss =  true ? {
  name: "1j6b6yg-summaryCss",
  styles: "display:flex;flex-direction:column;justify-content:space-between;padding:4px 0;;label:summaryCss;"
} : undefined;
var scoreCss =  true ? {
  name: "wy1nt5-scoreCss",
  styles: "font-size:14px;margin-bottom:5px;> span{font-weight:600;letter-spacing:2px;};label:scoreCss;"
} : undefined;
var domainCss =  true ? {
  name: "1s27053-domainCss",
  styles: "font-size:22px;font-weight:600;margin-bottom:8px;max-width:240px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;;label:domainCss;"
} : undefined;
var scoreDescriptionCss =  true ? {
  name: "ol2ani-scoreDescriptionCss",
  styles: "color:#7f869f;font-size:13px;;label:scoreDescriptionCss;"
} : undefined;
var TopPanel_specialPageDescriptionCss =  true ? {
  name: "1oiagdo-specialPageDescriptionCss",
  styles: "color:#ced1dd;font-size:22px;font-weight:600;line-height:24px;white-space:pre-wrap;;label:specialPageDescriptionCss;"
} : undefined;
var TopPanel = () => {
  var tabData = Object(es["c" /* useSelector */])(selectors["d" /* tabDataSelector */]);
  var {
    isPrivacyEnabled,
    isSpecialPage,
    score,
    tld
  } = tabData;
  var scoreIconCss = /*#__PURE__*/Object(core_browser_esm["a" /* css */])("flex-shrink:0;margin-right:12px;opacity:", isPrivacyEnabled ? 1 : 0, ";transition:opacity 0.4s;;label:scoreIconCss;" + ( true ? "" : undefined));
  var scoreIconDisabledCss = /*#__PURE__*/Object(core_browser_esm["a" /* css */])("opacity:", isPrivacyEnabled ? 0 : 1, ";position:absolute;transition:opacity 0.4s;;label:scoreIconDisabledCss;" + ( true ? "" : undefined));
  return Object(core_browser_esm["b" /* jsx */])("div", {
    css: topPanelCss
  }, isSpecialPage && Object(core_browser_esm["b" /* jsx */])(react_default.a.Fragment, null, Object(core_browser_esm["b" /* jsx */])(privacy_score_na_default.a, {
    css: scoreIconCss
  }), " ", Object(core_browser_esm["b" /* jsx */])(privacy_score_na_default.a, {
    css: scoreIconDisabledCss
  })), score === 1 && Object(core_browser_esm["b" /* jsx */])(react_default.a.Fragment, null, Object(core_browser_esm["b" /* jsx */])(privacy_score_1_default.a, {
    css: scoreIconCss
  }), " ", Object(core_browser_esm["b" /* jsx */])(privacy_score_disabled_1_default.a, {
    css: scoreIconDisabledCss
  })), score === 2 && Object(core_browser_esm["b" /* jsx */])(react_default.a.Fragment, null, Object(core_browser_esm["b" /* jsx */])(privacy_score_2_default.a, {
    css: scoreIconCss
  }), " ", Object(core_browser_esm["b" /* jsx */])(privacy_score_disabled_2_default.a, {
    css: scoreIconDisabledCss
  })), score === 3 && Object(core_browser_esm["b" /* jsx */])(react_default.a.Fragment, null, Object(core_browser_esm["b" /* jsx */])(privacy_score_3_default.a, {
    css: scoreIconCss
  }), " ", Object(core_browser_esm["b" /* jsx */])(privacy_score_disabled_3_default.a, {
    css: scoreIconDisabledCss
  })), score === 4 && Object(core_browser_esm["b" /* jsx */])(react_default.a.Fragment, null, Object(core_browser_esm["b" /* jsx */])(privacy_score_4_default.a, {
    css: scoreIconCss
  }), " ", Object(core_browser_esm["b" /* jsx */])(privacy_score_disabled_4_default.a, {
    css: scoreIconDisabledCss
  })), score === 5 && Object(core_browser_esm["b" /* jsx */])(react_default.a.Fragment, null, Object(core_browser_esm["b" /* jsx */])(privacy_score_5_default.a, {
    css: scoreIconCss
  }), " ", Object(core_browser_esm["b" /* jsx */])(privacy_score_disabled_5_default.a, {
    css: scoreIconDisabledCss
  })), Object(core_browser_esm["b" /* jsx */])("div", {
    css: summaryCss
  }, !isSpecialPage && Object(core_browser_esm["b" /* jsx */])(react_default.a.Fragment, null, Object(core_browser_esm["b" /* jsx */])("div", {
    css: scoreCss
  }, Object(localization["b" /* getMessage */])('popupHomeScoreLabel'), ": ", Object(core_browser_esm["b" /* jsx */])("span", null, score, "/5")), Object(core_browser_esm["b" /* jsx */])("div", {
    css: domainCss
  }, tld), Object(core_browser_esm["b" /* jsx */])("div", {
    css: scoreDescriptionCss
  }, score === 5 && Object(localization["b" /* getMessage */])('popupHomeScoreFiveDescription'), score === 4 && Object(localization["b" /* getMessage */])('popupHomeScoreFourDescription'), score === 3 && Object(localization["b" /* getMessage */])('popupHomeScoreThreeDescription'), score === 2 && Object(localization["b" /* getMessage */])('popupHomeScoreTwoDescription'), score === 1 && Object(localization["b" /* getMessage */])('popupHomeScoreOneDescription'))), isSpecialPage && Object(core_browser_esm["b" /* jsx */])("div", {
    css: TopPanel_specialPageDescriptionCss
  }, Object(localization["b" /* getMessage */])('popupHomeScoreNA'))));
};
// CONCATENATED MODULE: ./src/privacy-search/components/Popup/Home/index.tsx






var Home = () => {
  return Object(core_browser_esm["b" /* jsx */])(react_default.a.Fragment, null, Object(core_browser_esm["b" /* jsx */])(TopPanel, null), Object(core_browser_esm["b" /* jsx */])(ProtectionToggleBar, null), Object(core_browser_esm["b" /* jsx */])(BottomPanel, null));
};

/* harmony default export */ var Popup_Home = __webpack_exports__["default"] = (Home);

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY29tbW9uL2NvbXBvbmVudHMvVG9nZ2xlLnRzeCIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvdXRpbHMvZGlzcGxheS50cyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvaGlkZGVuLWljb24uc3ZnIiwid2VicGFjazovLy8uL3NyYy9wcml2YWN5LXNlYXJjaC9pY29ucy9wcm90ZWN0aW9uLW9uLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJvdGVjdGlvbi1vZmYuc3ZnIiwid2VicGFjazovLy8uL3NyYy9wcml2YWN5LXNlYXJjaC9pY29ucy9wcm90ZWN0aW9uLW9mZi1ncmF5LnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS0xLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS0yLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS0zLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS00LnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS01LnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS1kaXNhYmxlZC0xLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS1kaXNhYmxlZC0yLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS1kaXNhYmxlZC0zLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS1kaXNhYmxlZC00LnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS1kaXNhYmxlZC01LnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvaWNvbnMvcHJpdmFjeS1zY29yZS1uYS5zdmciLCJ3ZWJwYWNrOi8vLy4vc3JjL3ByaXZhY3ktc2VhcmNoL2NvbXBvbmVudHMvUG9wdXAvSG9tZS9UcmFja2VyRGlzcGxheS50c3giLCJ3ZWJwYWNrOi8vLy4vc3JjL3ByaXZhY3ktc2VhcmNoL2NvbXBvbmVudHMvUG9wdXAvSG9tZS9Cb3R0b21QYW5lbC50c3giLCJ3ZWJwYWNrOi8vLy4vc3JjL3ByaXZhY3ktc2VhcmNoL2NvbXBvbmVudHMvUG9wdXAvSG9tZS9Qcm90ZWN0aW9uVG9nZ2xlQmFyLnRzeCIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvY29tcG9uZW50cy9Qb3B1cC9Ib21lL1RvcFBhbmVsLnRzeCIsIndlYnBhY2s6Ly8vLi9zcmMvcHJpdmFjeS1zZWFyY2gvY29tcG9uZW50cy9Qb3B1cC9Ib21lL2luZGV4LnRzeCJdLCJuYW1lcyI6WyJUb2dnbGUiLCJpZCIsImhlaWdodCIsIndpZHRoIiwiaGV4Q29sb3IiLCJjaGVja2VkIiwiYW5pbWF0ZSIsIm9uQ2hhbmdlIiwic2xpZGVyRGlhbWV0ZXIiLCJ0b2dnbGVDc3MiLCJjc3MiLCJldmVudCIsImdldERpc3BsYXlDb3VudCIsImNvdW50Iiwia1N0YXJ0IiwibVN0YXJ0IiwiTWF0aCIsImZsb29yIiwiVHJhY2tlckRpc3BsYXkiLCJibG9ja2VkIiwic3VidGl0bGUiLCJ0cmFja2VyRGlzcGxheUNzcyIsInJlc29sdmVUaXRsZUJnQ29sb3IiLCJ0aXRsZUNzcyIsImNvdW50Q3NzIiwic3VidGl0bGVDc3MiLCJnZXRNZXNzYWdlIiwiYm90dG9tUGFuZWxDc3MiLCJpbm5lckNvbnRhaW5lckNzcyIsIm1hbmFnZUJ0bkNzcyIsInNwZWNpYWxQYWdlSWNvbiIsInNwZWNpYWxQYWdlRGVzY3JpcHRpb25Dc3MiLCJCb3R0b21QYW5lbCIsInRhYkRhdGEiLCJ1c2VTZWxlY3RvciIsInRhYkRhdGFTZWxlY3RvciIsInRyYWNrZXJzQmxvY2tlZCIsImNvb2tpZXNCbG9ja2VkIiwiaXNTcGVjaWFsUGFnZSIsImlzUHJpdmFjeUVuYWJsZWQiLCJkaXNwYXRjaCIsInVzZUFwcERpc3BhdGNoIiwic2NyZWVuTWFuYWdlbWVudFNsaWNlIiwiYWN0aW9ucyIsInNldEFjdGl2ZVNjcmVlbiIsIlNjcmVlbnMiLCJNYW5hZ2VUcmFja2VycyIsInByb3RlY3Rpb25Ub2dnbGVCYXJDc3MiLCJwcm90ZWN0aW9uSWNvbkNzcyIsInByb3RlY3Rpb25EZXNjcmlwdGlvbkNzcyIsIlByb3RlY3Rpb25Ub2dnbGVCYXIiLCJ0b2dnbGVQcml2YWN5IiwiYnJvd3NlciIsInJ1bnRpbWUiLCJzZW5kTWVzc2FnZSIsIm1lc3NhZ2UiLCJ0aGVuIiwidGFicyIsInJlbG9hZCIsIndpbmRvdyIsImNsb3NlIiwidG9wUGFuZWxDc3MiLCJzdW1tYXJ5Q3NzIiwic2NvcmVDc3MiLCJkb21haW5Dc3MiLCJzY29yZURlc2NyaXB0aW9uQ3NzIiwiVG9wUGFuZWwiLCJzY29yZSIsInRsZCIsInNjb3JlSWNvbkNzcyIsInNjb3JlSWNvbkRpc2FibGVkQ3NzIiwiSG9tZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBWUEsSUFBTUEsTUFBdUIsR0FBRyxVQUE4RTtBQUFBLE1BQTdFO0FBQUNDLE1BQUQ7QUFBS0MsVUFBTDtBQUFhQyxTQUFiO0FBQW9CQyxZQUFwQjtBQUE4QkMsV0FBTyxHQUFHLEtBQXhDO0FBQStDQyxXQUFPLEdBQUcsSUFBekQ7QUFBK0RDLFlBQVEsRUFBUkE7QUFBL0QsR0FBNkU7QUFDMUcsTUFBTUMsY0FBYyxHQUFHTixNQUFNLEdBQUcsSUFBaEM7QUFFQSxNQUFNTyxTQUFTLGdCQUFHQyxpRUFBSCwyREFJR1IsTUFKSCxlQUtFQyxLQUxGLG1HQWdCVUQsTUFoQlYsa0RBcUJMSSxPQUFPLElBQUksbUJBckJOLGdEQXlCYUYsUUF6QmIsOERBOEJVSSxjQTlCVixrRUFpQ0dBLGNBakNILHFCQWtDUUEsY0FsQ1Isc0NBb0NFQSxjQXBDRixTQXNDTEYsT0FBTyxJQUFJLG1CQXRDTixvRkEyQ2lCSCxLQUFLLEdBQUdELE1BM0N6QixzREFBZjtBQStDQSxTQUNJO0FBQUssT0FBRyxFQUFFTztBQUFWLEtBQ0k7QUFBTyxhQUFTLEVBQUMsUUFBakI7QUFBMEIsV0FBTyxFQUFFUjtBQUFuQyxLQUNJO0FBQ0ksTUFBRSxFQUFFQSxFQURSO0FBRUksUUFBSSxFQUFDLFVBRlQ7QUFHSSxXQUFPLEVBQUVJLE9BSGIsQ0FJSTtBQUpKO0FBS0ksWUFBUSxFQUFHTSxLQUFELElBQWdEO0FBQ3RELFVBQUlKLFNBQUosRUFBYztBQUNWQSxpQkFBUTtBQUNYO0FBQ0o7QUFUTCxJQURKLEVBWUk7QUFBTSxhQUFTLEVBQUM7QUFBaEIsSUFaSixDQURKLENBREo7QUFrQkgsQ0FwRUQ7O0FBc0VlUCwrREFBZixFOzs7Ozs7OztBQzdFQTtBQUFPLElBQU1ZLGVBQWUsR0FBRyxVQUFvRTtBQUFBLE1BQW5FO0FBQUNDLFNBQUQ7QUFBUUMsVUFBTSxHQUFHLElBQWpCO0FBQXVCQyxVQUFNLEdBQUc7QUFBaEMsR0FBbUU7O0FBQy9GO0FBQ0EsTUFBSUYsS0FBSyxJQUFJRSxNQUFiLEVBQXFCO0FBQ2pCLHFCQUFVQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0osS0FBSyxHQUFHLE9BQW5CLENBQVY7QUFDSCxHQUo4RixDQU0vRjs7O0FBQ0EsTUFBSUEsS0FBSyxJQUFJQyxNQUFiLEVBQXFCO0FBQ2pCLHFCQUFVRSxJQUFJLENBQUNDLEtBQUwsQ0FBV0osS0FBSyxHQUFHLElBQW5CLENBQVY7QUFDSDs7QUFFRCxTQUFPQSxLQUFQO0FBQ0gsQ0FaTSxDOzs7Ozs7O0FDTlAsWUFBWSxtQkFBTyxDQUFDLENBQU87O0FBRTNCO0FBQ0Esd0VBQXdFLDhnQ0FBOGdDLDhCQUE4QiwycEJBQTJwQiw4QkFBOEIsUUFBUSxpQ0FBaUMsYUFBYSw2QkFBNkIsNEVBQTRFO0FBQzU4RDs7QUFFQSwyQkFBMkI7O0FBRTNCOztBQUVBOzs7Ozs7OztBQ1ZBLFlBQVksbUJBQU8sQ0FBQyxDQUFPOztBQUUzQjtBQUNBLDBFQUEwRSxtREFBbUQsMkJBQTJCLGlDQUFpQyw2QkFBNkIscUlBQXFJLCtCQUErQixRQUFRLGlDQUFpQyxhQUFhLDZCQUE2Qix1RUFBdUU7QUFDcGhCOztBQUVBLDZCQUE2Qjs7QUFFN0I7O0FBRUE7Ozs7Ozs7O0FDVkEsWUFBWSxtQkFBTyxDQUFDLENBQU87O0FBRTNCO0FBQ0EsMEVBQTBFLG1EQUFtRCw4QkFBOEIsaUVBQWlFLGdDQUFnQyxvREFBb0Q7QUFDaFQ7O0FBRUEsOEJBQThCOztBQUU5Qjs7QUFFQTs7Ozs7Ozs7QUNWQSxZQUFZLG1CQUFPLENBQUMsQ0FBTzs7QUFFM0I7QUFDQSwwRUFBMEUsbURBQW1ELDhCQUE4QixpRUFBaUUsZ0NBQWdDLG9EQUFvRDtBQUNoVDs7QUFFQSxrQ0FBa0M7O0FBRWxDOztBQUVBOzs7Ozs7OztBQ1ZBLFlBQVksbUJBQU8sQ0FBQyxDQUFPOztBQUUzQjtBQUNBLDBFQUEwRSxzREFBc0QsOEJBQThCLG1IQUFtSCw4QkFBOEIsNFNBQTRTLDhCQUE4QixnTkFBZ047QUFDejBCOztBQUVBLDhCQUE4Qjs7QUFFOUI7O0FBRUE7Ozs7Ozs7O0FDVkEsWUFBWSxtQkFBTyxDQUFDLENBQU87O0FBRTNCO0FBQ0EsMEVBQTBFLHNEQUFzRCw4QkFBOEIsbThCQUFtOEIsOEJBQThCLDRTQUE0Uyw4QkFBOEIscVFBQXFRO0FBQzlzRDs7QUFFQSw4QkFBOEI7O0FBRTlCOztBQUVBOzs7Ozs7OztBQ1ZBLFlBQVksbUJBQU8sQ0FBQyxDQUFPOztBQUUzQjtBQUNBLDBFQUEwRSxzREFBc0QsOEJBQThCLDRTQUE0Uyw4QkFBOEIsa1RBQWtULDhCQUE4Qiw4cERBQThwRDtBQUN0OUU7O0FBRUEsOEJBQThCOztBQUU5Qjs7QUFFQTs7Ozs7Ozs7QUNWQSxZQUFZLG1CQUFPLENBQUMsQ0FBTzs7QUFFM0I7QUFDQSwwRUFBMEUsMkRBQTJELDhCQUE4Qiw2WUFBNlksOEJBQThCLG1jQUFtYyw4QkFBOEIsK01BQStNO0FBQzl2Qzs7QUFFQSw4QkFBOEI7O0FBRTlCOztBQUVBOzs7Ozs7OztBQ1ZBLFlBQVksbUJBQU8sQ0FBQyxDQUFPOztBQUUzQjtBQUNBLDBFQUEwRSxzREFBc0QsZ0NBQWdDLHlGQUF5Riw4QkFBOEIscW1DQUFxbUM7QUFDNTNDOztBQUVBLDhCQUE4Qjs7QUFFOUI7O0FBRUE7Ozs7Ozs7O0FDVkEsWUFBWSxtQkFBTyxDQUFDLENBQU87O0FBRTNCO0FBQ0EsMEVBQTBFLHNEQUFzRCw4QkFBOEIsbUhBQW1ILDhCQUE4Qiw0U0FBNFMsOEJBQThCLGdOQUFnTjtBQUN6MEI7O0FBRUEsc0NBQXNDOztBQUV0Qzs7QUFFQTs7Ozs7Ozs7QUNWQSxZQUFZLG1CQUFPLENBQUMsQ0FBTzs7QUFFM0I7QUFDQSwwRUFBMEUsc0RBQXNELDhCQUE4QixtOEJBQW04Qiw4QkFBOEIsNFNBQTRTLDhCQUE4QixxUUFBcVE7QUFDOXNEOztBQUVBLHNDQUFzQzs7QUFFdEM7O0FBRUE7Ozs7Ozs7O0FDVkEsWUFBWSxtQkFBTyxDQUFDLENBQU87O0FBRTNCO0FBQ0EsMEVBQTBFLHNEQUFzRCw4QkFBOEIsNFNBQTRTLDhCQUE4QixrVEFBa1QsOEJBQThCLDhwREFBOHBEO0FBQ3Q5RTs7QUFFQSxzQ0FBc0M7O0FBRXRDOztBQUVBOzs7Ozs7OztBQ1ZBLFlBQVksbUJBQU8sQ0FBQyxDQUFPOztBQUUzQjtBQUNBLDBFQUEwRSwyREFBMkQsOEJBQThCLDZZQUE2WSw4QkFBOEIsbWZBQW1mLDhCQUE4QiwrTUFBK007QUFDOXlDOztBQUVBLHNDQUFzQzs7QUFFdEM7O0FBRUE7Ozs7Ozs7O0FDVkEsWUFBWSxtQkFBTyxDQUFDLENBQU87O0FBRTNCO0FBQ0EsMEVBQTBFLDJEQUEyRCw4QkFBOEIsbVpBQW1aLDhCQUE4Qiw0bENBQTRsQztBQUNockQ7O0FBRUEsc0NBQXNDOztBQUV0Qzs7QUFFQTs7Ozs7Ozs7QUNWQSxZQUFZLG1CQUFPLENBQUMsQ0FBTzs7QUFFM0I7QUFDQSwwRUFBMEUsc0RBQXNELDhCQUE4Qiw0U0FBNFMsOEJBQThCLDBuREFBMG5EO0FBQ2xtRTs7QUFFQSwrQkFBK0I7O0FBRS9COztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVkE7QUFDQTtBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQVFPLElBQU1LLGNBQXVDLEdBQUcsV0FBZ0M7QUFBQSxNQUEvQjtBQUFDQyxXQUFEO0FBQVVOLFNBQVY7QUFBaUJPO0FBQWpCLEdBQStCO0FBQ25GLE1BQU1DLGlCQUFpQixPQUF2Qjs7QUFjQSxNQUFNQyxtQkFBbUIsR0FBRyxNQUFNO0FBQzlCLFFBQUlILE9BQUosRUFBYTtBQUNULGFBQU8sU0FBUDtBQUNIOztBQUNELFFBQUlOLEtBQUssR0FBRyxDQUFaLEVBQWU7QUFDWCxhQUFPLFNBQVA7QUFDSDs7QUFDRCxXQUFPLFNBQVA7QUFDSCxHQVJEOztBQVVBLE1BQU1VLFFBQVEsZ0JBQUdiLHVDQUFILG1DQUVJWSxtQkFBbUIsRUFGdkIsZ0NBSURILE9BQU8sR0FBRyxTQUFILEdBQWUsU0FKckIscU1BQWQ7QUFpQkEsTUFBTUssUUFBUSxRQUFkO0FBS0EsTUFBTUMsV0FBVyxRQUFqQjtBQUtBLFNBQ0k7QUFBSyxPQUFHLEVBQUVKO0FBQVYsS0FDSTtBQUFLLE9BQUcsRUFBRUU7QUFBVixLQUNLSixPQUFPLEdBQUdPLDBDQUFVLENBQUMsdUJBQUQsQ0FBYixHQUF5Q0EsMENBQVUsQ0FBQywwQkFBRCxDQUQvRCxDQURKLEVBSUk7QUFBSyxPQUFHLEVBQUVGO0FBQVYsS0FBcUJaLDBDQUFlLENBQUM7QUFBQ0M7QUFBRCxHQUFELENBQXBDLENBSkosRUFLSTtBQUFLLE9BQUcsRUFBRVk7QUFBVixLQUF3QkwsUUFBeEIsQ0FMSixDQURKO0FBU0gsQ0E3RE0sQzs7Ozs7Ozs7Ozs7QUNaUDtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtDQUdBOztBQUNBOztBQUVBLElBQU1PLGNBQWMsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFwQjtBQUtBLElBQU1DLGlCQUFpQixHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQXZCO0FBV0EsSUFBTUMsWUFBWSxHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQWxCO0FBd0JBLElBQU1DLGVBQWUsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFyQjtBQUlBLElBQU1DLHlCQUF5QixHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQS9CO0FBV08sSUFBTUMsV0FBZSxHQUFHLE1BQU07QUFDakMsTUFBTUMsT0FBTyxHQUFHQyxpQ0FBVyxDQUFDQyxvQ0FBRCxDQUEzQjtBQUNBLE1BQU07QUFBQ0MsbUJBQUQ7QUFBa0JDLGtCQUFsQjtBQUFrQ0MsaUJBQWxDO0FBQWlEQztBQUFqRCxNQUFxRU4sT0FBM0U7QUFFQSxNQUFNTyxRQUFRLEdBQUdDLHVDQUFjLEVBQS9CO0FBRUEsU0FDSTtBQUFLLE9BQUcsRUFBRWQ7QUFBVixLQUNJO0FBQUssT0FBRyxFQUFFQztBQUFWLEtBQ0ssQ0FBQ1UsYUFBRCxJQUNHLHdFQUNJLHdDQUFDLGNBQUQ7QUFDSSxXQUFPLEVBQUVDLGdCQURiO0FBRUksU0FBSyxFQUFFSCxlQUZYO0FBR0ksWUFBUSxFQUFFViwwQ0FBVSxDQUFDLHdCQUFEO0FBSHhCLElBREosRUFNSSx3Q0FBQyxjQUFEO0FBQ0ksV0FBTyxFQUFFYSxnQkFEYjtBQUVJLFNBQUssRUFBRUYsY0FGWDtBQUdJLFlBQVEsRUFBRVgsMENBQVUsQ0FBQyx1QkFBRDtBQUh4QixJQU5KLEVBV0k7QUFDSSxPQUFHLEVBQUVHLFlBRFQ7QUFFSSxRQUFJLEVBQUMsUUFGVDtBQUdJLFdBQU8sRUFBRSxNQUFNO0FBQ1hXLGNBQVEsQ0FBQ0UsMEJBQXFCLENBQUNDLE9BQXRCLENBQThCQyxlQUE5QixDQUE4Q0MsMEJBQU8sQ0FBQ0MsY0FBdEQsQ0FBRCxDQUFSO0FBQ0gsS0FMTDtBQU1JLFlBQVEsRUFBRSxDQUFDUCxnQkFBRCxJQUFzQkgsZUFBZSxLQUFLLENBQXBCLElBQXlCQyxjQUFjLEtBQUs7QUFOaEYsS0FRS1gsMENBQVUsQ0FBQyw0QkFBRCxDQVJmLENBWEosQ0FGUixFQXlCS1ksYUFBYSxJQUNWO0FBQUssT0FBRyxFQUFFUDtBQUFWLEtBQ0ksd0NBQUMscUJBQUQ7QUFBWSxPQUFHLEVBQUVEO0FBQWpCLElBREosRUFFS0osMENBQVUsQ0FBQyxvQkFBRCxDQUZmLENBMUJSLENBREosQ0FESjtBQW9DSCxDQTFDTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcEVQO0FBQ0E7Q0FHQTs7Q0FFQTs7Q0FFQTs7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7QUFFQSxJQUFNcUIsc0JBQXNCLEdBQUc7QUFBSDtBQUFBO0FBQUEsYUFBNUI7QUFTQSxJQUFNQyxpQkFBaUIsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUF2QjtBQUlBLElBQU1DLHdCQUF3QixHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQTlCOzs7Ozs7O0FBUU8sSUFBTUMsbUJBQXVCLEdBQUcsTUFBTTtBQUN6QyxNQUFNakIsT0FBTyxHQUFHQyxpQ0FBVyxDQUFDQyxvQ0FBRCxDQUEzQjtBQUNBLE1BQU07QUFBQ0ksb0JBQUQ7QUFBbUJEO0FBQW5CLE1BQW9DTCxPQUExQztBQUVBLFNBQ0k7QUFBSyxPQUFHLEVBQUVjO0FBQVYsS0FDS1IsZ0JBQWdCLElBQUksd0NBQUMsdUJBQUQ7QUFBYyxPQUFHLEVBQUVTO0FBQW5CLElBRHpCLEVBRUssQ0FBQ1QsZ0JBQUQsSUFBcUIsQ0FBQ0QsYUFBdEIsSUFBdUMsd0NBQUMsd0JBQUQ7QUFBZSxPQUFHLEVBQUVVO0FBQXBCLElBRjVDLEVBR0ssQ0FBQ1QsZ0JBQUQsSUFBcUJELGFBQXJCLElBQXNDLHdDQUFDLDZCQUFEO0FBQW1CLE9BQUcsRUFBRVU7QUFBeEIsSUFIM0MsRUFJSTtBQUFLLE9BQUcsRUFBRUM7QUFBVixLQUNLdkIsMENBQVUsQ0FBQyxzQkFBRCxDQURmLEVBQ3lDLEdBRHpDLEVBRUksc0RBQU9hLGdCQUFnQixHQUFHYiwwQ0FBVSxDQUFDLG1CQUFELENBQWIsR0FBcUNBLDBDQUFVLENBQUMsb0JBQUQsQ0FBdEUsQ0FGSixDQUpKLEVBUUk7QUFDSSxPQUFHO0FBRFAsS0FLSSx3Q0FBQyx5QkFBRDtBQUNJLE1BQUUsRUFBQyxnQkFEUDtBQUVJLFVBQU0sRUFBRSxFQUZaO0FBR0ksU0FBSyxFQUFFLEVBSFg7QUFJSSxZQUFRLEVBQUMsU0FKYjtBQUtJLFdBQU8sRUFBRWEsZ0JBTGI7QUFNSSxXQUFPLEVBQUUsS0FOYjtBQU9JLFlBQVEsRUFBRSxNQUFNO0FBQ1osVUFBSSxDQUFDRCxhQUFMLEVBQW9CO0FBQ2hCLFlBQU1hLGFBQWEsR0FBR1osZ0JBQWdCLEdBQUcsbUJBQUgsR0FBeUIsaUJBQS9EO0FBQ0FhLGVBQU8sQ0FBQ0MsT0FBUixDQUFnQkMsV0FBaEIsQ0FBNEI7QUFBQ0MsaUJBQU8sRUFBRUo7QUFBVixTQUE1QixFQUFzREssSUFBdEQsQ0FBMkQsTUFBTTtBQUM3REosaUJBQU8sQ0FBQ0ssSUFBUixDQUFhQyxNQUFiO0FBQ0FDLGdCQUFNLENBQUNDLEtBQVA7QUFDSCxTQUhEO0FBSUg7QUFDSjtBQWZMLElBTEosQ0FSSixDQURKO0FBa0NILENBdENNLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BDUDtBQUNBO0FBQ0E7Q0FJQTs7Q0FFQTs7Q0FFQTs7Q0FFQTs7Q0FFQTs7Q0FFQTs7Q0FFQTs7Q0FFQTs7Q0FFQTs7Q0FFQTs7Q0FFQTs7QUFDQTtBQUVBOztBQUVBLElBQU1DLFdBQVcsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFqQjtBQVFBLElBQU1DLFVBQVUsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFoQjtBQU9BLElBQU1DLFFBQVEsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUFkO0FBVUEsSUFBTUMsU0FBUyxHQUFHO0FBQUg7QUFBQTtBQUFBLGFBQWY7QUFVQSxJQUFNQyxtQkFBbUIsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUF6QjtBQUtBLElBQU1sQyxrQ0FBeUIsR0FBRztBQUFIO0FBQUE7QUFBQSxhQUEvQjtBQVFPLElBQU1tQyxRQUFZLEdBQUcsTUFBTTtBQUM5QixNQUFNakMsT0FBTyxHQUFHQyxpQ0FBVyxDQUFDQyxvQ0FBRCxDQUEzQjtBQUNBLE1BQU07QUFBQ0ksb0JBQUQ7QUFBbUJELGlCQUFuQjtBQUFrQzZCLFNBQWxDO0FBQXlDQztBQUF6QyxNQUFnRG5DLE9BQXREO0FBRUEsTUFBTW9DLFlBQVksZ0JBQUczRCx1Q0FBSCw2Q0FHSDZCLGdCQUFnQixHQUFHLENBQUgsR0FBTyxDQUhwQiw2RUFBbEI7QUFPQSxNQUFNK0Isb0JBQW9CLGdCQUFHNUQsdUNBQUgsYUFDWDZCLGdCQUFnQixHQUFHLENBQUgsR0FBTyxDQURaLHVHQUExQjtBQU1BLFNBQ0k7QUFBSyxPQUFHLEVBQUVzQjtBQUFWLEtBQ0t2QixhQUFhLElBQ1Ysd0VBQ0ksd0NBQUMsMEJBQUQ7QUFBZ0IsT0FBRyxFQUFFK0I7QUFBckIsSUFESixPQUMwQyx3Q0FBQywwQkFBRDtBQUFnQixPQUFHLEVBQUVDO0FBQXJCLElBRDFDLENBRlIsRUFNS0gsS0FBSyxLQUFLLENBQVYsSUFDRyx3RUFDSSx3Q0FBQyx5QkFBRDtBQUFlLE9BQUcsRUFBRUU7QUFBcEIsSUFESixPQUN5Qyx3Q0FBQyxrQ0FBRDtBQUF1QixPQUFHLEVBQUVDO0FBQTVCLElBRHpDLENBUFIsRUFXS0gsS0FBSyxLQUFLLENBQVYsSUFDRyx3RUFDSSx3Q0FBQyx5QkFBRDtBQUFlLE9BQUcsRUFBRUU7QUFBcEIsSUFESixPQUN5Qyx3Q0FBQyxrQ0FBRDtBQUF1QixPQUFHLEVBQUVDO0FBQTVCLElBRHpDLENBWlIsRUFnQktILEtBQUssS0FBSyxDQUFWLElBQ0csd0VBQ0ksd0NBQUMseUJBQUQ7QUFBZSxPQUFHLEVBQUVFO0FBQXBCLElBREosT0FDeUMsd0NBQUMsa0NBQUQ7QUFBdUIsT0FBRyxFQUFFQztBQUE1QixJQUR6QyxDQWpCUixFQXFCS0gsS0FBSyxLQUFLLENBQVYsSUFDRyx3RUFDSSx3Q0FBQyx5QkFBRDtBQUFlLE9BQUcsRUFBRUU7QUFBcEIsSUFESixPQUN5Qyx3Q0FBQyxrQ0FBRDtBQUF1QixPQUFHLEVBQUVDO0FBQTVCLElBRHpDLENBdEJSLEVBMEJLSCxLQUFLLEtBQUssQ0FBVixJQUNHLHdFQUNJLHdDQUFDLHlCQUFEO0FBQWUsT0FBRyxFQUFFRTtBQUFwQixJQURKLE9BQ3lDLHdDQUFDLGtDQUFEO0FBQXVCLE9BQUcsRUFBRUM7QUFBNUIsSUFEekMsQ0EzQlIsRUErQkk7QUFBSyxPQUFHLEVBQUVSO0FBQVYsS0FDSyxDQUFDeEIsYUFBRCxJQUNHLHdFQUNJO0FBQUssT0FBRyxFQUFFeUI7QUFBVixLQUNLckMsMENBQVUsQ0FBQyxxQkFBRCxDQURmLFFBQ3lDLHNEQUFPeUMsS0FBUCxPQUR6QyxDQURKLEVBSUk7QUFBSyxPQUFHLEVBQUVIO0FBQVYsS0FBc0JJLEdBQXRCLENBSkosRUFLSTtBQUFLLE9BQUcsRUFBRUg7QUFBVixLQUNLRSxLQUFLLEtBQUssQ0FBVixJQUFlekMsMENBQVUsQ0FBQywrQkFBRCxDQUQ5QixFQUVLeUMsS0FBSyxLQUFLLENBQVYsSUFBZXpDLDBDQUFVLENBQUMsK0JBQUQsQ0FGOUIsRUFHS3lDLEtBQUssS0FBSyxDQUFWLElBQWV6QywwQ0FBVSxDQUFDLGdDQUFELENBSDlCLEVBSUt5QyxLQUFLLEtBQUssQ0FBVixJQUFlekMsMENBQVUsQ0FBQyw4QkFBRCxDQUo5QixFQUtLeUMsS0FBSyxLQUFLLENBQVYsSUFBZXpDLDBDQUFVLENBQUMsOEJBQUQsQ0FMOUIsQ0FMSixDQUZSLEVBZ0JLWSxhQUFhLElBQUk7QUFBSyxPQUFHLEVBQUVQLGtDQUF5QkE7QUFBbkMsS0FBc0NMLDBDQUFVLENBQUMsa0JBQUQsQ0FBaEQsQ0FoQnRCLENBL0JKLENBREo7QUFvREgsQ0FyRU0sQzs7QUMvRVA7QUFFQTtBQUNBO0FBQ0E7OztBQUVBLElBQU02QyxJQUFRLEdBQUcsTUFBTTtBQUNuQixTQUNJLHdFQUNJLHdDQUFDLFFBQUQsT0FESixFQUVJLHdDQUFDLG1CQUFELE9BRkosRUFHSSx3Q0FBQyxXQUFELE9BSEosQ0FESjtBQU9ILENBUkQ7O0FBVWVBLG9GQUFmLEUiLCJmaWxlIjoiNS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwge0ZDfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQge2Nzc30gZnJvbSAnQGVtb3Rpb24vY29yZSc7XG5cbnR5cGUgVG9nZ2xlUHJvcHMgPSB7XG4gICAgaWQ6IHN0cmluZztcbiAgICBoZWlnaHQ6IG51bWJlcjtcbiAgICB3aWR0aDogbnVtYmVyO1xuICAgIGhleENvbG9yOiBzdHJpbmc7XG4gICAgY2hlY2tlZD86IGJvb2xlYW47XG4gICAgYW5pbWF0ZT86IGJvb2xlYW47XG4gICAgb25DaGFuZ2U/OiAoKSA9PiB2b2lkO1xufTtcblxuY29uc3QgVG9nZ2xlOiBGQzxUb2dnbGVQcm9wcz4gPSAoe2lkLCBoZWlnaHQsIHdpZHRoLCBoZXhDb2xvciwgY2hlY2tlZCA9IGZhbHNlLCBhbmltYXRlID0gdHJ1ZSwgb25DaGFuZ2V9KSA9PiB7XG4gICAgY29uc3Qgc2xpZGVyRGlhbWV0ZXIgPSBoZWlnaHQgKiAwLjc1O1xuXG4gICAgY29uc3QgdG9nZ2xlQ3NzID0gY3NzYFxuICAgICAgICAuc3dpdGNoIHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgIGhlaWdodDogJHtoZWlnaHR9cHg7XG4gICAgICAgICAgICB3aWR0aDogJHt3aWR0aH1weDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5zd2l0Y2ggaW5wdXQge1xuICAgICAgICAgICAgb3BhY2l0eTogMDtcbiAgICAgICAgICAgIHdpZHRoOiAwO1xuICAgICAgICAgICAgaGVpZ2h0OiAwO1xuICAgICAgICB9XG5cbiAgICAgICAgLnNsaWRlciB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2VkMWRkO1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogJHtoZWlnaHR9cHg7XG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgICAgICBpbnNldDogMDtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcblxuICAgICAgICAgICAgJHthbmltYXRlICYmICd0cmFuc2l0aW9uOiAwLjRzOyd9XG4gICAgICAgIH1cblxuICAgICAgICBpbnB1dDpjaGVja2VkICsgLnNsaWRlciB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAke2hleENvbG9yfTtcbiAgICAgICAgfVxuXG4gICAgICAgIC5zbGlkZXI6YmVmb3JlIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlYmVjZjc7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAke3NsaWRlckRpYW1ldGVyfXB4O1xuICAgICAgICAgICAgYm94LXNoYWRvdzogMXB4IDJweCA0cHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgICAgICAgICAgY29udGVudDogJyc7XG4gICAgICAgICAgICBoZWlnaHQ6ICR7c2xpZGVyRGlhbWV0ZXJ9cHg7XG4gICAgICAgICAgICBtYXJnaW46IGNhbGMoJHtzbGlkZXJEaWFtZXRlcn1weCAvIDYpO1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgd2lkdGg6ICR7c2xpZGVyRGlhbWV0ZXJ9cHg7XG5cbiAgICAgICAgICAgICR7YW5pbWF0ZSAmJiAndHJhbnNpdGlvbjogMC40czsnfVxuICAgICAgICB9XG5cbiAgICAgICAgaW5wdXQ6Y2hlY2tlZCArIC5zbGlkZXI6YmVmb3JlIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoJHt3aWR0aCAtIGhlaWdodH1weCk7XG4gICAgICAgIH1cbiAgICBgO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPGRpdiBjc3M9e3RvZ2dsZUNzc30+XG4gICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwic3dpdGNoXCIgaHRtbEZvcj17aWR9PlxuICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICBpZD17aWR9XG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJjaGVja2JveFwiXG4gICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e2NoZWNrZWR9XG4gICAgICAgICAgICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBAdHlwZXNjcmlwdC1lc2xpbnQvbm8tdW51c2VkLXZhcnNcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhldmVudDogUmVhY3QuQ2hhbmdlRXZlbnQ8SFRNTElucHV0RWxlbWVudD4pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvbkNoYW5nZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJzbGlkZXJcIiAvPlxuICAgICAgICAgICAgPC9sYWJlbD5cbiAgICAgICAgPC9kaXY+XG4gICAgKTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFRvZ2dsZTtcbiIsInR5cGUgZ2V0RGlzcGxheUNvdW50UHJvcHMgPSB7XG4gICAgY291bnQ6IG51bWJlcjtcbiAgICBrU3RhcnQ/OiBudW1iZXI7XG4gICAgbVN0YXJ0PzogbnVtYmVyO1xufTtcblxuZXhwb3J0IGNvbnN0IGdldERpc3BsYXlDb3VudCA9ICh7Y291bnQsIGtTdGFydCA9IDEwMDAsIG1TdGFydCA9IDEwMDAwMDB9OiBnZXREaXNwbGF5Q291bnRQcm9wcykgPT4ge1xuICAgIC8vIEZvcm1hdCBtaWxsaW9uc1xuICAgIGlmIChjb3VudCA+PSBtU3RhcnQpIHtcbiAgICAgICAgcmV0dXJuIGAke01hdGguZmxvb3IoY291bnQgLyAxMDAwMDAwKX1NYDtcbiAgICB9XG5cbiAgICAvLyBGb3JtYXQgdGhvdXNhbmRzXG4gICAgaWYgKGNvdW50ID49IGtTdGFydCkge1xuICAgICAgICByZXR1cm4gYCR7TWF0aC5mbG9vcihjb3VudCAvIDEwMDApfWtgO1xuICAgIH1cblxuICAgIHJldHVybiBjb3VudDtcbn07XG4iLCJ2YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG5mdW5jdGlvbiBIaWRkZW5JY29uIChwcm9wcykge1xuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3ZnXCIscHJvcHMsW1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZFwiOlwiTTIxLjMzMDIgMi4yNjAyNEMyMC45ODQ5IDEuOTAzNjUgMjAuNDMyNCAxLjkwMzY1IDIwLjA4NyAyLjI2MDI0TDE2Ljk1NjEgNS40OTMyN0MxMy4wNjU1IDMuNjg2NTcgOC45MjE1NyA0LjEyNjM2IDQuOTczMzcgNi43ODg4NkMyLjAyNjYgOC43NzM4NSAwLjI0MjQzMSAxMS4yNjk5IDAuMTczMzY2IDExLjM3NjlDLTAuMDU2ODQ5OCAxMS43MDk3IC0wLjA1Njg0OTggMTIuMTYxNCAwLjE5NjM4OCAxMi40OTQyQzEuODY1NDUgMTQuNjA5OSAzLjYyNjYgMTYuMjYyMSA1LjQxMDc4IDE3LjQxNTFMMi42NDgxOSAyMC4yNjc4QzIuMzAyODYgMjAuNjI0MyAyLjMwMjg2IDIxLjE5NDkgMi42NDgxOSAyMS41NjMzQzIuODIwODUgMjEuNzQxNiAzLjAzOTU1IDIxLjgzNjcgMy4yNjk3NyAyMS44MzY3QzMuNDk5OTggMjEuODM2NyAzLjcxODY5IDIxLjc0MTYgMy44OTEzNSAyMS41NjMzTDIxLjMzMDIgMy41NTU4M0MyMS42NzU1IDMuMTk5MjQgMjEuNjc1NSAyLjYyODcxIDIxLjMzMDIgMi4yNjAyNFpNOS44MTk0MSAxMi44NzQ2QzkuNjkyNzkgMTIuNTc3NCA5LjYyMzczIDEyLjI1NjUgOS42MjM3MyAxMS45MTE4QzkuNjIzNzMgMTEuMjU4MSA5Ljg2NTQ1IDEwLjY0IDEwLjMxNDQgMTAuMTc2NEMxMS4wMTY1IDkuNDUxMzYgMTIuMDY0IDkuMjczMDcgMTIuOTI3MyA5LjY1MzQyTDkuODE5NDEgMTIuODc0NlpNMTQuMjM5NiA4LjMxMDI5QzEyLjYzOTYgNy4yNTI0MiAxMC40NzU1IDcuNDQyNiA5LjA4MjcyIDguODgwODJDOC4yOTk5OCA5LjY4OTA4IDcuODc0MDkgMTAuNzU4OCA3Ljg3NDA5IDExLjg5OTlDNy44NzQwOSAxMi43MzE5IDguMTA0MyAxMy41MjgzIDguNTMwMiAxNC4yMDU4TDYuNzExNSAxNi4wODM4QzUuMTIzMDEgMTUuMTMyOSAzLjU1NzU0IDEzLjczMDQgMi4wMjY2IDExLjg4OEMyLjY4MjcyIDExLjEwMzUgNC4wNTI1IDkuNTgyMTEgNS45NDAyNyA4LjMxMDI5QzkuMTk3ODMgNi4xMTEzNSAxMi40NDM5IDUuNjM1OSAxNS42MDkzIDYuODgzOTVMMTQuMjM5NiA4LjMxMDI5WlwiLFwiZmlsbFwiOlwiI0NFRDFERFwiLFwia2V5XCI6MH0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZFwiOlwiTTIzLjgwNDMgMTEuMzMwN0MyMi41OTU3IDkuNzk3MzQgMjEuMzQxIDguNTAxNzUgMjAuMDYzMyA3LjQ2NzY2QzE5LjY3MTkgNy4xNTg2MiAxOS4xMTk0IDcuMjI5OTMgMTguODIwMSA3LjYyMjE3QzE4LjUyMDkgOC4wMTQ0MiAxOC41ODk5IDguNTg0OTUgMTguOTY5OCA4LjkwNTg4QzE5Ljk4MjcgOS43MTQxNCAyMC45ODQyIDEwLjczNjMgMjEuOTYyNiAxMS45MTMxQzIxLjM4NzEgMTIuNjE0NCAyMC4yNTkgMTMuODYyNCAxOC43MjgxIDE1LjAxNTRDMTUuNzgxMyAxNy4yMjYyIDEyLjgxMTUgMTguMDIyNiA5Ljg5OTI4IDE3LjM5MjZDOS40MjczNCAxNy4yODU2IDguOTU1NCAxNy42MDY1IDguODUxOCAxOC4wOTM5QzguNzQ4MiAxOC41ODEyIDkuMDU4OTkgMTkuMDY4NSA5LjUzMDkzIDE5LjE3NTVDMTAuMjkwNiAxOS4zNDE5IDExLjA1MDQgMTkuNDI1MSAxMS44MjE2IDE5LjQyNTFDMTIuOTcyNyAxOS40MjUxIDE0LjE0NjggMTkuMjM0OSAxNS4yOTc4IDE4Ljg2NjVDMTYuODE3MyAxOC4zNzkxIDE4LjMzNjcgMTcuNTcwOSAxOS43OTg2IDE2LjQ2NTVDMjIuMjYxOSAxNC41OTkzIDIzLjczNTMgMTIuNTQzIDIzLjgwNDMgMTIuNDU5OEMyNC4wNTc2IDEyLjExNTEgMjQuMDU3NiAxMS42NjM1IDIzLjgwNDMgMTEuMzMwN1pcIixcImZpbGxcIjpcIiNDRUQxRERcIixcImtleVwiOjF9KSxSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGVmc1wiLHtcImtleVwiOjJ9LFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJjbGlwUGF0aFwiLHtcImlkXCI6XCJjbGlwMFwifSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicmVjdFwiLHtcIndpZHRoXCI6XCIyNFwiLFwiaGVpZ2h0XCI6XCIxOS44MjYxXCIsXCJmaWxsXCI6XCJ3aGl0ZVwiLFwidHJhbnNmb3JtXCI6XCJ0cmFuc2xhdGUoMCAyKVwifSkpKV0pO1xufVxuXG5IaWRkZW5JY29uLmRlZmF1bHRQcm9wcyA9IHtcIndpZHRoXCI6XCIyNFwiLFwiaGVpZ2h0XCI6XCIyNFwiLFwidmlld0JveFwiOlwiMCAwIDI0IDI0XCIsXCJmaWxsXCI6XCJub25lXCJ9O1xuXG5tb2R1bGUuZXhwb3J0cyA9IEhpZGRlbkljb247XG5cbkhpZGRlbkljb24uZGVmYXVsdCA9IEhpZGRlbkljb247XG4iLCJ2YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG5mdW5jdGlvbiBQcm90ZWN0aW9uT24gKHByb3BzKSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzdmdcIixwcm9wcyxbUmVhY3QuY3JlYXRlRWxlbWVudChcImNpcmNsZVwiLHtcImN4XCI6XCI4XCIsXCJjeVwiOlwiOFwiLFwiclwiOlwiOFwiLFwiZmlsbFwiOlwiIzI5RERDQ1wiLFwia2V5XCI6MH0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJnXCIse1wiY2xpcFBhdGhcIjpcInVybCgjY2xpcDApXCIsXCJrZXlcIjoxfSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLHtcImRcIjpcIk0xMS41IDYuMDAzOEwxMC4wMjQyIDVMNy4yNTgwNiA4LjYxMjE3TDUuNTY0NTIgNy40NTYyN0w0LjUgOC44NDc5MUw3LjY2MTI5IDExVjEwLjk5MjRMNy42NjkzNSAxMUwxMS41IDYuMDAzOFpcIixcImZpbGxcIjpcIiMyMDI5NDVcIn0pKSxSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGVmc1wiLHtcImtleVwiOjJ9LFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJjbGlwUGF0aFwiLHtcImlkXCI6XCJjbGlwMFwifSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicmVjdFwiLHtcIndpZHRoXCI6XCI3XCIsXCJoZWlnaHRcIjpcIjZcIixcImZpbGxcIjpcIndoaXRlXCIsXCJ0cmFuc2Zvcm1cIjpcInRyYW5zbGF0ZSg0LjUgNSlcIn0pKSldKTtcbn1cblxuUHJvdGVjdGlvbk9uLmRlZmF1bHRQcm9wcyA9IHtcIndpZHRoXCI6XCIxNlwiLFwiaGVpZ2h0XCI6XCIxNlwiLFwidmlld0JveFwiOlwiMCAwIDE2IDE2XCIsXCJmaWxsXCI6XCJub25lXCJ9O1xuXG5tb2R1bGUuZXhwb3J0cyA9IFByb3RlY3Rpb25PbjtcblxuUHJvdGVjdGlvbk9uLmRlZmF1bHQgPSBQcm90ZWN0aW9uT247XG4iLCJ2YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG5mdW5jdGlvbiBQcm90ZWN0aW9uT2ZmIChwcm9wcykge1xuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3ZnXCIscHJvcHMsW1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJjaXJjbGVcIix7XCJjeFwiOlwiOFwiLFwiY3lcIjpcIjhcIixcInJcIjpcIjhcIixcImZpbGxcIjpcIiNFQjU3NTdcIixcImtleVwiOjB9KSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLHtcImRcIjpcIk03IDQuNUg5TDguNzM5NDYgOC41SDcuMjc1ODZMNyA0LjVaXCIsXCJmaWxsXCI6XCJ3aGl0ZVwiLFwia2V5XCI6MX0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJjaXJjbGVcIix7XCJjeFwiOlwiOFwiLFwiY3lcIjpcIjEwLjVcIixcInJcIjpcIjFcIixcImZpbGxcIjpcIndoaXRlXCIsXCJrZXlcIjoyfSldKTtcbn1cblxuUHJvdGVjdGlvbk9mZi5kZWZhdWx0UHJvcHMgPSB7XCJ3aWR0aFwiOlwiMTZcIixcImhlaWdodFwiOlwiMTZcIixcInZpZXdCb3hcIjpcIjAgMCAxNiAxNlwiLFwiZmlsbFwiOlwibm9uZVwifTtcblxubW9kdWxlLmV4cG9ydHMgPSBQcm90ZWN0aW9uT2ZmO1xuXG5Qcm90ZWN0aW9uT2ZmLmRlZmF1bHQgPSBQcm90ZWN0aW9uT2ZmO1xuIiwidmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxuZnVuY3Rpb24gUHJvdGVjdGlvbk9mZkdyYXkgKHByb3BzKSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzdmdcIixwcm9wcyxbUmVhY3QuY3JlYXRlRWxlbWVudChcImNpcmNsZVwiLHtcImN4XCI6XCI4XCIsXCJjeVwiOlwiOFwiLFwiclwiOlwiOFwiLFwiZmlsbFwiOlwiIzdGODY5RlwiLFwia2V5XCI6MH0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZFwiOlwiTTcgNC41SDlMOC43Mzk0NiA4LjVINy4yNzU4Nkw3IDQuNVpcIixcImZpbGxcIjpcIndoaXRlXCIsXCJrZXlcIjoxfSksUmVhY3QuY3JlYXRlRWxlbWVudChcImNpcmNsZVwiLHtcImN4XCI6XCI4XCIsXCJjeVwiOlwiMTAuNVwiLFwiclwiOlwiMVwiLFwiZmlsbFwiOlwid2hpdGVcIixcImtleVwiOjJ9KV0pO1xufVxuXG5Qcm90ZWN0aW9uT2ZmR3JheS5kZWZhdWx0UHJvcHMgPSB7XCJ3aWR0aFwiOlwiMTZcIixcImhlaWdodFwiOlwiMTZcIixcInZpZXdCb3hcIjpcIjAgMCAxNiAxNlwiLFwiZmlsbFwiOlwibm9uZVwifTtcblxubW9kdWxlLmV4cG9ydHMgPSBQcm90ZWN0aW9uT2ZmR3JheTtcblxuUHJvdGVjdGlvbk9mZkdyYXkuZGVmYXVsdCA9IFByb3RlY3Rpb25PZmZHcmF5O1xuIiwidmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxuZnVuY3Rpb24gUHJpdmFjeVNjb3JlMSAocHJvcHMpIHtcbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcInN2Z1wiLHByb3BzLFtSZWFjdC5jcmVhdGVFbGVtZW50KFwiY2lyY2xlXCIse1wiY3hcIjpcIjM2XCIsXCJjeVwiOlwiMzZcIixcInJcIjpcIjI4XCIsXCJmaWxsXCI6XCIjNjU3M0ZGXCIsXCJrZXlcIjowfSksUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIix7XCJkXCI6XCJNMzguMTgzMiAyNC41VjQ2LjMxODJIMzQuMjMwOFYyOC4zNDU5SDM0LjEwM0wyOSAzMS42MDU4VjI3Ljk4MzdMMzQuNDIyNiAyNC41SDM4LjE4MzJaXCIsXCJmaWxsXCI6XCJ3aGl0ZVwiLFwia2V5XCI6MX0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZmlsbFJ1bGVcIjpcImV2ZW5vZGRcIixcImNsaXBSdWxlXCI6XCJldmVub2RkXCIsXCJkXCI6XCJNMzYgNzJDNTUuODgyMyA3MiA3MiA1NS44ODIzIDcyIDM2QzcyIDE2LjExNzcgNTUuODgyMyAwIDM2IDBDMTYuMTE3NyAwIDAgMTYuMTE3NyAwIDM2QzAgNTUuODgyMyAxNi4xMTc3IDcyIDM2IDcyWk0zNiA2OEM1My42NzMxIDY4IDY4IDUzLjY3MzEgNjggMzZDNjggMTguMzI2OSA1My42NzMxIDQgMzYgNEMxOC4zMjY5IDQgNCAxOC4zMjY5IDQgMzZDNCA1My42NzMxIDE4LjMyNjkgNjggMzYgNjhaXCIsXCJmaWxsXCI6XCIjRUJFQ0Y3XCIsXCJrZXlcIjoyfSksUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIix7XCJmaWxsUnVsZVwiOlwiZXZlbm9kZFwiLFwiY2xpcFJ1bGVcIjpcImV2ZW5vZGRcIixcImRcIjpcIk0zNS45Mjg2IDcxLjk5OTlMMzYgNzEuNVY2OEMyMS43NjkgNjggOS43MDc2NSA1OC43MTA0IDUuNTQ5MjYgNDUuODY0M0wxLjc0MjkyIDQ3LjA5NzRDNi40MTQxNSA2MS41Mjc3IDE5Ljk0OTggNzEuOTY4OSAzNS45Mjg2IDcxLjk5OTlaXCIsXCJmaWxsXCI6XCIjRUI1NzU3XCIsXCJrZXlcIjozfSldKTtcbn1cblxuUHJpdmFjeVNjb3JlMS5kZWZhdWx0UHJvcHMgPSB7XCJ3aWR0aFwiOlwiNzJcIixcImhlaWdodFwiOlwiNzJcIixcInZpZXdCb3hcIjpcIjAgMCA3MiA3MlwiLFwiZmlsbFwiOlwibm9uZVwifTtcblxubW9kdWxlLmV4cG9ydHMgPSBQcml2YWN5U2NvcmUxO1xuXG5Qcml2YWN5U2NvcmUxLmRlZmF1bHQgPSBQcml2YWN5U2NvcmUxO1xuIiwidmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxuZnVuY3Rpb24gUHJpdmFjeVNjb3JlMiAocHJvcHMpIHtcbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcInN2Z1wiLHByb3BzLFtSZWFjdC5jcmVhdGVFbGVtZW50KFwiY2lyY2xlXCIse1wiY3hcIjpcIjM2XCIsXCJjeVwiOlwiMzZcIixcInJcIjpcIjI4XCIsXCJmaWxsXCI6XCIjNjU3M0ZGXCIsXCJrZXlcIjowfSksUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIix7XCJkXCI6XCJNMjguMTI3OCA0Ni42MTY1VjQzLjc2MTRMMzUuNzAyNCAzNi4zMzU5QzM2LjQyNjggMzUuNjA0NCAzNy4wMzA1IDM0Ljk1NDUgMzcuNTEzNSAzNC4zODY0QzM3Ljk5NjQgMzMuODE4MiAzOC4zNTg3IDMzLjI2NzggMzguNjAwMSAzMi43MzUxQzM4Ljg0MTYgMzIuMjAyNCAzOC45NjI0IDMxLjYzNDIgMzguOTYyNCAzMS4wMzA1QzM4Ljk2MjQgMzAuMzQxNiAzOC44MDYxIDI5Ljc1MjEgMzguNDkzNiAyOS4yNjIxQzM4LjE4MTEgMjguNzY0OSAzNy43NTE0IDI4LjM4MTQgMzcuMjA0NSAyOC4xMTE1QzM2LjY1NzcgMjcuODQxNiAzNi4wMzYyIDI3LjcwNjcgMzUuMzQwMiAyNy43MDY3QzM0LjYyMjkgMjcuNzA2NyAzMy45OTQzIDI3Ljg1NTggMzMuNDU0NSAyOC4xNTQxQzMyLjkxNDggMjguNDQ1MyAzMi40OTU3IDI4Ljg2MDggMzIuMTk3NCAyOS40MDA2QzMxLjkwNjMgMjkuOTQwMyAzMS43NjA3IDMwLjU4MzEgMzEuNzYwNyAzMS4zMjg4SDI4QzI4IDI5Ljk0MzkgMjguMzE2MSAyOC43NDAxIDI4Ljk0ODIgMjcuNzE3M0MyOS41ODAzIDI2LjY5NDYgMzAuNDUwMyAyNS45MDI3IDMxLjU1ODIgMjUuMzQxNkMzMi42NzMzIDI0Ljc4MDUgMzMuOTUxNyAyNC41IDM1LjM5MzUgMjQuNUMzNi44NTY1IDI0LjUgMzguMTQyIDI0Ljc3MzQgMzkuMjUgMjUuMzIwM0M0MC4zNTggMjUuODY3MiA0MS4yMTczIDI2LjYxNjUgNDEuODI4MSAyNy41NjgyQzQyLjQ0NiAyOC41MTk5IDQyLjc1NSAyOS42MDY1IDQyLjc1NSAzMC44MjgxQzQyLjc1NSAzMS42NDQ5IDQyLjU5ODcgMzIuNDQ3NCA0Mi4yODYyIDMzLjIzNThDNDEuOTczNyAzNC4wMjQxIDQxLjQyMzMgMzQuODk3NyA0MC42MzQ5IDM1Ljg1NjVDMzkuODUzNyAzNi44MTUzIDM4Ljc1NjQgMzcuOTc2NiAzNy4zNDMgMzkuMzQwMkwzMy41ODI0IDQzLjE2NDhWNDMuMzEzOUg0My4wODUyVjQ2LjYxNjVIMjguMTI3OFpcIixcImZpbGxcIjpcIndoaXRlXCIsXCJrZXlcIjoxfSksUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIix7XCJmaWxsUnVsZVwiOlwiZXZlbm9kZFwiLFwiY2xpcFJ1bGVcIjpcImV2ZW5vZGRcIixcImRcIjpcIk0zNiA3MkM1NS44ODIzIDcyIDcyIDU1Ljg4MjMgNzIgMzZDNzIgMTYuMTE3NyA1NS44ODIzIDAgMzYgMEMxNi4xMTc3IDAgMCAxNi4xMTc3IDAgMzZDMCA1NS44ODIzIDE2LjExNzcgNzIgMzYgNzJaTTM2IDY4QzUzLjY3MzEgNjggNjggNTMuNjczMSA2OCAzNkM2OCAxOC4zMjY5IDUzLjY3MzEgNCAzNiA0QzE4LjMyNjkgNCA0IDE4LjMyNjkgNCAzNkM0IDUzLjY3MzEgMTguMzI2OSA2OCAzNiA2OFpcIixcImZpbGxcIjpcIiNFQkVDRjdcIixcImtleVwiOjJ9KSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLHtcImZpbGxSdWxlXCI6XCJldmVub2RkXCIsXCJjbGlwUnVsZVwiOlwiZXZlbm9kZFwiLFwiZFwiOlwiTTM1LjkyNzMgNzEuOTk5OUwzNiA3MS43NjM2VjY4QzE4LjMyNjkgNjggNCA1My42NzMxIDQgMzZDNCAyNS4zMzc0IDkuMjE0OTYgMTUuODkyOCAxNy4yMzM0IDEwLjA3NzdMMTQuOTAzMiA2LjgyNjE3QzUuODczNTcgMTMuMzY3MiAwIDIzLjk5NzcgMCAzNkMwIDU1Ljg1OCAxNi4wNzg1IDcxLjk2MDcgMzUuOTI3MyA3MS45OTk5WlwiLFwiZmlsbFwiOlwiI0YxOEIxRlwiLFwia2V5XCI6M30pXSk7XG59XG5cblByaXZhY3lTY29yZTIuZGVmYXVsdFByb3BzID0ge1wid2lkdGhcIjpcIjcyXCIsXCJoZWlnaHRcIjpcIjcyXCIsXCJ2aWV3Qm94XCI6XCIwIDAgNzIgNzJcIixcImZpbGxcIjpcIm5vbmVcIn07XG5cbm1vZHVsZS5leHBvcnRzID0gUHJpdmFjeVNjb3JlMjtcblxuUHJpdmFjeVNjb3JlMi5kZWZhdWx0ID0gUHJpdmFjeVNjb3JlMjtcbiIsInZhciBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbmZ1bmN0aW9uIFByaXZhY3lTY29yZTMgKHByb3BzKSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzdmdcIixwcm9wcyxbUmVhY3QuY3JlYXRlRWxlbWVudChcImNpcmNsZVwiLHtcImN4XCI6XCIzNlwiLFwiY3lcIjpcIjM2XCIsXCJyXCI6XCIyOFwiLFwiZmlsbFwiOlwiIzY1NzNGRlwiLFwia2V5XCI6MH0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZmlsbFJ1bGVcIjpcImV2ZW5vZGRcIixcImNsaXBSdWxlXCI6XCJldmVub2RkXCIsXCJkXCI6XCJNMzYgNzJDNTUuODgyMyA3MiA3MiA1NS44ODIzIDcyIDM2QzcyIDE2LjExNzcgNTUuODgyMyAwIDM2IDBDMTYuMTE3NyAwIDAgMTYuMTE3NyAwIDM2QzAgNTUuODgyMyAxNi4xMTc3IDcyIDM2IDcyWk0zNiA2OEM1My42NzMxIDY4IDY4IDUzLjY3MzEgNjggMzZDNjggMTguMzI2OSA1My42NzMxIDQgMzYgNEMxOC4zMjY5IDQgNCAxOC4zMjY5IDQgMzZDNCA1My42NzMxIDE4LjMyNjkgNjggMzYgNjhaXCIsXCJmaWxsXCI6XCIjRUJFQ0Y3XCIsXCJrZXlcIjoxfSksUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIix7XCJmaWxsUnVsZVwiOlwiZXZlbm9kZFwiLFwiY2xpcFJ1bGVcIjpcImV2ZW5vZGRcIixcImRcIjpcIk01NC45MjUgMTAuMTkzNEM0OS42MjQ3IDYuMjk5ODcgNDMuMDgwOSA0IDM2IDRDMTguMzI2OSA0IDQgMTguMzI2OSA0IDM2QzQgNTMuNjczMSAxOC4zMjY5IDY4IDM2IDY4VjcxLjUwMDJMMzUuODg5IDcxLjk5OThDMTYuMDU3OCA3MS45NCAwIDU1Ljg0NTIgMCAzNkMwIDE2LjExNzcgMTYuMTE3NyAwIDM2IDBDNDMuOTY2IDAgNTEuMzI3NyAyLjU4NzM2IDU3LjI5MDYgNi45Njc1NEw1NC45MjUgMTAuMTkzNFpcIixcImZpbGxcIjpcIiNGRkNGMjVcIixcImtleVwiOjJ9KSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLHtcImRcIjpcIk0zNi4wMDA3IDQ2LjkxNDhDMzQuNDY2NiA0Ni45MTQ4IDMzLjEwMyA0Ni42NTIgMzEuOTA5OCA0Ni4xMjY0QzMwLjcyMzcgNDUuNjAwOSAyOS43ODYyIDQ0Ljg2OTMgMjkuMDk3MyA0My45MzE4QzI4LjQwODQgNDIuOTk0MyAyOC4wNDI2IDQxLjkxMTIgMjggNDAuNjgyNUgzMi4wMDU3QzMyLjA0MTIgNDEuMjcyIDMyLjIzNjUgNDEuNzg2OSAzMi41OTE2IDQyLjIyNzNDMzIuOTQ2NyA0Mi42NjA1IDMzLjQxOSA0Mi45OTc5IDM0LjAwODUgNDMuMjM5M0MzNC41OTggNDMuNDgwOCAzNS4yNTg1IDQzLjYwMTYgMzUuOTkwMSA0My42MDE2QzM2Ljc3MTMgNDMuNjAxNiAzNy40NjM4IDQzLjQ2NjYgMzguMDY3NSA0My4xOTY3QzM4LjY3MTIgNDIuOTE5NyAzOS4xNDM1IDQyLjUzNjIgMzkuNDg0NCA0Mi4wNDYyQzM5LjgyNTMgNDEuNTU2MSAzOS45OTIyIDQwLjk5MTUgMzkuOTg1MSA0MC4zNTIzQzM5Ljk5MjIgMzkuNjkxOCAzOS44MjE3IDM5LjEwOTQgMzkuNDczNyAzOC42MDUxQzM5LjEyNTcgMzguMTAwOSAzOC42MjE0IDM3LjcwNjcgMzcuOTYwOSAzNy40MjI2QzM3LjMwNzUgMzcuMTM4NSAzNi41MTkyIDM2Ljk5NjQgMzUuNTk1OSAzNi45OTY0SDMzLjY2NzZWMzMuOTQ5NkgzNS41OTU5QzM2LjM1NTggMzMuOTQ5NiAzNy4wMTk5IDMzLjgxODIgMzcuNTg4MSAzMy41NTU0QzM4LjE2MzQgMzMuMjkyNiAzOC42MTQzIDMyLjkyMzMgMzguOTQxMSAzMi40NDc0QzM5LjI2NzggMzEuOTY0NSAzOS40Mjc2IDMxLjQwNyAzOS40MjA1IDMwLjc3NDlDMzkuNDI3NiAzMC4xNTcgMzkuMjg5MSAyOS42MjA3IDM5LjAwNSAyOS4xNjYyQzM4LjcyOCAyOC43MDQ1IDM4LjMzMzggMjguMzQ1OSAzNy44MjI0IDI4LjA5MDJDMzcuMzE4MiAyNy44MzQ1IDM2LjcyNTEgMjcuNzA2NyAzNi4wNDMzIDI3LjcwNjdDMzUuMzc1NyAyNy43MDY3IDM0Ljc1NzggMjcuODI3NCAzNC4xODk2IDI4LjA2ODlDMzMuNjIxNCAyOC4zMTA0IDMzLjE2MzQgMjguNjU0OCAzMi44MTUzIDI5LjEwMjNDMzIuNDY3MyAyOS41NDI2IDMyLjI4MjcgMzAuMDY4MiAzMi4yNjE0IDMwLjY3OUgyOC40NTgxQzI4LjQ4NjUgMjkuNDU3NCAyOC44MzgxIDI4LjM4NDkgMjkuNTEyOCAyNy40NjE2QzMwLjE5NDYgMjYuNTMxMyAzMS4xMDM3IDI1LjgwNjggMzIuMjQwMSAyNS4yODg0QzMzLjM3NjQgMjQuNzYyOCAzNC42NTEzIDI0LjUgMzYuMDY0NiAyNC41QzM3LjUyMDYgMjQuNSAzOC43ODQ4IDI0Ljc3MzQgMzkuODU3MiAyNS4zMjAzQzQwLjkzNjggMjUuODYwMSA0MS43NzEzIDI2LjU4ODEgNDIuMzYwOCAyNy41MDQzQzQyLjk1MDMgMjguNDIwNSA0My4yNDUgMjkuNDMyNSA0My4yNDUgMzAuNTQwNUM0My4yNTIxIDMxLjc2OTIgNDIuODg5OSAzMi43OTkgNDIuMTU4NCAzMy42M0M0MS40MzM5IDM0LjQ2MDkgNDAuNDgyMiAzNS4wMDQzIDM5LjMwMzMgMzUuMjU5OVYzNS40MzA0QzQwLjgzNzQgMzUuNjQzNSA0Mi4wMTI4IDM2LjIxMTYgNDIuODI5NSAzNy4xMzQ5QzQzLjY1MzQgMzguMDUxMSA0NC4wNjE4IDM5LjE5MTEgNDQuMDU0NyA0MC41NTQ3QzQ0LjA1NDcgNDEuNzc2MyA0My43MDY3IDQyLjg3IDQzLjAxMDcgNDMuODM1OUM0Mi4zMjE3IDQ0Ljc5NDcgNDEuMzcgNDUuNTQ3NiA0MC4xNTU1IDQ2LjA5NDVDMzguOTQ4MiA0Ni42NDEzIDM3LjU2MzIgNDYuOTE0OCAzNi4wMDA3IDQ2LjkxNDhaXCIsXCJmaWxsXCI6XCJ3aGl0ZVwiLFwia2V5XCI6M30pXSk7XG59XG5cblByaXZhY3lTY29yZTMuZGVmYXVsdFByb3BzID0ge1wid2lkdGhcIjpcIjczXCIsXCJoZWlnaHRcIjpcIjcyXCIsXCJ2aWV3Qm94XCI6XCIwIDAgNzMgNzJcIixcImZpbGxcIjpcIm5vbmVcIn07XG5cbm1vZHVsZS5leHBvcnRzID0gUHJpdmFjeVNjb3JlMztcblxuUHJpdmFjeVNjb3JlMy5kZWZhdWx0ID0gUHJpdmFjeVNjb3JlMztcbiIsInZhciBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbmZ1bmN0aW9uIFByaXZhY3lTY29yZTQgKHByb3BzKSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzdmdcIixwcm9wcyxbUmVhY3QuY3JlYXRlRWxlbWVudChcImNpcmNsZVwiLHtcImN4XCI6XCIzNi43OTExXCIsXCJjeVwiOlwiMzZcIixcInJcIjpcIjI4XCIsXCJmaWxsXCI6XCIjNjU3M0ZGXCIsXCJrZXlcIjowfSksUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIix7XCJmaWxsUnVsZVwiOlwiZXZlbm9kZFwiLFwiY2xpcFJ1bGVcIjpcImV2ZW5vZGRcIixcImRcIjpcIk0zNi43OTExIDcyQzU2LjY3MzQgNzIgNzIuNzkxMSA1NS44ODIzIDcyLjc5MTEgMzZDNzIuNzkxMSAxNi4xMTc3IDU2LjY3MzQgMCAzNi43OTExIDBDMTYuOTA4OSAwIDAuNzkxMTM4IDE2LjExNzcgMC43OTExMzggMzZDMC43OTExMzggNTUuODgyMyAxNi45MDg5IDcyIDM2Ljc5MTEgNzJaTTM2Ljc5MTEgNjhDNTQuNDY0MiA2OCA2OC43OTExIDUzLjY3MzEgNjguNzkxMSAzNkM2OC43OTExIDE4LjMyNjkgNTQuNDY0MiA0IDM2Ljc5MTEgNEMxOS4xMTggNCA0Ljc5MTE0IDE4LjMyNjkgNC43OTExNCAzNkM0Ljc5MTE0IDUzLjY3MzEgMTkuMTE4IDY4IDM2Ljc5MTEgNjhaXCIsXCJmaWxsXCI6XCIjRUJFQ0Y3XCIsXCJrZXlcIjoxfSksUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIix7XCJmaWxsUnVsZVwiOlwiZXZlbm9kZFwiLFwiY2xpcFJ1bGVcIjpcImV2ZW5vZGRcIixcImRcIjpcIk02Ny4xMTg1IDQ2LjIzNzFDNjguMjAzMSA0My4wMjI5IDY4Ljc5MSAzOS41ODAxIDY4Ljc5MSAzNkM2OC43OTEgMTguMzI2OSA1NC40NjQxIDQgMzYuNzkxIDRDMTkuMTE3OSA0IDQuNzkxMDIgMTguMzI2OSA0Ljc5MTAyIDM2QzQuNzkxMDIgNTMuNjAzNCAxOS4wMDUxIDY3Ljg4NjkgMzYuNTgyIDY3Ljk5OTNWNzEuMDAwOUwzNi4zMzMgNzEuOTk3MUMxNi42NjE4IDcxLjc1MTkgMC43OTEwMTYgNTUuNzI5MyAwLjc5MTAxNiAzNkMwLjc5MTAxNiAxNi4xMTc3IDE2LjkwODggMCAzNi43OTEgMEM1Ni42NzMzIDAgNzIuNzkxIDE2LjExNzcgNzIuNzkxIDM2QzcyLjc5MSA0MC4wMDM5IDcyLjEzNzQgNDMuODU1MSA3MC45MzEgNDcuNDUyN0w2Ny4xMTg1IDQ2LjIzNzFaXCIsXCJmaWxsXCI6XCIjQTRENDExXCIsXCJrZXlcIjoyfSksUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIix7XCJkXCI6XCJNMjYuNzkxIDQyLjI2OTlWMzkuMTI3MUwzNi4wNDg4IDI0LjVIMzguNjY5NlYyOC45NzQ0SDM3LjA3MTZMMzAuODM5MyAzOC44NTAxVjM5LjAyMDZINDMuNzYxOVY0Mi4yNjk5SDI2Ljc5MVpNMzcuMTk5NCA0Ni4zMTgyVjQxLjMxMTFMMzcuMjQyIDM5LjkwNDhWMjQuNUg0MC45NzA3VjQ2LjMxODJIMzcuMTk5NFpcIixcImZpbGxcIjpcIndoaXRlXCIsXCJrZXlcIjozfSldKTtcbn1cblxuUHJpdmFjeVNjb3JlNC5kZWZhdWx0UHJvcHMgPSB7XCJ3aWR0aFwiOlwiNzNcIixcImhlaWdodFwiOlwiNzJcIixcInZpZXdCb3hcIjpcIjAgMCA3MyA3MlwiLFwiZmlsbFwiOlwibm9uZVwifTtcblxubW9kdWxlLmV4cG9ydHMgPSBQcml2YWN5U2NvcmU0O1xuXG5Qcml2YWN5U2NvcmU0LmRlZmF1bHQgPSBQcml2YWN5U2NvcmU0O1xuIiwidmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxuZnVuY3Rpb24gUHJpdmFjeVNjb3JlNSAocHJvcHMpIHtcbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcInN2Z1wiLHByb3BzLFtSZWFjdC5jcmVhdGVFbGVtZW50KFwiY2lyY2xlXCIse1wiY3hcIjpcIjM2XCIsXCJjeVwiOlwiMzZcIixcInJcIjpcIjM2XCIsXCJmaWxsXCI6XCIjMjlERENDXCIsXCJrZXlcIjowfSksUmVhY3QuY3JlYXRlRWxlbWVudChcImNpcmNsZVwiLHtcImN4XCI6XCIzNlwiLFwiY3lcIjpcIjM2XCIsXCJyXCI6XCIzMFwiLFwiZmlsbFwiOlwiIzY1NzNGRlwiLFwic3Ryb2tlXCI6XCJ3aGl0ZVwiLFwic3Ryb2tlV2lkdGhcIjpcIjRcIixcImtleVwiOjF9KSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLHtcImRcIjpcIk0zNi4wMzIgNDcuMjk4M0MzNC42MTE1IDQ3LjI5ODMgMzMuMzQwMiA0Ny4wMzIgMzIuMjE4IDQ2LjQ5OTNDMzEuMDk1OSA0NS45NTk1IDMwLjIwNDUgNDUuMjIwOSAyOS41NDQgNDQuMjgzNEMyOC44OTA2IDQzLjM0NTkgMjguNTQyNiA0Mi4yNzM0IDI4LjUgNDEuMDY2MUgzMi4zMzUyQzMyLjQwNjIgNDEuOTYwOSAzMi43OTMzIDQyLjY5MjUgMzMuNDk2NCA0My4yNjA3QzM0LjE5OTYgNDMuODIxNyAzNS4wNDQ3IDQ0LjEwMjMgMzYuMDMyIDQ0LjEwMjNDMzYuODA2MSA0NC4xMDIzIDM3LjQ5NSA0My45MjQ3IDM4LjA5ODcgNDMuNTY5NkMzOC43MDI0IDQzLjIxNDUgMzkuMTc4MyA0Mi43MjA5IDM5LjUyNjMgNDIuMDg4OEMzOS44NzQzIDQxLjQ1NjcgNDAuMDQ0NyA0MC43MzU4IDQwLjAzNzYgMzkuOTI2MUM0MC4wNDQ3IDM5LjEwMjMgMzkuODcwNyAzOC4zNzA3IDM5LjUxNTYgMzcuNzMxNUMzOS4xNjA1IDM3LjA5MjMgMzguNjc0IDM2LjU5MTYgMzguMDU2MSAzNi4yMjk0QzM3LjQzODIgMzUuODYwMSAzNi43MjggMzUuNjc1NCAzNS45MjU0IDM1LjY3NTRDMzUuMjcyIDM1LjY2ODMgMzQuNjI5MyAzNS43ODkxIDMzLjk5NzIgMzYuMDM3NkMzMy4zNjUxIDM2LjI4NjIgMzIuODY0MyAzNi42MTI5IDMyLjQ5NSAzNy4wMTc4TDI4LjkyNjEgMzYuNDMxOEwzMC4wNjYxIDI1LjE4MThINDIuNzIyM1YyOC40ODQ0SDMzLjMzNjZMMzIuNzA4MSAzNC4yNjkySDMyLjgzNTlDMzMuMjQwOCAzMy43OTMzIDMzLjgxMjUgMzMuMzk5MiAzNC41NTExIDMzLjA4NjdDMzUuMjg5OCAzMi43NjcxIDM2LjA5OTQgMzIuNjA3MyAzNi45ODAxIDMyLjYwNzNDMzguMzAxMSAzMi42MDczIDM5LjQ4MDEgMzIuOTE5OCA0MC41MTcgMzMuNTQ0OEM0MS41NTQgMzQuMTYyNiA0Mi4zNzA3IDM1LjAxNDkgNDIuOTY3MyAzNi4xMDE2QzQzLjU2MzkgMzcuMTg4MiA0My44NjIyIDM4LjQzMTEgNDMuODYyMiAzOS44MzAzQzQzLjg2MjIgNDEuMjcyIDQzLjUyODQgNDIuNTU3NSA0Mi44NjA4IDQzLjY4NjhDNDIuMjAwMyA0NC44MDkgNDEuMjgwNSA0NS42OTMyIDQwLjEwMTYgNDYuMzM5NUMzOC45Mjk3IDQ2Ljk3ODcgMzcuNTczMiA0Ny4yOTgzIDM2LjAzMiA0Ny4yOTgzWlwiLFwiZmlsbFwiOlwid2hpdGVcIixcImtleVwiOjJ9KV0pO1xufVxuXG5Qcml2YWN5U2NvcmU1LmRlZmF1bHRQcm9wcyA9IHtcIndpZHRoXCI6XCI3MlwiLFwiaGVpZ2h0XCI6XCI3MlwiLFwidmlld0JveFwiOlwiMCAwIDcyIDcyXCIsXCJmaWxsXCI6XCJub25lXCJ9O1xuXG5tb2R1bGUuZXhwb3J0cyA9IFByaXZhY3lTY29yZTU7XG5cblByaXZhY3lTY29yZTUuZGVmYXVsdCA9IFByaXZhY3lTY29yZTU7XG4iLCJ2YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG5mdW5jdGlvbiBQcml2YWN5U2NvcmVEaXNhYmxlZDEgKHByb3BzKSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzdmdcIixwcm9wcyxbUmVhY3QuY3JlYXRlRWxlbWVudChcImNpcmNsZVwiLHtcImN4XCI6XCIzNlwiLFwiY3lcIjpcIjM2XCIsXCJyXCI6XCIyOFwiLFwiZmlsbFwiOlwiI0NFRDFERFwiLFwia2V5XCI6MH0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZFwiOlwiTTM4LjE4MzIgMjQuNVY0Ni4zMTgySDM0LjIzMDhWMjguMzQ1OUgzNC4xMDNMMjkgMzEuNjA1OFYyNy45ODM3TDM0LjQyMjYgMjQuNUgzOC4xODMyWlwiLFwiZmlsbFwiOlwid2hpdGVcIixcImtleVwiOjF9KSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLHtcImZpbGxSdWxlXCI6XCJldmVub2RkXCIsXCJjbGlwUnVsZVwiOlwiZXZlbm9kZFwiLFwiZFwiOlwiTTM2IDcyQzU1Ljg4MjMgNzIgNzIgNTUuODgyMyA3MiAzNkM3MiAxNi4xMTc3IDU1Ljg4MjMgMCAzNiAwQzE2LjExNzcgMCAwIDE2LjExNzcgMCAzNkMwIDU1Ljg4MjMgMTYuMTE3NyA3MiAzNiA3MlpNMzYgNjhDNTMuNjczMSA2OCA2OCA1My42NzMxIDY4IDM2QzY4IDE4LjMyNjkgNTMuNjczMSA0IDM2IDRDMTguMzI2OSA0IDQgMTguMzI2OSA0IDM2QzQgNTMuNjczMSAxOC4zMjY5IDY4IDM2IDY4WlwiLFwiZmlsbFwiOlwiI0ZCRkJGRFwiLFwia2V5XCI6Mn0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZmlsbFJ1bGVcIjpcImV2ZW5vZGRcIixcImNsaXBSdWxlXCI6XCJldmVub2RkXCIsXCJkXCI6XCJNMzUuOTI4NiA3MS45OTk5TDM2IDcxLjVWNjhDMjEuNzY5IDY4IDkuNzA3NjUgNTguNzEwNCA1LjU0OTI2IDQ1Ljg2NDNMMS43NDI5MiA0Ny4wOTc0QzYuNDE0MTUgNjEuNTI3NyAxOS45NDk4IDcxLjk2ODkgMzUuOTI4NiA3MS45OTk5WlwiLFwiZmlsbFwiOlwiI0NFRDFERFwiLFwia2V5XCI6M30pXSk7XG59XG5cblByaXZhY3lTY29yZURpc2FibGVkMS5kZWZhdWx0UHJvcHMgPSB7XCJ3aWR0aFwiOlwiNzJcIixcImhlaWdodFwiOlwiNzJcIixcInZpZXdCb3hcIjpcIjAgMCA3MiA3MlwiLFwiZmlsbFwiOlwibm9uZVwifTtcblxubW9kdWxlLmV4cG9ydHMgPSBQcml2YWN5U2NvcmVEaXNhYmxlZDE7XG5cblByaXZhY3lTY29yZURpc2FibGVkMS5kZWZhdWx0ID0gUHJpdmFjeVNjb3JlRGlzYWJsZWQxO1xuIiwidmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxuZnVuY3Rpb24gUHJpdmFjeVNjb3JlRGlzYWJsZWQyIChwcm9wcykge1xuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3ZnXCIscHJvcHMsW1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJjaXJjbGVcIix7XCJjeFwiOlwiMzZcIixcImN5XCI6XCIzNlwiLFwiclwiOlwiMjhcIixcImZpbGxcIjpcIiNDRUQxRERcIixcImtleVwiOjB9KSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLHtcImRcIjpcIk0yOC4xMjc4IDQ2LjYxNjVWNDMuNzYxNEwzNS43MDI0IDM2LjMzNTlDMzYuNDI2OCAzNS42MDQ0IDM3LjAzMDUgMzQuOTU0NSAzNy41MTM1IDM0LjM4NjRDMzcuOTk2NCAzMy44MTgyIDM4LjM1ODcgMzMuMjY3OCAzOC42MDAxIDMyLjczNTFDMzguODQxNiAzMi4yMDI0IDM4Ljk2MjQgMzEuNjM0MiAzOC45NjI0IDMxLjAzMDVDMzguOTYyNCAzMC4zNDE2IDM4LjgwNjEgMjkuNzUyMSAzOC40OTM2IDI5LjI2MjFDMzguMTgxMSAyOC43NjQ5IDM3Ljc1MTQgMjguMzgxNCAzNy4yMDQ1IDI4LjExMTVDMzYuNjU3NyAyNy44NDE2IDM2LjAzNjIgMjcuNzA2NyAzNS4zNDAyIDI3LjcwNjdDMzQuNjIyOSAyNy43MDY3IDMzLjk5NDMgMjcuODU1OCAzMy40NTQ1IDI4LjE1NDFDMzIuOTE0OCAyOC40NDUzIDMyLjQ5NTcgMjguODYwOCAzMi4xOTc0IDI5LjQwMDZDMzEuOTA2MyAyOS45NDAzIDMxLjc2MDcgMzAuNTgzMSAzMS43NjA3IDMxLjMyODhIMjhDMjggMjkuOTQzOSAyOC4zMTYxIDI4Ljc0MDEgMjguOTQ4MiAyNy43MTczQzI5LjU4MDMgMjYuNjk0NiAzMC40NTAzIDI1LjkwMjcgMzEuNTU4MiAyNS4zNDE2QzMyLjY3MzMgMjQuNzgwNSAzMy45NTE3IDI0LjUgMzUuMzkzNSAyNC41QzM2Ljg1NjUgMjQuNSAzOC4xNDIgMjQuNzczNCAzOS4yNSAyNS4zMjAzQzQwLjM1OCAyNS44NjcyIDQxLjIxNzMgMjYuNjE2NSA0MS44MjgxIDI3LjU2ODJDNDIuNDQ2IDI4LjUxOTkgNDIuNzU1IDI5LjYwNjUgNDIuNzU1IDMwLjgyODFDNDIuNzU1IDMxLjY0NDkgNDIuNTk4NyAzMi40NDc0IDQyLjI4NjIgMzMuMjM1OEM0MS45NzM3IDM0LjAyNDEgNDEuNDIzMyAzNC44OTc3IDQwLjYzNDkgMzUuODU2NUMzOS44NTM3IDM2LjgxNTMgMzguNzU2NCAzNy45NzY2IDM3LjM0MyAzOS4zNDAyTDMzLjU4MjQgNDMuMTY0OFY0My4zMTM5SDQzLjA4NTJWNDYuNjE2NUgyOC4xMjc4WlwiLFwiZmlsbFwiOlwid2hpdGVcIixcImtleVwiOjF9KSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLHtcImZpbGxSdWxlXCI6XCJldmVub2RkXCIsXCJjbGlwUnVsZVwiOlwiZXZlbm9kZFwiLFwiZFwiOlwiTTM2IDcyQzU1Ljg4MjMgNzIgNzIgNTUuODgyMyA3MiAzNkM3MiAxNi4xMTc3IDU1Ljg4MjMgMCAzNiAwQzE2LjExNzcgMCAwIDE2LjExNzcgMCAzNkMwIDU1Ljg4MjMgMTYuMTE3NyA3MiAzNiA3MlpNMzYgNjhDNTMuNjczMSA2OCA2OCA1My42NzMxIDY4IDM2QzY4IDE4LjMyNjkgNTMuNjczMSA0IDM2IDRDMTguMzI2OSA0IDQgMTguMzI2OSA0IDM2QzQgNTMuNjczMSAxOC4zMjY5IDY4IDM2IDY4WlwiLFwiZmlsbFwiOlwiI0ZCRkJGRFwiLFwia2V5XCI6Mn0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZmlsbFJ1bGVcIjpcImV2ZW5vZGRcIixcImNsaXBSdWxlXCI6XCJldmVub2RkXCIsXCJkXCI6XCJNMzUuOTI3MyA3MS45OTk5TDM2IDcxLjc2MzZWNjhDMTguMzI2OSA2OCA0IDUzLjY3MzEgNCAzNkM0IDI1LjMzNzQgOS4yMTQ5NiAxNS44OTI4IDE3LjIzMzQgMTAuMDc3N0wxNC45MDMyIDYuODI2MTdDNS44NzM1NyAxMy4zNjcyIDAgMjMuOTk3NyAwIDM2QzAgNTUuODU4IDE2LjA3ODUgNzEuOTYwNyAzNS45MjczIDcxLjk5OTlaXCIsXCJmaWxsXCI6XCIjQ0VEMUREXCIsXCJrZXlcIjozfSldKTtcbn1cblxuUHJpdmFjeVNjb3JlRGlzYWJsZWQyLmRlZmF1bHRQcm9wcyA9IHtcIndpZHRoXCI6XCI3MlwiLFwiaGVpZ2h0XCI6XCI3MlwiLFwidmlld0JveFwiOlwiMCAwIDcyIDcyXCIsXCJmaWxsXCI6XCJub25lXCJ9O1xuXG5tb2R1bGUuZXhwb3J0cyA9IFByaXZhY3lTY29yZURpc2FibGVkMjtcblxuUHJpdmFjeVNjb3JlRGlzYWJsZWQyLmRlZmF1bHQgPSBQcml2YWN5U2NvcmVEaXNhYmxlZDI7XG4iLCJ2YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG5mdW5jdGlvbiBQcml2YWN5U2NvcmVEaXNhYmxlZDMgKHByb3BzKSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzdmdcIixwcm9wcyxbUmVhY3QuY3JlYXRlRWxlbWVudChcImNpcmNsZVwiLHtcImN4XCI6XCIzNlwiLFwiY3lcIjpcIjM2XCIsXCJyXCI6XCIyOFwiLFwiZmlsbFwiOlwiI0NFRDFERFwiLFwia2V5XCI6MH0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZmlsbFJ1bGVcIjpcImV2ZW5vZGRcIixcImNsaXBSdWxlXCI6XCJldmVub2RkXCIsXCJkXCI6XCJNMzYgNzJDNTUuODgyMyA3MiA3MiA1NS44ODIzIDcyIDM2QzcyIDE2LjExNzcgNTUuODgyMyAwIDM2IDBDMTYuMTE3NyAwIDAgMTYuMTE3NyAwIDM2QzAgNTUuODgyMyAxNi4xMTc3IDcyIDM2IDcyWk0zNiA2OEM1My42NzMxIDY4IDY4IDUzLjY3MzEgNjggMzZDNjggMTguMzI2OSA1My42NzMxIDQgMzYgNEMxOC4zMjY5IDQgNCAxOC4zMjY5IDQgMzZDNCA1My42NzMxIDE4LjMyNjkgNjggMzYgNjhaXCIsXCJmaWxsXCI6XCIjRkJGQkZEXCIsXCJrZXlcIjoxfSksUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIix7XCJmaWxsUnVsZVwiOlwiZXZlbm9kZFwiLFwiY2xpcFJ1bGVcIjpcImV2ZW5vZGRcIixcImRcIjpcIk01NC45MjUgMTAuMTkzNEM0OS42MjQ3IDYuMjk5ODcgNDMuMDgwOSA0IDM2IDRDMTguMzI2OSA0IDQgMTguMzI2OSA0IDM2QzQgNTMuNjczMSAxOC4zMjY5IDY4IDM2IDY4VjcxLjUwMDJMMzUuODg5IDcxLjk5OThDMTYuMDU3OCA3MS45NCAwIDU1Ljg0NTIgMCAzNkMwIDE2LjExNzcgMTYuMTE3NyAwIDM2IDBDNDMuOTY2IDAgNTEuMzI3NyAyLjU4NzM2IDU3LjI5MDYgNi45Njc1NEw1NC45MjUgMTAuMTkzNFpcIixcImZpbGxcIjpcIiNDRUQxRERcIixcImtleVwiOjJ9KSxSZWFjdC5jcmVhdGVFbGVtZW50KFwicGF0aFwiLHtcImRcIjpcIk0zNi4wMDA3IDQ2LjkxNDhDMzQuNDY2NiA0Ni45MTQ4IDMzLjEwMyA0Ni42NTIgMzEuOTA5OCA0Ni4xMjY0QzMwLjcyMzcgNDUuNjAwOSAyOS43ODYyIDQ0Ljg2OTMgMjkuMDk3MyA0My45MzE4QzI4LjQwODQgNDIuOTk0MyAyOC4wNDI2IDQxLjkxMTIgMjggNDAuNjgyNUgzMi4wMDU3QzMyLjA0MTIgNDEuMjcyIDMyLjIzNjUgNDEuNzg2OSAzMi41OTE2IDQyLjIyNzNDMzIuOTQ2NyA0Mi42NjA1IDMzLjQxOSA0Mi45OTc5IDM0LjAwODUgNDMuMjM5M0MzNC41OTggNDMuNDgwOCAzNS4yNTg1IDQzLjYwMTYgMzUuOTkwMSA0My42MDE2QzM2Ljc3MTMgNDMuNjAxNiAzNy40NjM4IDQzLjQ2NjYgMzguMDY3NSA0My4xOTY3QzM4LjY3MTIgNDIuOTE5NyAzOS4xNDM1IDQyLjUzNjIgMzkuNDg0NCA0Mi4wNDYyQzM5LjgyNTMgNDEuNTU2MSAzOS45OTIyIDQwLjk5MTUgMzkuOTg1MSA0MC4zNTIzQzM5Ljk5MjIgMzkuNjkxOCAzOS44MjE3IDM5LjEwOTQgMzkuNDczNyAzOC42MDUxQzM5LjEyNTcgMzguMTAwOSAzOC42MjE0IDM3LjcwNjcgMzcuOTYwOSAzNy40MjI2QzM3LjMwNzUgMzcuMTM4NSAzNi41MTkyIDM2Ljk5NjQgMzUuNTk1OSAzNi45OTY0SDMzLjY2NzZWMzMuOTQ5NkgzNS41OTU5QzM2LjM1NTggMzMuOTQ5NiAzNy4wMTk5IDMzLjgxODIgMzcuNTg4MSAzMy41NTU0QzM4LjE2MzQgMzMuMjkyNiAzOC42MTQzIDMyLjkyMzMgMzguOTQxMSAzMi40NDc0QzM5LjI2NzggMzEuOTY0NSAzOS40Mjc2IDMxLjQwNyAzOS40MjA1IDMwLjc3NDlDMzkuNDI3NiAzMC4xNTcgMzkuMjg5MSAyOS42MjA3IDM5LjAwNSAyOS4xNjYyQzM4LjcyOCAyOC43MDQ1IDM4LjMzMzggMjguMzQ1OSAzNy44MjI0IDI4LjA5MDJDMzcuMzE4MiAyNy44MzQ1IDM2LjcyNTEgMjcuNzA2NyAzNi4wNDMzIDI3LjcwNjdDMzUuMzc1NyAyNy43MDY3IDM0Ljc1NzggMjcuODI3NCAzNC4xODk2IDI4LjA2ODlDMzMuNjIxNCAyOC4zMTA0IDMzLjE2MzQgMjguNjU0OCAzMi44MTUzIDI5LjEwMjNDMzIuNDY3MyAyOS41NDI2IDMyLjI4MjcgMzAuMDY4MiAzMi4yNjE0IDMwLjY3OUgyOC40NTgxQzI4LjQ4NjUgMjkuNDU3NCAyOC44MzgxIDI4LjM4NDkgMjkuNTEyOCAyNy40NjE2QzMwLjE5NDYgMjYuNTMxMyAzMS4xMDM3IDI1LjgwNjggMzIuMjQwMSAyNS4yODg0QzMzLjM3NjQgMjQuNzYyOCAzNC42NTEzIDI0LjUgMzYuMDY0NiAyNC41QzM3LjUyMDYgMjQuNSAzOC43ODQ4IDI0Ljc3MzQgMzkuODU3MiAyNS4zMjAzQzQwLjkzNjggMjUuODYwMSA0MS43NzEzIDI2LjU4ODEgNDIuMzYwOCAyNy41MDQzQzQyLjk1MDMgMjguNDIwNSA0My4yNDUgMjkuNDMyNSA0My4yNDUgMzAuNTQwNUM0My4yNTIxIDMxLjc2OTIgNDIuODg5OSAzMi43OTkgNDIuMTU4NCAzMy42M0M0MS40MzM5IDM0LjQ2MDkgNDAuNDgyMiAzNS4wMDQzIDM5LjMwMzMgMzUuMjU5OVYzNS40MzA0QzQwLjgzNzQgMzUuNjQzNSA0Mi4wMTI4IDM2LjIxMTYgNDIuODI5NSAzNy4xMzQ5QzQzLjY1MzQgMzguMDUxMSA0NC4wNjE4IDM5LjE5MTEgNDQuMDU0NyA0MC41NTQ3QzQ0LjA1NDcgNDEuNzc2MyA0My43MDY3IDQyLjg3IDQzLjAxMDcgNDMuODM1OUM0Mi4zMjE3IDQ0Ljc5NDcgNDEuMzcgNDUuNTQ3NiA0MC4xNTU1IDQ2LjA5NDVDMzguOTQ4MiA0Ni42NDEzIDM3LjU2MzIgNDYuOTE0OCAzNi4wMDA3IDQ2LjkxNDhaXCIsXCJmaWxsXCI6XCJ3aGl0ZVwiLFwia2V5XCI6M30pXSk7XG59XG5cblByaXZhY3lTY29yZURpc2FibGVkMy5kZWZhdWx0UHJvcHMgPSB7XCJ3aWR0aFwiOlwiNzNcIixcImhlaWdodFwiOlwiNzJcIixcInZpZXdCb3hcIjpcIjAgMCA3MyA3MlwiLFwiZmlsbFwiOlwibm9uZVwifTtcblxubW9kdWxlLmV4cG9ydHMgPSBQcml2YWN5U2NvcmVEaXNhYmxlZDM7XG5cblByaXZhY3lTY29yZURpc2FibGVkMy5kZWZhdWx0ID0gUHJpdmFjeVNjb3JlRGlzYWJsZWQzO1xuIiwidmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxuZnVuY3Rpb24gUHJpdmFjeVNjb3JlRGlzYWJsZWQ0IChwcm9wcykge1xuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3ZnXCIscHJvcHMsW1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJjaXJjbGVcIix7XCJjeFwiOlwiMzYuNzkxMVwiLFwiY3lcIjpcIjM2XCIsXCJyXCI6XCIyOFwiLFwiZmlsbFwiOlwiI0NFRDFERFwiLFwia2V5XCI6MH0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZmlsbFJ1bGVcIjpcImV2ZW5vZGRcIixcImNsaXBSdWxlXCI6XCJldmVub2RkXCIsXCJkXCI6XCJNMzYuNzkxMSA3MkM1Ni42NzM0IDcyIDcyLjc5MTEgNTUuODgyMyA3Mi43OTExIDM2QzcyLjc5MTEgMTYuMTE3NyA1Ni42NzM0IDAgMzYuNzkxMSAwQzE2LjkwODkgMCAwLjc5MTEzOCAxNi4xMTc3IDAuNzkxMTM4IDM2QzAuNzkxMTM4IDU1Ljg4MjMgMTYuOTA4OSA3MiAzNi43OTExIDcyWk0zNi43OTExIDY4QzU0LjQ2NDIgNjggNjguNzkxMSA1My42NzMxIDY4Ljc5MTEgMzZDNjguNzkxMSAxOC4zMjY5IDU0LjQ2NDIgNCAzNi43OTExIDRDMTkuMTE4IDQgNC43OTExNCAxOC4zMjY5IDQuNzkxMTQgMzZDNC43OTExNCA1My42NzMxIDE5LjExOCA2OCAzNi43OTExIDY4WlwiLFwiZmlsbFwiOlwiI0ZCRkJGRFwiLFwia2V5XCI6MX0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZmlsbFJ1bGVcIjpcImV2ZW5vZGRcIixcImNsaXBSdWxlXCI6XCJldmVub2RkXCIsXCJkXCI6XCJNNjcuMTE4NSA0Ni4yMzcxQzY4LjIwMzEgNDMuMDIyOSA2OC43OTEgMzkuNTgwMSA2OC43OTEgMzZDNjguNzkxIDE4LjMyNjkgNTQuNDY0MSA0LjAwMDAyIDM2Ljc5MSA0LjAwMDAyQzE5LjExNzkgNC4wMDAwMiA0Ljc5MTAyIDE4LjMyNjkgNC43OTEwMiAzNkM0Ljc5MTAyIDUzLjYwMzQgMTkuMDA1MSA2Ny44ODY5IDM2LjU4MiA2Ny45OTkzVjcxLjAwMDlMMzYuMzMzIDcxLjk5NzJDMTYuNjYxOCA3MS43NTE5IDAuNzkxMDE2IDU1LjcyOTQgMC43OTEwMTYgMzZDMC43OTEwMTYgMTYuMTE3OCAxNi45MDg4IDEuNTI1ODhlLTA1IDM2Ljc5MSAxLjUyNTg4ZS0wNUM1Ni42NzMzIDEuNTI1ODhlLTA1IDcyLjc5MSAxNi4xMTc4IDcyLjc5MSAzNkM3Mi43OTEgNDAuMDAzOSA3Mi4xMzc0IDQzLjg1NTEgNzAuOTMxIDQ3LjQ1MjdMNjcuMTE4NSA0Ni4yMzcxWlwiLFwiZmlsbFwiOlwiI0NFRDFERFwiLFwia2V5XCI6Mn0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZFwiOlwiTTI2Ljc5MSA0Mi4yNjk5VjM5LjEyNzFMMzYuMDQ4OCAyNC41SDM4LjY2OTZWMjguOTc0NEgzNy4wNzE2TDMwLjgzOTMgMzguODUwMVYzOS4wMjA2SDQzLjc2MTlWNDIuMjY5OUgyNi43OTFaTTM3LjE5OTQgNDYuMzE4MlY0MS4zMTExTDM3LjI0MiAzOS45MDQ4VjI0LjVINDAuOTcwN1Y0Ni4zMTgySDM3LjE5OTRaXCIsXCJmaWxsXCI6XCJ3aGl0ZVwiLFwia2V5XCI6M30pXSk7XG59XG5cblByaXZhY3lTY29yZURpc2FibGVkNC5kZWZhdWx0UHJvcHMgPSB7XCJ3aWR0aFwiOlwiNzNcIixcImhlaWdodFwiOlwiNzJcIixcInZpZXdCb3hcIjpcIjAgMCA3MyA3MlwiLFwiZmlsbFwiOlwibm9uZVwifTtcblxubW9kdWxlLmV4cG9ydHMgPSBQcml2YWN5U2NvcmVEaXNhYmxlZDQ7XG5cblByaXZhY3lTY29yZURpc2FibGVkNC5kZWZhdWx0ID0gUHJpdmFjeVNjb3JlRGlzYWJsZWQ0O1xuIiwidmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxuZnVuY3Rpb24gUHJpdmFjeVNjb3JlRGlzYWJsZWQ1IChwcm9wcykge1xuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3ZnXCIscHJvcHMsW1JlYWN0LmNyZWF0ZUVsZW1lbnQoXCJjaXJjbGVcIix7XCJjeFwiOlwiMzYuMDAwMVwiLFwiY3lcIjpcIjM2XCIsXCJyXCI6XCIyOFwiLFwiZmlsbFwiOlwiI0NFRDFERFwiLFwia2V5XCI6MH0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZmlsbFJ1bGVcIjpcImV2ZW5vZGRcIixcImNsaXBSdWxlXCI6XCJldmVub2RkXCIsXCJkXCI6XCJNMzYuMDAwMSA3MkM1NS44ODI0IDcyIDcyLjAwMDEgNTUuODgyMyA3Mi4wMDAxIDM2QzcyLjAwMDEgMTYuMTE3NyA1NS44ODI0IDAgMzYuMDAwMSAwQzE2LjExNzkgMCAwLjAwMDEyMjA3IDE2LjExNzcgMC4wMDAxMjIwNyAzNkMwLjAwMDEyMjA3IDU1Ljg4MjMgMTYuMTE3OSA3MiAzNi4wMDAxIDcyWk0zNi4wMDAxIDY4QzUzLjY3MzIgNjggNjguMDAwMSA1My42NzMxIDY4LjAwMDEgMzZDNjguMDAwMSAxOC4zMjY5IDUzLjY3MzIgNCAzNi4wMDAxIDRDMTguMzI3IDQgNC4wMDAxMiAxOC4zMjY5IDQuMDAwMTIgMzZDNC4wMDAxMiA1My42NzMxIDE4LjMyNyA2OCAzNi4wMDAxIDY4WlwiLFwiZmlsbFwiOlwiI0NFRDFERFwiLFwia2V5XCI6MX0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZFwiOlwiTTM1LjUzMiA0Ni4xMTY1QzM0LjExMTUgNDYuMTE2NSAzMi44NDAyIDQ1Ljg1MDEgMzEuNzE4IDQ1LjMxNzVDMzAuNTk1OSA0NC43Nzc3IDI5LjcwNDUgNDQuMDM5MSAyOS4wNDQgNDMuMTAxNkMyOC4zOTA2IDQyLjE2NDEgMjguMDQyNiA0MS4wOTE2IDI4IDM5Ljg4NDJIMzEuODM1MkMzMS45MDYyIDQwLjc3OTEgMzIuMjkzMyA0MS41MTA3IDMyLjk5NjQgNDIuMDc4OEMzMy42OTk2IDQyLjYzOTkgMzQuNTQ0NyA0Mi45MjA1IDM1LjUzMiA0Mi45MjA1QzM2LjMwNjEgNDIuOTIwNSAzNi45OTUgNDIuNzQyOSAzNy41OTg3IDQyLjM4NzhDMzguMjAyNCA0Mi4wMzI3IDM4LjY3ODMgNDEuNTM5MSAzOS4wMjYzIDQwLjkwN0MzOS4zNzQzIDQwLjI3NDkgMzkuNTQ0NyAzOS41NTQgMzkuNTM3NiAzOC43NDQzQzM5LjU0NDcgMzcuOTIwNSAzOS4zNzA3IDM3LjE4ODkgMzkuMDE1NiAzNi41NDk3QzM4LjY2MDUgMzUuOTEwNSAzOC4xNzQgMzUuNDA5OCAzNy41NTYxIDM1LjA0NzZDMzYuOTM4MiAzNC42NzgzIDM2LjIyOCAzNC40OTM2IDM1LjQyNTQgMzQuNDkzNkMzNC43NzIgMzQuNDg2NSAzNC4xMjkzIDM0LjYwNzIgMzMuNDk3MiAzNC44NTU4QzMyLjg2NTEgMzUuMTA0NCAzMi4zNjQzIDM1LjQzMTEgMzEuOTk1IDM1LjgzNTlMMjguNDI2MSAzNS4yNUwyOS41NjYxIDI0SDQyLjIyMjNWMjcuMzAyNkgzMi44MzY2TDMyLjIwODEgMzMuMDg3NEgzMi4zMzU5QzMyLjc0MDggMzIuNjExNSAzMy4zMTI1IDMyLjIxNzMgMzQuMDUxMSAzMS45MDQ4QzM0Ljc4OTggMzEuNTg1MiAzNS41OTk0IDMxLjQyNTQgMzYuNDgwMSAzMS40MjU0QzM3LjgwMTEgMzEuNDI1NCAzOC45ODAxIDMxLjczNzkgNDAuMDE3IDMyLjM2MjlDNDEuMDU0IDMyLjk4MDggNDEuODcwNyAzMy44MzMxIDQyLjQ2NzMgMzQuOTE5N0M0My4wNjM5IDM2LjAwNjQgNDMuMzYyMiAzNy4yNDkzIDQzLjM2MjIgMzguNjQ4NEM0My4zNjIyIDQwLjA5MDIgNDMuMDI4NCA0MS4zNzU3IDQyLjM2MDggNDIuNTA1QzQxLjcwMDMgNDMuNjI3MSA0MC43ODA1IDQ0LjUxMTQgMzkuNjAxNiA0NS4xNTc3QzM4LjQyOTcgNDUuNzk2OSAzNy4wNzMyIDQ2LjExNjUgMzUuNTMyIDQ2LjExNjVaXCIsXCJmaWxsXCI6XCJ3aGl0ZVwiLFwia2V5XCI6Mn0pXSk7XG59XG5cblByaXZhY3lTY29yZURpc2FibGVkNS5kZWZhdWx0UHJvcHMgPSB7XCJ3aWR0aFwiOlwiNzJcIixcImhlaWdodFwiOlwiNzJcIixcInZpZXdCb3hcIjpcIjAgMCA3MiA3MlwiLFwiZmlsbFwiOlwibm9uZVwifTtcblxubW9kdWxlLmV4cG9ydHMgPSBQcml2YWN5U2NvcmVEaXNhYmxlZDU7XG5cblByaXZhY3lTY29yZURpc2FibGVkNS5kZWZhdWx0ID0gUHJpdmFjeVNjb3JlRGlzYWJsZWQ1O1xuIiwidmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxuZnVuY3Rpb24gUHJpdmFjeVNjb3JlTmEgKHByb3BzKSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzdmdcIixwcm9wcyxbUmVhY3QuY3JlYXRlRWxlbWVudChcImNpcmNsZVwiLHtcImN4XCI6XCIzNlwiLFwiY3lcIjpcIjM2XCIsXCJyXCI6XCIyOFwiLFwiZmlsbFwiOlwiIzY1NzNGRlwiLFwia2V5XCI6MH0pLFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJwYXRoXCIse1wiZmlsbFJ1bGVcIjpcImV2ZW5vZGRcIixcImNsaXBSdWxlXCI6XCJldmVub2RkXCIsXCJkXCI6XCJNMzYgNzJDNTUuODgyMyA3MiA3MiA1NS44ODIzIDcyIDM2QzcyIDE2LjExNzcgNTUuODgyMyAwIDM2IDBDMTYuMTE3NyAwIDAgMTYuMTE3NyAwIDM2QzAgNTUuODgyMyAxNi4xMTc3IDcyIDM2IDcyWk0zNiA2OEM1My42NzMxIDY4IDY4IDUzLjY3MzEgNjggMzZDNjggMTguMzI2OSA1My42NzMxIDQgMzYgNEMxOC4zMjY5IDQgNCAxOC4zMjY5IDQgMzZDNCA1My42NzMxIDE4LjMyNjkgNjggMzYgNjhaXCIsXCJmaWxsXCI6XCIjRUJFQ0Y3XCIsXCJrZXlcIjoxfSksUmVhY3QuY3JlYXRlRWxlbWVudChcInBhdGhcIix7XCJkXCI6XCJNMzMuMjcyNyA0MC40NTg4VjQwLjA5NjZDMzMuMjc5OCAzOC44NTM3IDMzLjM4OTkgMzcuODYyOSAzMy42MDMgMzcuMTI0M0MzMy44MjMyIDM2LjM4NTcgMzQuMTQyOCAzNS43ODkxIDM0LjU2MTggMzUuMzM0NUMzNC45ODA4IDM0Ljg4IDM1LjQ4NTEgMzQuNDY4IDM2LjA3NDYgMzQuMDk4N0MzNi41MTQ5IDMzLjgxNDYgMzYuOTA5MSAzMy41MTk5IDM3LjI1NzEgMzMuMjE0NUMzNy42MDUxIDMyLjkwOTEgMzcuODgyMSAzMi41NzE3IDM4LjA4ODEgMzIuMjAyNEMzOC4yOTQgMzEuODI2IDM4LjM5NyAzMS40MDcgMzguMzk3IDMwLjk0NTNDMzguMzk3IDMwLjQ1NTMgMzguMjc5OCAzMC4wMjU2IDM4LjA0NTUgMjkuNjU2MkMzNy44MTExIDI5LjI4NjkgMzcuNDk1IDI5LjAwMjggMzcuMDk3MyAyOC44MDRDMzYuNzA2NyAyOC42MDUxIDM2LjI3MzQgMjguNTA1NyAzNS43OTc2IDI4LjUwNTdDMzUuMzM1OSAyOC41MDU3IDM0Ljg5OTEgMjguNjA4NyAzNC40ODcyIDI4LjgxNDZDMzQuMDc1MyAyOS4wMTM1IDMzLjczNzkgMjkuMzExOCAzMy40NzUxIDI5LjcwOTVDMzMuMjEyNCAzMC4xMDAxIDMzLjA3MDMgMzAuNTg2NiAzMy4wNDkgMzEuMTY5SDI4LjcwMjRDMjguNzM3OSAyOS43NDg2IDI5LjA3ODggMjguNTc2NyAyOS43MjUxIDI3LjY1MzRDMzAuMzcxNCAyNi43MjMgMzEuMjI3MyAyNi4wMzA1IDMyLjI5MjYgMjUuNTc2QzMzLjM1OCAyNS4xMTQzIDM0LjUzMzQgMjQuODgzNSAzNS44MTg5IDI0Ljg4MzVDMzcuMjMyMiAyNC44ODM1IDM4LjQ4MjIgMjUuMTE3OSAzOS41Njg5IDI1LjU4NjZDNDAuNjU1NSAyNi4wNDgzIDQxLjUwNzggMjYuNzE5NSA0Mi4xMjU3IDI3LjYwMDFDNDIuNzQzNiAyOC40ODA4IDQzLjA1MjYgMjkuNTQyNiA0My4wNTI2IDMwLjc4NTVDNDMuMDUyNiAzMS42MTY1IDQyLjkxNDEgMzIuMzU1MSA0Mi42MzcxIDMzLjAwMTRDNDIuMzY3MiAzMy42NDA2IDQxLjk4NzIgMzQuMjA4OCA0MS40OTcyIDM0LjcwNkM0MS4wMDcxIDM1LjE5NiA0MC40MjgzIDM1LjYzOTkgMzkuNzYwNyAzNi4wMzc2QzM5LjE5OTYgMzYuMzcxNCAzOC43Mzc5IDM2LjcxOTUgMzguMzc1NyAzNy4wODE3QzM4LjAyMDYgMzcuNDQzOSAzNy43NTQzIDM3Ljg2MjkgMzcuNTc2NyAzOC4zMzg4QzM3LjQwNjMgMzguODE0NiAzNy4zMTc1IDM5LjQwMDYgMzcuMzEwNCA0MC4wOTY2VjQwLjQ1ODhIMzMuMjcyN1pNMzUuMzgyMSA0Ny4yNzdDMzQuNjcxOSA0Ny4yNzcgMzQuMDY0NiA0Ny4wMjg0IDMzLjU2MDQgNDYuNTMxMkMzMy4wNjMyIDQ2LjAyNyAzMi44MTgyIDQ1LjQyMzMgMzIuODI1MyA0NC43MjAyQzMyLjgxODIgNDQuMDI0MSAzMy4wNjMyIDQzLjQyNzYgMzMuNTYwNCA0Mi45MzA0QzM0LjA2NDYgNDIuNDMzMiAzNC42NzE5IDQyLjE4NDcgMzUuMzgyMSA0Mi4xODQ3QzM2LjA1NjggNDIuMTg0NyAzNi42NDk5IDQyLjQzMzIgMzcuMTYxMiA0Mi45MzA0QzM3LjY3MjYgNDMuNDI3NiAzNy45MzE4IDQ0LjAyNDEgMzcuOTM4OSA0NC43MjAyQzM3LjkzMTggNDUuMTg4OSAzNy44MDc1IDQ1LjYxODYgMzcuNTY2MSA0Ni4wMDkyQzM3LjMzMTcgNDYuMzkyOCAzNy4wMjI3IDQ2LjcwMTcgMzYuNjM5MiA0Ni45MzYxQzM2LjI1NTcgNDcuMTYzNCAzNS44MzY2IDQ3LjI3NyAzNS4zODIxIDQ3LjI3N1pcIixcImZpbGxcIjpcIiMyRTM5QjNcIixcImtleVwiOjJ9KV0pO1xufVxuXG5Qcml2YWN5U2NvcmVOYS5kZWZhdWx0UHJvcHMgPSB7XCJ3aWR0aFwiOlwiNzJcIixcImhlaWdodFwiOlwiNzJcIixcInZpZXdCb3hcIjpcIjAgMCA3MiA3MlwiLFwiZmlsbFwiOlwibm9uZVwifTtcblxubW9kdWxlLmV4cG9ydHMgPSBQcml2YWN5U2NvcmVOYTtcblxuUHJpdmFjeVNjb3JlTmEuZGVmYXVsdCA9IFByaXZhY3lTY29yZU5hO1xuIiwiaW1wb3J0IFJlYWN0LCB7RkN9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7Y3NzfSBmcm9tICdAZW1vdGlvbi9jb3JlJztcblxuaW1wb3J0IHtnZXREaXNwbGF5Q291bnR9IGZyb20gJy4uLy4uLy4uL3V0aWxzL2Rpc3BsYXknO1xuaW1wb3J0IHtnZXRNZXNzYWdlfSBmcm9tICcuLi8uLi8uLi91dGlscy9sb2NhbGl6YXRpb24nO1xuXG50eXBlIHRyYWNrZXJEaXNwbGF5UHJvcHMgPSB7XG4gICAgYmxvY2tlZDogYm9vbGVhbjtcbiAgICBjb3VudDogbnVtYmVyO1xuICAgIHN1YnRpdGxlOiBzdHJpbmc7XG59O1xuXG5leHBvcnQgY29uc3QgVHJhY2tlckRpc3BsYXk6IEZDPHRyYWNrZXJEaXNwbGF5UHJvcHM+ID0gKHtibG9ja2VkLCBjb3VudCwgc3VidGl0bGV9KSA9PiB7XG4gICAgY29uc3QgdHJhY2tlckRpc3BsYXlDc3MgPSBjc3NgXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNlYmVjZjc7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgaGVpZ2h0OiA4N3B4O1xuICAgICAgICBwYWRkaW5nOiAyNHB4IDE2cHg7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgd2lkdGg6IDEwNnB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAxMXB4O1xuICAgIGA7XG5cbiAgICBjb25zdCByZXNvbHZlVGl0bGVCZ0NvbG9yID0gKCkgPT4ge1xuICAgICAgICBpZiAoYmxvY2tlZCkge1xuICAgICAgICAgICAgcmV0dXJuICcjMjlERENDJztcbiAgICAgICAgfVxuICAgICAgICBpZiAoY291bnQgPiAwKSB7XG4gICAgICAgICAgICByZXR1cm4gJyNlYjU3NTcnO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAnIzdmODY5Zic7XG4gICAgfTtcblxuICAgIGNvbnN0IHRpdGxlQ3NzID0gY3NzYFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBiYWNrZ3JvdW5kOiAke3Jlc29sdmVUaXRsZUJnQ29sb3IoKX07XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE3cHg7XG4gICAgICAgIGNvbG9yOiAke2Jsb2NrZWQgPyAnIzIwMjk0NScgOiAnI2ZmZmZmZid9O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIGhlaWdodDogMjJweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgICAgIHBhZGRpbmc6IDAgMTJweDtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IC0xMXB4O1xuICAgICAgICB0cmFuc2l0aW9uOiAwLjRzO1xuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIGA7XG5cbiAgICBjb25zdCBjb3VudENzcyA9IGNzc2BcbiAgICAgICAgZm9udC1zaXplOiA2MHB4O1xuICAgICAgICBmb250LXdlaWdodDogNzAwO1xuICAgIGA7XG5cbiAgICBjb25zdCBzdWJ0aXRsZUNzcyA9IGNzc2BcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGA7XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2IGNzcz17dHJhY2tlckRpc3BsYXlDc3N9PlxuICAgICAgICAgICAgPGRpdiBjc3M9e3RpdGxlQ3NzfT5cbiAgICAgICAgICAgICAgICB7YmxvY2tlZCA/IGdldE1lc3NhZ2UoJ3BvcHVwSG9tZUJsb2NrZWRMYWJlbCcpIDogZ2V0TWVzc2FnZSgncG9wdXBIb21lTm90QmxvY2tlZExhYmVsJyl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY3NzPXtjb3VudENzc30+e2dldERpc3BsYXlDb3VudCh7Y291bnR9KX08L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY3NzPXtzdWJ0aXRsZUNzc30+e3N1YnRpdGxlfTwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICApO1xufTtcbiIsImltcG9ydCBSZWFjdCwge0ZDfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQge2Nzc30gZnJvbSAnQGVtb3Rpb24vY29yZSc7XG5pbXBvcnQge3VzZVNlbGVjdG9yfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB7Z2V0TWVzc2FnZX0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvbG9jYWxpemF0aW9uJztcbmltcG9ydCBzY3JlZW5NYW5hZ2VtZW50U2xpY2UsIHtTY3JlZW5zfSBmcm9tICcuLi8uLi8uLi9yZWR1Y2Vycy9TY3JlZW5zJztcbmltcG9ydCB7dGFiRGF0YVNlbGVjdG9yfSBmcm9tICcuLi8uLi8uLi9zZWxlY3RvcnMvaW5kZXgnO1xuaW1wb3J0IHtUcmFja2VyRGlzcGxheX0gZnJvbSAnLi9UcmFja2VyRGlzcGxheSc7XG5pbXBvcnQge3VzZUFwcERpc3BhdGNofSBmcm9tICcuLi8uLi8uLi9zdG9yZSc7XG5cbi8vIEB0cy1leHBlY3QtZXJyb3JcbmltcG9ydCBIaWRkZW5JY29uIGZyb20gJy4uLy4uLy4uL2ljb25zL2hpZGRlbi1pY29uLnN2Zyc7XG5cbmNvbnN0IGJvdHRvbVBhbmVsQ3NzID0gY3NzYFxuICAgIGhlaWdodDogMjMycHg7XG4gICAgcGFkZGluZzogMTJweDtcbmA7XG5cbmNvbnN0IGlubmVyQ29udGFpbmVyQ3NzID0gY3NzYFxuICAgIGJhY2tncm91bmQ6ICNmYmZiZmQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ViZWNmNztcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgaGVpZ2h0OiAyMDBweDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgcGFkZGluZzogMTZweDtcbmA7XG5cbmNvbnN0IG1hbmFnZUJ0bkNzcyA9IGNzc2BcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM2NTczZmY7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGNvbG9yOiAjNjU3M2ZmO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBoZWlnaHQ6IDM4cHg7XG4gICAgbWFyZ2luLXRvcDogYXV0bztcbiAgICB0cmFuc2l0aW9uOiAwLjRzO1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgJjpmb2N1cyB7XG4gICAgICAgIG91dGxpbmU6IG5vbmU7XG4gICAgfVxuXG4gICAgJjpkaXNhYmxlZCB7XG4gICAgICAgIGNvbG9yOiBsaWdodGdyYXk7XG4gICAgICAgIGN1cnNvcjogaW5pdGlhbDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xuICAgIH1cbmA7XG5cbmNvbnN0IHNwZWNpYWxQYWdlSWNvbiA9IGNzc2BcbiAgICBtYXJnaW4tYm90dG9tOiAxNHB4O1xuYDtcblxuY29uc3Qgc3BlY2lhbFBhZ2VEZXNjcmlwdGlvbkNzcyA9IGNzc2BcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZzogMCAzNXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbmA7XG5cbmV4cG9ydCBjb25zdCBCb3R0b21QYW5lbDogRkMgPSAoKSA9PiB7XG4gICAgY29uc3QgdGFiRGF0YSA9IHVzZVNlbGVjdG9yKHRhYkRhdGFTZWxlY3Rvcik7XG4gICAgY29uc3Qge3RyYWNrZXJzQmxvY2tlZCwgY29va2llc0Jsb2NrZWQsIGlzU3BlY2lhbFBhZ2UsIGlzUHJpdmFjeUVuYWJsZWR9ID0gdGFiRGF0YTtcblxuICAgIGNvbnN0IGRpc3BhdGNoID0gdXNlQXBwRGlzcGF0Y2goKTtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxkaXYgY3NzPXtib3R0b21QYW5lbENzc30+XG4gICAgICAgICAgICA8ZGl2IGNzcz17aW5uZXJDb250YWluZXJDc3N9PlxuICAgICAgICAgICAgICAgIHshaXNTcGVjaWFsUGFnZSAmJiAoXG4gICAgICAgICAgICAgICAgICAgIDw+XG4gICAgICAgICAgICAgICAgICAgICAgICA8VHJhY2tlckRpc3BsYXlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBibG9ja2VkPXtpc1ByaXZhY3lFbmFibGVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvdW50PXt0cmFja2Vyc0Jsb2NrZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VidGl0bGU9e2dldE1lc3NhZ2UoJ3BvcHVwSG9tZVRyYWNrZXJzTGFiZWwnKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICA8VHJhY2tlckRpc3BsYXlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBibG9ja2VkPXtpc1ByaXZhY3lFbmFibGVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvdW50PXtjb29raWVzQmxvY2tlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdWJ0aXRsZT17Z2V0TWVzc2FnZSgncG9wdXBIb21lQ29va2llc0xhYmVsJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNzcz17bWFuYWdlQnRuQ3NzfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJzdWJtaXRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGF0Y2goc2NyZWVuTWFuYWdlbWVudFNsaWNlLmFjdGlvbnMuc2V0QWN0aXZlU2NyZWVuKFNjcmVlbnMuTWFuYWdlVHJhY2tlcnMpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXshaXNQcml2YWN5RW5hYmxlZCB8fCAodHJhY2tlcnNCbG9ja2VkID09PSAwICYmIGNvb2tpZXNCbG9ja2VkID09PSAwKX1cbiAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Z2V0TWVzc2FnZSgncG9wdXBIb21lTWFuYWdlVHJhY2tlcnNCdG4nKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICA8Lz5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIHtpc1NwZWNpYWxQYWdlICYmIChcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjc3M9e3NwZWNpYWxQYWdlRGVzY3JpcHRpb25Dc3N9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPEhpZGRlbkljb24gY3NzPXtzcGVjaWFsUGFnZUljb259IC8+XG4gICAgICAgICAgICAgICAgICAgICAgICB7Z2V0TWVzc2FnZSgncG9wdXBIb21lTkFDYWxsb3V0Jyl9XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgKTtcbn07XG4iLCJpbXBvcnQgUmVhY3QsIHtGQ30gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHtjc3N9IGZyb20gJ0BlbW90aW9uL2NvcmUnO1xuaW1wb3J0IHt1c2VTZWxlY3Rvcn0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG4vLyBAdHMtZXhwZWN0LWVycm9yXG5pbXBvcnQgUHJvdGVjdGlvbk9uIGZyb20gJy4uLy4uLy4uL2ljb25zL3Byb3RlY3Rpb24tb24uc3ZnJztcbi8vIEB0cy1leHBlY3QtZXJyb3JcbmltcG9ydCBQcm90ZWN0aW9uT2ZmIGZyb20gJy4uLy4uLy4uL2ljb25zL3Byb3RlY3Rpb24tb2ZmLnN2Zyc7XG4vLyBAdHMtZXhwZWN0LWVycm9yXG5pbXBvcnQgUHJvdGVjdGlvbk9mZkdyYXkgZnJvbSAnLi4vLi4vLi4vaWNvbnMvcHJvdGVjdGlvbi1vZmYtZ3JheS5zdmcnO1xuXG5pbXBvcnQge2dldE1lc3NhZ2V9IGZyb20gJy4uLy4uLy4uL3V0aWxzL2xvY2FsaXphdGlvbic7XG5pbXBvcnQge3RhYkRhdGFTZWxlY3Rvcn0gZnJvbSAnLi4vLi4vLi4vc2VsZWN0b3JzL2luZGV4JztcbmltcG9ydCBUb2dnbGUgZnJvbSAnLi4vLi4vLi4vLi4vY29tbW9uL2NvbXBvbmVudHMvVG9nZ2xlJztcblxuY29uc3QgcHJvdGVjdGlvblRvZ2dsZUJhckNzcyA9IGNzc2BcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZWJlY2Y3O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWJlY2Y3O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgaGVpZ2h0OiA0NnB4O1xuICAgIHBhZGRpbmc6IDAgMTJweDtcbmA7XG5cbmNvbnN0IHByb3RlY3Rpb25JY29uQ3NzID0gY3NzYFxuICAgIG1hcmdpbi1yaWdodDogOHB4O1xuYDtcblxuY29uc3QgcHJvdGVjdGlvbkRlc2NyaXB0aW9uQ3NzID0gY3NzYFxuICAgIGZvbnQtc2l6ZTogMTRweDtcblxuICAgID4gc3BhbiB7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgfVxuYDtcblxuZXhwb3J0IGNvbnN0IFByb3RlY3Rpb25Ub2dnbGVCYXI6IEZDID0gKCkgPT4ge1xuICAgIGNvbnN0IHRhYkRhdGEgPSB1c2VTZWxlY3Rvcih0YWJEYXRhU2VsZWN0b3IpO1xuICAgIGNvbnN0IHtpc1ByaXZhY3lFbmFibGVkLCBpc1NwZWNpYWxQYWdlfSA9IHRhYkRhdGE7XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2IGNzcz17cHJvdGVjdGlvblRvZ2dsZUJhckNzc30+XG4gICAgICAgICAgICB7aXNQcml2YWN5RW5hYmxlZCAmJiA8UHJvdGVjdGlvbk9uIGNzcz17cHJvdGVjdGlvbkljb25Dc3N9IC8+fVxuICAgICAgICAgICAgeyFpc1ByaXZhY3lFbmFibGVkICYmICFpc1NwZWNpYWxQYWdlICYmIDxQcm90ZWN0aW9uT2ZmIGNzcz17cHJvdGVjdGlvbkljb25Dc3N9IC8+fVxuICAgICAgICAgICAgeyFpc1ByaXZhY3lFbmFibGVkICYmIGlzU3BlY2lhbFBhZ2UgJiYgPFByb3RlY3Rpb25PZmZHcmF5IGNzcz17cHJvdGVjdGlvbkljb25Dc3N9IC8+fVxuICAgICAgICAgICAgPGRpdiBjc3M9e3Byb3RlY3Rpb25EZXNjcmlwdGlvbkNzc30+XG4gICAgICAgICAgICAgICAge2dldE1lc3NhZ2UoJ3BvcHVwSG9tZVRvZ2dsZUxhYmVsJyl9eycgJ31cbiAgICAgICAgICAgICAgICA8c3Bhbj57aXNQcml2YWN5RW5hYmxlZCA/IGdldE1lc3NhZ2UoJ3BvcHVwSG9tZVRvZ2dsZU9uJykgOiBnZXRNZXNzYWdlKCdwb3B1cEhvbWVUb2dnbGVPZmYnKX08L3NwYW4+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICBjc3M9e2Nzc2BcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICAgICAgICAgICAgYH1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8VG9nZ2xlXG4gICAgICAgICAgICAgICAgICAgIGlkPVwicHJpdmFjeS10b2dnbGVcIlxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ9ezI0fVxuICAgICAgICAgICAgICAgICAgICB3aWR0aD17NDB9XG4gICAgICAgICAgICAgICAgICAgIGhleENvbG9yPVwiIzY1NzNmZlwiXG4gICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e2lzUHJpdmFjeUVuYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgIGFuaW1hdGU9e2ZhbHNlfVxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFpc1NwZWNpYWxQYWdlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdG9nZ2xlUHJpdmFjeSA9IGlzUHJpdmFjeUVuYWJsZWQgPyAnZGVhY3RpdmF0ZVByaXZhY3knIDogJ2FjdGl2YXRlUHJpdmFjeSc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJvd3Nlci5ydW50aW1lLnNlbmRNZXNzYWdlKHttZXNzYWdlOiB0b2dnbGVQcml2YWN5fSkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyb3dzZXIudGFicy5yZWxvYWQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LmNsb3NlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICApO1xufTtcbiIsImltcG9ydCBSZWFjdCwge0ZDfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQge2Nzc30gZnJvbSAnQGVtb3Rpb24vY29yZSc7XG5pbXBvcnQge3VzZVNlbGVjdG9yfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB7Z2V0TWVzc2FnZX0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvbG9jYWxpemF0aW9uJztcblxuLy8gQHRzLWV4cGVjdC1lcnJvclxuaW1wb3J0IFByaXZhY3lTY29yZTEgZnJvbSAnLi4vLi4vLi4vaWNvbnMvcHJpdmFjeS1zY29yZS0xLnN2Zyc7XG4vLyBAdHMtZXhwZWN0LWVycm9yXG5pbXBvcnQgUHJpdmFjeVNjb3JlMiBmcm9tICcuLi8uLi8uLi9pY29ucy9wcml2YWN5LXNjb3JlLTIuc3ZnJztcbi8vIEB0cy1leHBlY3QtZXJyb3JcbmltcG9ydCBQcml2YWN5U2NvcmUzIGZyb20gJy4uLy4uLy4uL2ljb25zL3ByaXZhY3ktc2NvcmUtMy5zdmcnO1xuLy8gQHRzLWV4cGVjdC1lcnJvclxuaW1wb3J0IFByaXZhY3lTY29yZTQgZnJvbSAnLi4vLi4vLi4vaWNvbnMvcHJpdmFjeS1zY29yZS00LnN2Zyc7XG4vLyBAdHMtZXhwZWN0LWVycm9yXG5pbXBvcnQgUHJpdmFjeVNjb3JlNSBmcm9tICcuLi8uLi8uLi9pY29ucy9wcml2YWN5LXNjb3JlLTUuc3ZnJztcbi8vIEB0cy1leHBlY3QtZXJyb3JcbmltcG9ydCBQcml2YWN5U2NvcmVEaXNhYmxlZDEgZnJvbSAnLi4vLi4vLi4vaWNvbnMvcHJpdmFjeS1zY29yZS1kaXNhYmxlZC0xLnN2Zyc7XG4vLyBAdHMtZXhwZWN0LWVycm9yXG5pbXBvcnQgUHJpdmFjeVNjb3JlRGlzYWJsZWQyIGZyb20gJy4uLy4uLy4uL2ljb25zL3ByaXZhY3ktc2NvcmUtZGlzYWJsZWQtMi5zdmcnO1xuLy8gQHRzLWV4cGVjdC1lcnJvclxuaW1wb3J0IFByaXZhY3lTY29yZURpc2FibGVkMyBmcm9tICcuLi8uLi8uLi9pY29ucy9wcml2YWN5LXNjb3JlLWRpc2FibGVkLTMuc3ZnJztcbi8vIEB0cy1leHBlY3QtZXJyb3JcbmltcG9ydCBQcml2YWN5U2NvcmVEaXNhYmxlZDQgZnJvbSAnLi4vLi4vLi4vaWNvbnMvcHJpdmFjeS1zY29yZS1kaXNhYmxlZC00LnN2Zyc7XG4vLyBAdHMtZXhwZWN0LWVycm9yXG5pbXBvcnQgUHJpdmFjeVNjb3JlRGlzYWJsZWQ1IGZyb20gJy4uLy4uLy4uL2ljb25zL3ByaXZhY3ktc2NvcmUtZGlzYWJsZWQtNS5zdmcnO1xuLy8gQHRzLWV4cGVjdC1lcnJvclxuaW1wb3J0IFByaXZhY3lTY29yZU5BIGZyb20gJy4uLy4uLy4uL2ljb25zL3ByaXZhY3ktc2NvcmUtbmEuc3ZnJztcblxuaW1wb3J0IHt0YWJEYXRhU2VsZWN0b3J9IGZyb20gJy4uLy4uLy4uL3NlbGVjdG9ycy9pbmRleCc7XG5cbmNvbnN0IHRvcFBhbmVsQ3NzID0gY3NzYFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIHBhZGRpbmc6IDEycHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuYDtcblxuY29uc3Qgc3VtbWFyeUNzcyA9IGNzc2BcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHBhZGRpbmc6IDRweCAwO1xuYDtcblxuY29uc3Qgc2NvcmVDc3MgPSBjc3NgXG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDVweDtcblxuICAgID4gc3BhbiB7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAycHg7XG4gICAgfVxuYDtcblxuY29uc3QgZG9tYWluQ3NzID0gY3NzYFxuICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIG1hcmdpbi1ib3R0b206IDhweDtcbiAgICBtYXgtd2lkdGg6IDI0MHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbmA7XG5cbmNvbnN0IHNjb3JlRGVzY3JpcHRpb25Dc3MgPSBjc3NgXG4gICAgY29sb3I6ICM3Zjg2OWY7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuYDtcblxuY29uc3Qgc3BlY2lhbFBhZ2VEZXNjcmlwdGlvbkNzcyA9IGNzc2BcbiAgICBjb2xvcjogI2NlZDFkZDtcbiAgICBmb250LXNpemU6IDIycHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBsaW5lLWhlaWdodDogMjRweDtcbiAgICB3aGl0ZS1zcGFjZTogcHJlLXdyYXA7XG5gO1xuXG5leHBvcnQgY29uc3QgVG9wUGFuZWw6IEZDID0gKCkgPT4ge1xuICAgIGNvbnN0IHRhYkRhdGEgPSB1c2VTZWxlY3Rvcih0YWJEYXRhU2VsZWN0b3IpO1xuICAgIGNvbnN0IHtpc1ByaXZhY3lFbmFibGVkLCBpc1NwZWNpYWxQYWdlLCBzY29yZSwgdGxkfSA9IHRhYkRhdGE7XG5cbiAgICBjb25zdCBzY29yZUljb25Dc3MgPSBjc3NgXG4gICAgICAgIGZsZXgtc2hyaW5rOiAwO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEycHg7XG4gICAgICAgIG9wYWNpdHk6ICR7aXNQcml2YWN5RW5hYmxlZCA/IDEgOiAwfTtcbiAgICAgICAgdHJhbnNpdGlvbjogb3BhY2l0eSAwLjRzO1xuICAgIGA7XG5cbiAgICBjb25zdCBzY29yZUljb25EaXNhYmxlZENzcyA9IGNzc2BcbiAgICAgICAgb3BhY2l0eTogJHtpc1ByaXZhY3lFbmFibGVkID8gMCA6IDF9O1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRyYW5zaXRpb246IG9wYWNpdHkgMC40cztcbiAgICBgO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPGRpdiBjc3M9e3RvcFBhbmVsQ3NzfT5cbiAgICAgICAgICAgIHtpc1NwZWNpYWxQYWdlICYmIChcbiAgICAgICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgICAgICA8UHJpdmFjeVNjb3JlTkEgY3NzPXtzY29yZUljb25Dc3N9IC8+IDxQcml2YWN5U2NvcmVOQSBjc3M9e3Njb3JlSWNvbkRpc2FibGVkQ3NzfSAvPlxuICAgICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgKX1cbiAgICAgICAgICAgIHtzY29yZSA9PT0gMSAmJiAoXG4gICAgICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICAgICAgPFByaXZhY3lTY29yZTEgY3NzPXtzY29yZUljb25Dc3N9IC8+IDxQcml2YWN5U2NvcmVEaXNhYmxlZDEgY3NzPXtzY29yZUljb25EaXNhYmxlZENzc30gLz5cbiAgICAgICAgICAgICAgICA8Lz5cbiAgICAgICAgICAgICl9XG4gICAgICAgICAgICB7c2NvcmUgPT09IDIgJiYgKFxuICAgICAgICAgICAgICAgIDw+XG4gICAgICAgICAgICAgICAgICAgIDxQcml2YWN5U2NvcmUyIGNzcz17c2NvcmVJY29uQ3NzfSAvPiA8UHJpdmFjeVNjb3JlRGlzYWJsZWQyIGNzcz17c2NvcmVJY29uRGlzYWJsZWRDc3N9IC8+XG4gICAgICAgICAgICAgICAgPC8+XG4gICAgICAgICAgICApfVxuICAgICAgICAgICAge3Njb3JlID09PSAzICYmIChcbiAgICAgICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgICAgICA8UHJpdmFjeVNjb3JlMyBjc3M9e3Njb3JlSWNvbkNzc30gLz4gPFByaXZhY3lTY29yZURpc2FibGVkMyBjc3M9e3Njb3JlSWNvbkRpc2FibGVkQ3NzfSAvPlxuICAgICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgKX1cbiAgICAgICAgICAgIHtzY29yZSA9PT0gNCAmJiAoXG4gICAgICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICAgICAgPFByaXZhY3lTY29yZTQgY3NzPXtzY29yZUljb25Dc3N9IC8+IDxQcml2YWN5U2NvcmVEaXNhYmxlZDQgY3NzPXtzY29yZUljb25EaXNhYmxlZENzc30gLz5cbiAgICAgICAgICAgICAgICA8Lz5cbiAgICAgICAgICAgICl9XG4gICAgICAgICAgICB7c2NvcmUgPT09IDUgJiYgKFxuICAgICAgICAgICAgICAgIDw+XG4gICAgICAgICAgICAgICAgICAgIDxQcml2YWN5U2NvcmU1IGNzcz17c2NvcmVJY29uQ3NzfSAvPiA8UHJpdmFjeVNjb3JlRGlzYWJsZWQ1IGNzcz17c2NvcmVJY29uRGlzYWJsZWRDc3N9IC8+XG4gICAgICAgICAgICAgICAgPC8+XG4gICAgICAgICAgICApfVxuICAgICAgICAgICAgPGRpdiBjc3M9e3N1bW1hcnlDc3N9PlxuICAgICAgICAgICAgICAgIHshaXNTcGVjaWFsUGFnZSAmJiAoXG4gICAgICAgICAgICAgICAgICAgIDw+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNzcz17c2NvcmVDc3N9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtnZXRNZXNzYWdlKCdwb3B1cEhvbWVTY29yZUxhYmVsJyl9OiA8c3Bhbj57c2NvcmV9LzU8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY3NzPXtkb21haW5Dc3N9Pnt0bGR9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNzcz17c2NvcmVEZXNjcmlwdGlvbkNzc30+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3Njb3JlID09PSA1ICYmIGdldE1lc3NhZ2UoJ3BvcHVwSG9tZVNjb3JlRml2ZURlc2NyaXB0aW9uJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3Njb3JlID09PSA0ICYmIGdldE1lc3NhZ2UoJ3BvcHVwSG9tZVNjb3JlRm91ckRlc2NyaXB0aW9uJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3Njb3JlID09PSAzICYmIGdldE1lc3NhZ2UoJ3BvcHVwSG9tZVNjb3JlVGhyZWVEZXNjcmlwdGlvbicpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtzY29yZSA9PT0gMiAmJiBnZXRNZXNzYWdlKCdwb3B1cEhvbWVTY29yZVR3b0Rlc2NyaXB0aW9uJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3Njb3JlID09PSAxICYmIGdldE1lc3NhZ2UoJ3BvcHVwSG9tZVNjb3JlT25lRGVzY3JpcHRpb24nKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8Lz5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIHtpc1NwZWNpYWxQYWdlICYmIDxkaXYgY3NzPXtzcGVjaWFsUGFnZURlc2NyaXB0aW9uQ3NzfT57Z2V0TWVzc2FnZSgncG9wdXBIb21lU2NvcmVOQScpfTwvZGl2Pn1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICApO1xufTtcbiIsImltcG9ydCBSZWFjdCwge0ZDfSBmcm9tICdyZWFjdCc7XG5cbmltcG9ydCB7Qm90dG9tUGFuZWx9IGZyb20gJy4vQm90dG9tUGFuZWwnO1xuaW1wb3J0IHtQcm90ZWN0aW9uVG9nZ2xlQmFyfSBmcm9tICcuL1Byb3RlY3Rpb25Ub2dnbGVCYXInO1xuaW1wb3J0IHtUb3BQYW5lbH0gZnJvbSAnLi9Ub3BQYW5lbCc7XG5cbmNvbnN0IEhvbWU6IEZDID0gKCkgPT4ge1xuICAgIHJldHVybiAoXG4gICAgICAgIDw+XG4gICAgICAgICAgICA8VG9wUGFuZWwgLz5cbiAgICAgICAgICAgIDxQcm90ZWN0aW9uVG9nZ2xlQmFyIC8+XG4gICAgICAgICAgICA8Qm90dG9tUGFuZWwgLz5cbiAgICAgICAgPC8+XG4gICAgKTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IEhvbWU7XG4iXSwic291cmNlUm9vdCI6IiJ9