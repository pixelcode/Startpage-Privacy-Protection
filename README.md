# Startpage-Privacy-Protection

A browser addon created by [Startpage](https://startpage.com) and licensed under GPLv3. Available for download at [Mozilla](https://addons.mozilla.org/firefox/addon/startpage-privacy-protection).